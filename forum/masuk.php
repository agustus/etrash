<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-14 22:34:14
	**/
	require '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();
	$login = new Login($koneksi);

	if ($login->isUserLogin() == TRUE){
		//cek hak akses cuy
		$hak = $_SESSION['hak_akses'];
		$login->cek($hak);
		//echo "<META http-equiv=\"refresh\" content=\"0;URL=index.php\"> ";
		echo "<script>history.go(-2)</script>";
	}else {
    	include "../view/forum/masuk.php";
	}