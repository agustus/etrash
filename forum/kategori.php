<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-12 13:08:46
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	$thread = new Thread($koneksi);
	$forum = new Forum($koneksi);
	
	if(isset($_GET['id'])){ //id kategorinya bro
		$id_kat = $_GET['id'];
		if(empty($id_kat)){ //kalo idnya kosong
			redirect('../forum');
		}
		if(isset($_GET['sub'])){ //ini buat sub kategori
			$id_sub = $_GET['sub'];
			if(!empty($id_sub)){
				$kategori = $forum->detail_kategori($id_sub);
				$parent_kategori = $forum->detail_kategori($kategori['parent_id']);
				$isi_kategori = $forum->tampil_isi_kategori($id_sub);

				include "../view/forum/sub_kategori.php";
			}else{
				redirect('../forum');
			}
		}else{ //ini buat controller kategori
			$kategori = $forum->detail_kategori($id_kat);
			$sub_kategori = $forum->tampil_sub($id_kat);
			$isi_kategori = $forum->tampil_isi_kategori($id_kat);

			$sub = $forum->tampil_sub($id_kat);

			include "../view/forum/kategori.php";
		}
	}
	