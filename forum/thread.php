<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-13 21:12:36
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	//$login->auth();

	$forum = new Forum($koneksi);
	$threads = new Thread($koneksi);

	if(isset($_GET['id'])){ //id kategorinya bro
		$id_thread = $_GET['id'];
		if(!empty($id_thread)){
			$threads->ngitung($id_thread);
			$thread = $forum->tampil_thread($id_thread);
			$parent_kategori = $forum->detail_kategori($thread['parent_id']);
			$bls_thread = $forum->tampil_bls_thread($id_thread); // nampilin balasan thread
			if($thread['parent_thread']=='0'){
				include "../view/forum/thread.php";
			}else{
				redirect('../forum');
			}
			
		}else{
			redirect('../forum');
		}
	}else{
		redirect('../forum');
	}