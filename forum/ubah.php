<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-21 19:33:34
	**/
	require '../config/autoload.php';
	

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	$login->auth();

	$thread_ubh = new Thread($koneksi);


	if(isset($_GET['post'])){
		$post = $_GET['post'];
		$thread = $thread_ubh->tampil_thread($post);
		if(empty($thread)){//kalo ga ada direk aja
			redirect('../forum');
		}
		if(empty($post)){
			redirect('../forum');
		}else{
			$hak_tred = $thread['tb_user_id'];
			$id_u = $_SESSION['user_id'];
			if($hak_tred==$id_u){ //postingannya bukan?
				//Pas di klik tombol ubah
				if(isset($_POST['ubah'])){
					$judul = $_POST['txtJudul'];
					$isi = $_POST['txtIsi'];
					$thread_ubh->ubah_thread($post,$judul,$isi);
				}
				include "../view/forum/thread_u.php";
			}else{
				redirect('../forum');
			}
			
		}
	}else{
		redirect('../forum');
	}