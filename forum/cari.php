<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-21 17:52:33
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	$akun = new Akun($koneksi);
	$thread = new Thread($koneksi);

	if(isset($_GET['keywords'])){
		$key = $_GET['keywords'];
		if(!empty($key)){
			$t_car = $thread->cari_thread($key);
			include "../view/forum/cari.php";
		}else{
			redirect('../forum/index.php');
		}
	}else{
		redirect('../forum/index.php');
	}