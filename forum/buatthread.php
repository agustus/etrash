<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-14 19:40:56
	**/
	require '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	$login->auth();

	$forum = new Forum($koneksi);
	$kategori = $forum->tampil_kategori();

	$thread = new Thread($koneksi);

	$tred = array('username'=>'','isi'=>'', 'judul'=>'');

	if(isset($_GET['kategori'])){
		$id_kategori = $_GET['kategori'];
		$stt = "post-kategori";
		$bls = $thread->tampil_thread($id_kategori);

		if(empty($id_kategori)){ //kalo idkategorinya kosong di direk
			redirect('../forum');
		}

		if(isset($_GET['sub'])){ //sub kategori
			$id_sub = $_GET['sub'];
			if(empty($id_sub)){ //kalo idkategorinya kosong di direk
				redirect('../forum');
			}
			$kategori = $forum->detail_kategori($id_sub);
			$parent_kategori = $forum->detail_kategori($kategori['id']);

			if(isset($_POST['Post'])){ //pas di pencet button post
				$judul = $_POST['txtJudul'];
				$isi = $_POST['txtIsi'];
				$id_kategoris = $id_sub;
				$thread->submit_thread('0',$judul,$isi,$id_kategoris); //0 = parent
			}
		}else{ //ga pake sub
			if(isset($_POST['Post'])){ //pas di pencet button post
				$judul = $_POST['txtJudul'];
				$isi = $_POST['txtIsi'];
				$id_kategoris = $id_kategori;
				$thread->submit_thread('0',$judul,$isi,$id_kategoris); //0 = parent
			}
		}
	}else{
		$stt = "thr-new";
		if(isset($_POST['Post'])){
			$judul = $_POST['txtJudul'];
			$isi = $_POST['txtIsi'];
			$id_kategori = $_POST['txtKategori'];
			$thread->submit_thread('0',$judul,$isi,$id_kategori); //0 = parent
		}
	}
	include "../view/forum/thread_b.php";