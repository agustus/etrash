<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-14 09:28:54
	**/
	require '../config/autoload.php';
	

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	$login->auth();

	$thread_bls = new Thread($koneksi);

	if(isset($_GET['thread'])){
		$thread = $_GET['thread'];
		$bls = $thread_bls->tampil_thread($thread);
		$stt = "bales";
		$tred = array('username'=>'','isi'=>'', 'judul'=>'');
		if($bls['parent_thread']!='0'){ //kalo bukan parent di redirek
			redirect('../forum');
		}
		if(isset($_GET['post'])){ //buat quote
			$post = $_GET['post'];
			if(empty($post)){ //kalo kosong
				redirect('../forum/balas.php?thread='.$thread);
			}else{
				$tred = $thread_bls->tampil_thread($post);
			}
		}
		if(isset($_POST['Post'])){
			$judul = $_POST['txtJudul'];
			$isi = $_POST['txtIsi'];
			$id_kategori = $bls['id_kategori'];
			$thread_bls->submit_thread($thread,$judul,$isi,$id_kategori);
		}
		
		include '../view/forum/thread_b.php'; //view bales/new
	}else{
		redirect('../forum');
	}

	