<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-11 22:36:55
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	$akun = new Akun($koneksi);

	$forum = new Forum($koneksi);
	$thread = $forum->tampil_semua_thread();
	$post = $forum->tampil_semua_post();
	$pengguna = $akun->tampil();

	$kategori = $forum->tampil_kategori();
	$topUser_post = $forum->top_user_post();
	$topUser_thread = $forum->top_user_thread();

	if(isset($_SESSION['user_id'])){
		$id_us = $_SESSION['user_id'];
		$dataAkun = $akun->tampil_akun_forum($id_us);
	}

	if(isset($_GET['keluar'])){
		$login = new Login($koneksi);
		$login->doLogout();
	}

	include "../view/forum/beranda.php";