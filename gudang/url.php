<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 20:40:28
	**/
require_once '../config/pengaturan.php';
	if ( ! function_exists('base_url'))
	{
		function base_url($adaw=null)
		{
			$pengaturan = new Pengaturan();
			$url = $pengaturan->url();
			if($adaw==""){
				return $url;
			}else{
				return $url.$adaw;
			}
		}
		function base_asset($adaw=null)
		{
			$pengaturan = new Pengaturan();
			$url = $pengaturan->url();
			if($adaw==""){
				return $url.'asset/';
			}else{
				return $url.'asset/'.$adaw;
			}	
		}
		function site_url($adaw=null)
		{
			$url = sprintf(
    			"%s://%s%s",
    			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    			$_SERVER['SERVER_NAME'],
    			$_SERVER['REQUEST_URI']
  			);
			if($adaw==""){
				return $url;
			}else{
				return $url.$adaw;
			}	
		}
		function redirect($url=null)
		{
			if($url!=null){
				//echo "<meta http-equiv='refresh' content='0; url=".$url."'>";	
				header('location: '.$url);
			}else{
				//echo "<meta http-equiv='refresh' content='0; url=".$_SERVER['PHP_SELF']."'>";
				header('location: '.$_SERVER['PHP_SELF']);
			}
			
		}
	}