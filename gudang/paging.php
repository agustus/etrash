<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-11 15:08:29
	**/
require_once '../config/pengaturan.php';
	if ( ! function_exists('paging'))
	{
		function paging_artikelDepan($noHal,$jumSem,$jumArt){
			$noPage = $noHal;
			$jumPage=ceil($jumSem/$jumArt);

			//Membuat pagination
 			if ($noPage < $jumPage) {
  				echo "
  				<li class='arrow'>
  					<a href='".$_SERVER['PHP_SELF']."?halaman=".($noPage+1)."'>&raquo;</a>
  				</li>";
  			}
  	
 			//for($page = 1; $page <= $jumPage; $page++) asc
 			for($page = $jumPage; $page >= 1; $page--) //desc
 			{
  				$showPage = 0;
  				if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage))
  				{
   					if (($showPage == 4) && ($page != 2)) {
    					echo " ... ";
   					}
   					if (($showPage != ($jumPage - 1)) && ($page == $jumPage)) {
    					//echo " ... ";
   					}
   					if ($page == $noPage){   
    	 				echo "<li class='active'><a>".$page."</a></li>";
	   				}
   				else{
    				echo "<li><a href='".$_SERVER['PHP_SELF']."?halaman=".$page."'>".$page."</a></li>";;
   				}
   				$showPage=$page;
  				}
 			} 
 			
 			if($noPage > 1)
 			{
  				echo "
  				<li class='arrow'>
  					<a href='".$_SERVER['PHP_SELF']."?page=".($noPage-1)."'>&laquo;</a>
  				</li>";
 			}
 			
		}
	}