<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-25 15:33:20
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	//cek udah login apa belum
	$login->auth();

	$laporan = new Laporan($koneksi);

	//cek hak akses admin
	$hak = $_SESSION['hak_akses'];
	$login->cek($hak);

	if(isset($_GET['status'])){
		$id_laporan = $_GET['status'];

		//nutupin bug
		if(empty($id_laporan)){
			redirect('../admin/laporan.php');
		}
		
		$data = $laporan->tampil_satu_laporan($id_laporan);
		if($data['stt']==0){
			$stt=1;
		}else{
			$stt=0;
		}
		$laporan->ubah_stat($id_laporan,$stt);
		redirect('../admin/laporan.php');
	}else{
		$data = $laporan->tampil_laporan();

		include '../view/admin/laporan.php';
	}