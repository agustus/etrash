<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-25 13:27:08
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	//cek udah login apa belum
	$login->auth();

	$vote = new Vote($koneksi);

	//cek hak akses admin
	$hak = $_SESSION['hak_akses'];
	$login->cek($hak);

	$tambah = isset($_GET['buat']);
	$ubah = isset($_GET['ubah']);
	$hapus = isset($_GET['hapus']);
	$stats = isset($_GET['status']);

	if($tambah){
		if(isset($_POST['A_tambah'])){
			$nama = $_POST['A_nama'];
			$lokasi = $_POST['A_lok'];
			$lat = $_POST['A_lat'];
			$long = $_POST['A_long'];
			$url = $_POST['A_foto'];
			$vote->tambah_vote($nama,$lokasi,$lat,$long,$url);
		}
		include "../view/admin/vote_buat.php";
	}else if($ubah){
		$id_vote = $_GET['id'];

		//nutupin bug
		if(empty($id_vote)){
			redirect('../admin/vote.php');
		}

		$data = $vote->tampil_satu($id_vote);

		if(isset($_POST['A_ubah'])){
			$nama = $_POST['A_nama'];
			$lokasi = $_POST['A_lok'];
			$lat = $_POST['A_lat'];
			$long = $_POST['A_long'];
			$url = $_POST['A_foto'];
			$vote->ubah_vote($id_vote,$nama,$lokasi,$lat,$long,$url);
			redirect('../admin/vote.php');
		}

		include "../view/admin/vote_ubah.php";
	}else if($hapus){
		$id_vote = $_GET['id'];

		//nutupin bug
		if(empty($id_vote)){
			redirect('../admin/vote.php');
		}

		if($id_vote!=null){
			$vote->hapus($id_vote);
			redirect('./vote.php');
		}
	}else if($stats){
		$id_vote = $_GET['status'];

		//nutupin bug
		if(empty($id_vote)){
			redirect('../admin/vote.php');
		}
		
		$data = $vote->tampil_satu($id_vote);
		if($data['stt']==0){
			$stt=1;
		}else{
			$stt=0;
		}
		$vote->ubah_stat($id_vote,$stt);
		redirect('../admin/vote.php');
	}else{
		// buat array data vote dari method tampil_semua()
		$wadahVote = $vote->tampil();
		include '../view/admin/vote.php';
	}