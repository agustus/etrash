<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 21:28:57
	**/
	require_once '../config/autoload.php';

	$db = new Pengaturan();
	$koneksi = $db->ambilKoneksi();
	$login = new Login($koneksi);
	$login->doLogout();