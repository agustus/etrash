<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-11 21:25:43
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	//cek udah login apa belum
	$login->auth();

	//akun
	$akun = new Akun($koneksi);

	//cek hak akses admin
	$hak = $_SESSION['hak_akses'];
	$login->cek($hak);

	$ubah = isset($_GET['ubah']);
	$hapus = isset($_GET['hapus']);
	$stats = isset($_GET['status']);

	if(isset($_GET['tambah'])){
		if(isset($_POST['simpan'])){
			$akun->daftar();
		}
		include "../view/admin/akun_buat.php";	
	}else if(isset($_GET['ubah'])){
		if(isset($_GET['id'])){
			$idnya = $_GET['id'];
			$akunz = $akun->tampil_satu($idnya);
			if(isset($_POST['ubah'])){
				$akun->ubah_adm($idnya);
			}
			include "../view/admin/akun_ubah.php";
		}else{
			redirect('../admin/akun.php');
		}
	}else if($stats){
		$id_akun = $_GET['status'];

		//nutupin bug
		if(empty($id_artikel)){
			redirect('../admin/akun.php');
		}

		$data = $akun->tampil_satu($id_akun);

		if($data['stt']==0){
			$stt=1;
		}else{
			$stt=0;
		}
		$akun->ubah_stat($id_akun,$stt);
		redirect('../admin/akun.php');

	}else{
		// buat array data akun dari method tampil()
		$wadahAkun = $akun->tampil();
		include '../view/admin/akun.php';
	}