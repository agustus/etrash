<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-25 15:09:28
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	//cek udah login apa belum
	$login->auth();

	$poster = new Poster($koneksi);

	//cek hak akses admin
	$hak = $_SESSION['hak_akses'];
	$login->cek($hak);

	if(isset($_GET['status'])){
		$id_poster = $_GET['status'];

		//nutupin bug
		if(empty($id_poster)){
			redirect('../admin/vote.php');
		}
		
		$data = $poster->tampil_satu_poster($id_poster);
		if($data['stt']==0){
			$stt=1;
		}else{
			$stt=0;
		}
		$poster->ubah_stat($id_poster,$stt);
		redirect('../admin/poster.php');
	}else{
		$data = $poster->tampil_poster();

		include '../view/admin/poster.php';
	}