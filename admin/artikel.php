<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 21:20:55
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	//cek udah login apa belum
	$login->auth();

	//artikel
	$artikel = new Artikel($koneksi);

	//cek hak akses admin
	$hak = $_SESSION['hak_akses'];
	$login->cek($hak);
	
	$ubah = isset($_GET['ubah']);
	$hapus = isset($_GET['hapus']);

	if(isset($_GET['tambah'])){
		if(isset($_POST['A_pos'])){
			$judul = $_POST['A_judul'];
			$isi = $_POST['A_isi'];
			$stt = $_POST['A_stt'];
			$artikel->simpan($judul,$isi,$stt);
		}
		include "../view/admin/artikel_buat.php";
	}else if($ubah){
		$id_artikel = $_GET['id'];
		if($id_artikel!=null){
			$id_user = $_SESSION['user_id'];
			$dataArtikel = $artikel->tampil_satu($id_artikel);
			//cek berhak edit atau ngganya
			if($dataArtikel['tb_user_id']!=$id_user){
				pesan('Kamu ga punya hak buat ngubah bro');
				redirect('./artikel.php');
			}
			include "../view/admin/artikel_ubah.php";
		}else{
			if(isset($_POST['A_ubah'])){
				if($dataArtikel['tb_user_id']!=$id_user){
					pesan('Kamu ga punya hak buat ngubah bro');
					redirect('./artikel.php');
				}
				$judul = $_POST['A_judul'];
				$isi = $_POST['A_isi'];
				$stt = $_POST['A_stt'];
				$idar = $_POST['A_id'];
				$artikel->ubah($idar,$judul,$isi,$stt);
				echo $id_artikel;
			}
			redirect('./artikel.php');
		}
	}else if($hapus){
		$id_artikel = $_GET['id'];
		if($id_artikel!=null){
			$id_user = $_SESSION['user_id'];
			$dataArtikel = $artikel->tampil_satu($id_artikel);
			//cek berhak edit atau ngganya
			if($dataArtikel['tb_user_id']!=$id_user){
				pesan('Kamu ga punya hak buat ngehapus bro');
				redirect('./artikel.php');
			}else{
				$dataArtikel = $artikel->hapus($id_artikel);
				redirect('./artikel.php');
			}
		}
	}else{
		// buat array data artikel dari method tampil()
		$wadahArtikel = $artikel->tampil();
		include '../view/admin/artikel.php';
	}
	
