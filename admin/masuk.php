<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 15:00:53
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();
	$login = new Login($koneksi);

	if ($login->isUserLogin() == TRUE){
		//cek hak akses cuy
		$hak = $_SESSION['hak_akses'];
		$login->cek($hak);
		echo "<META http-equiv=\"refresh\" content=\"0;URL=index.php\"> ";
	}else {
    	include "../view/admin/masuk.php";
	}

