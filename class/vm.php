<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-17 22:31:34
	**/
class Vm{
	private $db_koneksi = NULL;
	private $tb = "tb_vm";
	function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}

	function tampil_vm($id_user){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_vm.user_vm where ".$this->tb.".user_di_vm='".$id_user."' order by ".$this->tb.".id desc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tambah($id="",$pesan=""){
		if(!$this->db_koneksi->connect_errno || $id!="" || $pesan!=""){
			$tgl = date("Y-m-d H:i:s");
			$user = $_SESSION['user_id'];
			$query = "insert into ".$this->tb." (user_di_vm, user_vm, vm, tanggal) VALUES ('".$id."', '".$user."','".$pesan."','".$tgl."')";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo"
				<script>alert('Data Berhasil Disimpan')</script>
				";
				redirect('../web/profil.php?id='.$id);
			}else{
				echo "<script>alert('Gagal mengirim vm')</script>";	
			}
		}else{
			echo "<script>alert('Tidak boleh ada yng kosong!')</script>";
		}
	}
}