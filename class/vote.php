<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-15 08:38:57
	**/
class Vote{
	private $db_koneksi = NULL;
	private $tb = "tb_vote";
	private $tb_jwb = "tb_jwb_vote";

	function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}

	//admin
	function tampil_satu($id=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." where id='".$id."' order by tgl_vote asc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function tampil(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." order by tgl_vote asc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_last(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." order by id desc limit 1;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function tambah_vote($nama="",$lokasi="",$lat="",$long="",$url_poto){
		if(!$this->db_koneksi->connect_errno || $nama!="" || $lokasi!="" || $url_poto!=""){
			$tgl = date("Y-m-d H:i:s");
			$user = $_SESSION['user_id'];

			$query = "insert into ".$this->tb." (nama_lokasi, lokasi, tgl_vote, latitude, longtitude, url_poto, stt) VALUES ('".$nama."', '".$lokasi."','".$tgl."','".$lat."','".$long."','".$url_poto."','1')";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				$dats = $this->tampil_last();
				$iya = "insert into ".$this->tb_jwb." (jawaban, vote, tb_vote_id) VALUES ('Iya', '1','".$dats['id']."')";
				$this->db_koneksi->query($iya);
				$tidak = "insert into ".$this->tb_jwb." (jawaban, vote, tb_vote_id) VALUES ('Tidak', '1','".$dats['id']."')";
				$this->db_koneksi->query($tidak);
				echo"
				<script>alert('Data Berhasil Disimpan')</script>
				";
				redirect('../admin/vote.php');
			}else{
				echo "<script>alert('Data gagal disimpan ke database')</script>";	
			}
		}else{
			echo "<script>alert('Tidak boleh ada yng kosong!')</script>";
		}
	}

	function ubah_vote($id="",$nama="",$lokasi="",$lat="",$long="",$url_poto){
		if(!$this->db_koneksi->connect_errno){
			$query = "update ".$this->tb." SET nama_lokasi='".$nama."', lokasi='".$lokasi."', latitude='".$lat."', longtitude='".$long."', url_poto='".$url_poto."' WHERE id='".$id."'";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo "<script>alert('Berhasil Mengubah!')</script>";
			}else{
				//var_dump($query);
				echo "<script>alert('Gagal Mengubah!')</script>";
			}
		}
	}

	function ubah_stat($id="",$stt=""){
		if(!$this->db_koneksi->connect_errno){
			$query = "update ".$this->tb." SET stt='".$stt."' WHERE id='".$id."'";
			$hasil = $this->db_koneksi->query($query);
		}
	}

	function hapus($id=""){
		if(!$this->db_koneksi->connect_errno){
			$query = "delete from ".$this->tb_jwb." WHERE tb_vote_id='$id'";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				$query = "delete from ".$this->tb." WHERE id='$id'";
				$this->db_koneksi->query($query);
				echo "<script>alert('Berhasil Dihapus!')</script>";
			}
		}
	}

	//web
	function tampil_soal($id_vote=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." WHERE id='".$id_vote."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function tampil_jawaban($id_vote=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb_jwb." where tb_vote_id='$id_vote';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_semua(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." where stt='1' order by tgl_vote asc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_semua_json(){
		if(!$this->db_koneksi->connect_errno){
			$arr = array();
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." where stt='1' order by tgl_vote asc;");
			while ($row = $query->fetch_array()){
				$temp = array(
					"id"	=> $row['id'],
  					"nama_lokasi" => $row["nama_lokasi"],
  					"lokasi" => $row["lokasi"],
   					"tgl_vote" => $row["tgl_vote"],
  					"latitude" => $row["latitude"],
  					"longtitude" => $row["longtitude"],
  					"url_poto" => $row["url_poto"]
  					);
   				array_push($arr, $temp);
				//$data[] = $row;
			}
			$data = json_encode($arr);
			return "var data = {\"count\": 10785236, \"photos\":" . $data . "}";
        	
		}
	}

	function statistik($id_soal=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from tb_vote inner join tb_jwb_vote on tb_jwb_vote.tb_vote_id=tb_vote.id where tb_jwb_vote.tb_vote_id='$id_soal'");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
				return $data;
			}
		}
	}

	function jumlah_vote($id_soal=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select sum(tb_jwb_vote.vote) as tots from tb_vote inner join tb_jwb_vote on tb_jwb_vote.tb_vote_id=tb_vote.id where tb_jwb_vote.tb_vote_id='$id_soal'");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
				return $data[0];
			}
		}
	}

	function tambah($id_jwb="",$id_soal=""){
		if(!$this->db_koneksi->connect_errno){
			setcookie("user_id",$_SESSION['user_id'], time() + 200 * 24);
			if(!isset($_COOKIE["user_id"])){
				$query = "update ".$this->tb_jwb." set vote=vote+1 where id='".$id_jwb."'";
				$hasil = $this->db_koneksi->query($query);
				if($hasil){						
					echo "<script>alert('Terimakasih telah berpartisipasi.');</script>";
					/*$stat = $this->statistik($id_soal);
			$jum = 0;
			foreach ($stat as $stat) {
				$jum += $stat['vote'];
				$pr = sprintf("%2.1f",(($stat['vote']/$jum)*100));
				$gbr = $pr * 3;
			?>
					<section class="grid_12">
                 		<div class="progress-wrap">
                            <div class="progress">
                              <div class="progress-bar color2" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $stat['vote'];?>%">
                               <span class="bar-width"><?php echo $stat['vote'];?>%</span>
                              </div>
                            </div>
                            <h5><?php echo $stat['jawaban'];?></h5>
                        </div>
                    </section>
			<?php
					//echo $stat['jawaban']." : ".$stat['vote']." %<br>".$gbr."<hr>";
					}
				*/
				}else{
					echo "<script>alert('Gagal');</script>";
				}
			}else{
				echo "<script>alert('Anda sudah melakukan vote disini');</script>";
			}
		}
	}
}