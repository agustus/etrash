<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 15:31:27
	**/
class Login {
    
    private $db_koneksi = NULL;

    public $messages = array();
    
    public function __construct($koneksi){
        
        $this->db_koneksi = $koneksi;

        session_start();
        
        // Jika ada perintah logout
        if (isset($_GET['keluar'])){
            $this->doLogout();
        }
        
        if (isset($_POST['Joss'])){
            $this->doLogin();
        }
    }
    
    private function doLogin(){
        if (!$this->db_koneksi->connect_errno){
            
            $username = $this->db_koneksi->real_escape_string($_POST['A_uname']);
            $password = md5($this->db_koneksi->real_escape_string($_POST['A_pass']));
            
            $query = "SELECT * FROM tb_user WHERE username='$username' AND password='$password' AND stt='1';";
            $hasil = $this->db_koneksi->query($query);
            
            if ($hasil->num_rows == 1){
                $result_row = $hasil->fetch_object();
            
            	$_SESSION['user_id'] = $result_row->id;
                $_SESSION['username'] = $result_row->username;
                $_SESSION['email'] = $result_row->email;
                $_SESSION['hak_akses'] = $result_row->lvl;
                $_SESSION['user_login'] = 1;
                //Set KcFinder
                $_SESSION['ses_admin']="admin";
				$_SESSION['ses_kcfinder']=array();
				$_SESSION['ses_kcfinder']['disabled'] = false;
				$_SESSION['ses_kcfinder']['uploadURL'] = "../content_upload";
            } else {
                echo "<script>alert('Username atau Password Salah!');</script>";
                //exit();
            }
            
        }
    }
    
    public function doLogout()
    {
        // Hapus semua session
        $_SESSION = array();
        session_destroy();
        $this->messages[] = "Kamu udah keluar.";
        redirect('index.php');
    }
    
    public function isUserLogin(){
        if (isset($_SESSION['user_login'])){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function auth(){
    	if ($this->isUserLogin() != TRUE){
    		//echo "<script>alert('Kamu belum login!')</script>";
	 		redirect('masuk.php');
	 		exit();
		}
    }
    
    public function cek($hk)
    {
    	if($hk!=1){ //Bukan admin
    		echo "<script>alert('Kamu tidak berhak mengakses halaman ini!');</script>";
    		redirect('../web');
    	}
    }
}

