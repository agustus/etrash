<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 15:57:15
	**/
class Akun {

	private $db_koneksi = NULL;
	private $tb = "tb_user";

	public function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}
	public function daftar()
	{
		if (!$this->db_koneksi->connect_errno){
			$nama = $this->db_koneksi->real_escape_string($_POST['nama']);
			$email = $this->db_koneksi->real_escape_string($_POST['email']);
			$username = $this->db_koneksi->real_escape_string($_POST['username']);
            $password = md5($this->db_koneksi->real_escape_string($_POST['password']));
			$twitter = $this->db_koneksi->real_escape_string($_POST['twitter']);
			$bbm = $this->db_koneksi->real_escape_string($_POST['bbm']);
			$statSoc = $this->db_koneksi->real_escape_string($_POST['stat']);
			$tgl = date("Y-m-d H:i:s");
			
			if($nama=="" || $email=="" || $username=="" || $password==""){
				echo '
				<section class="note-box">
					<p>Username, email, nama, password tidak boleh kosong!</p>
				</section>
				';
			}else{
				if($this->cek($username)==1){
					echo '
					<section class="warning-box">
						<p>Username telah terpakai.</p>
					</section>';
				}else{
					$query = "INSERT INTO ".$this->tb." (username, password, email, nama, twitter, 
						pin_bbm, date_register, stt_sosial, lvl, stt) VALUES 
						('".$username."', '".$password."','".$email."','".$nama."','".$twitter."',
						'".$bbm."','".$tgl."','".$statSoc."','2','1')";
					$hasil = $this->db_koneksi->query($query);
					if($hasil){
						echo '
						<section class="success-box">
                    		<p>Berhasil daftar! Silahkan login.</p>
               			</section>
						';
						//redirect('index.php');
					}else{
						echo '
						<section class="error-box">
							<p>Gagal mendaftar.</p>
						</section>';	
					}
				}
			}
			exit();
		}
	}

	private function cek($username="")
	{
		if (!$this->db_koneksi->connect_errno){
			$query = "select * from tb_user where username='$username';";
            $hasil = $this->db_koneksi->query($query);
            
            if ($hasil->num_rows == 1){
            	return 1;
            }else{
            	return 0;
            }
		}
	}

	function tampil(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." order by id asc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_satu($id) {
        if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." where id='".$id."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}else{
        		redirect('../web');
        	}
		}
    }

    function tampil_akun_forum($id) {
        if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." where id='".$id."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
    }

    function ubah($idnya){
		if(!$this->db_koneksi->connect_errno){
			$nama = $this->db_koneksi->real_escape_string($_POST['A_nama']);
			$email = $this->db_koneksi->real_escape_string($_POST['A_email']);
            $password = md5($this->db_koneksi->real_escape_string($_POST['A_pass']));
			$twitter = $this->db_koneksi->real_escape_string($_POST['A_twt']);
			$bbm = $this->db_koneksi->real_escape_string($_POST['A_bbm']);
			$statSoc = $this->db_koneksi->real_escape_string($_POST['A_stat']);

			if($password!=md5("")){
				$pw = ", password='".$password."'";
			}else{
				$pw = "";
			}
			$query = "UPDATE ".$this->tb." SET nama='".$nama."', email='".$email."', twitter='".$twitter."', pin_bbm='".$bbm."', stt_sosial='".$statSoc."'".$pw." WHERE id='".$idnya."'";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo "<script>alert('Berhasil Mengubah!')</script>";
				redirect(base_url('web/profil.php'));
			}else{
				echo "<script>alert('Gagal Mengubah!')</script>";
				redirect(base_url('web/profil.php?ubah'));
			}
		}
	}

	//Admin
	function ubah_adm($idnya){
		if(!$this->db_koneksi->connect_errno){
			$nama = $this->db_koneksi->real_escape_string($_POST['A_nama']);
			$email = $this->db_koneksi->real_escape_string($_POST['A_email']);
            $password = md5($this->db_koneksi->real_escape_string($_POST['A_pass']));
			$twitter = $this->db_koneksi->real_escape_string($_POST['A_twt']);
			$bbm = $this->db_koneksi->real_escape_string($_POST['A_bbm']);
			$statSoc = $this->db_koneksi->real_escape_string($_POST['A_statSoc']);
			$lvl = $this->db_koneksi->real_escape_string($_POST['A_lvl']);
			$stat = $this->db_koneksi->real_escape_string($_POST['A_stat']);

			if($password!=md5("")){
				$pw = ", password='".$password."'";
			}else{
				$pw = "";
			}
			$query = "UPDATE ".$this->tb." SET nama='".$nama."', email='".$email."', twitter='".$twitter."', pin_bbm='".$bbm."', stt_sosial='".$statSoc."',lvl='".$lvl."',stt='".$stat."'".$pw." WHERE id='".$idnya."'";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo "<script>alert('Berhasil Mengubah!')</script>";
				redirect(base_url('./admin/akun.php'));
			}else{
				echo "<script>alert('Gagal Mengubah!')</script>";
				redirect(site_url());
			}
		}
	}

	public function hapus($id=""){
		if(!$this->db_koneksi->connect_errno){
			$query = "delete from ".$this->tb." WHERE id='$id'";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo "<script>alert('Berhasil Dihapus!')</script>";
			}
		}
	}

	function ubah_stat($id="",$stt=""){
		if(!$this->db_koneksi->connect_errno){
			$query = "update ".$this->tb." SET stt='".$stt."' WHERE id='".$id."'";
			$hasil = $this->db_koneksi->query($query);
		}
	}
}