<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-23 10:06:05
	**/
class Laporan{

	private $db_koneksi = NULL;
	private $tb = "tb_laporan";

	public function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}

	//admin
	function ubah_stat($id="",$stt=""){
		if(!$this->db_koneksi->connect_errno){
			$query = "update ".$this->tb." SET stt='".$stt."' WHERE id='".$id."'";
			$hasil = $this->db_koneksi->query($query);
		}
	}

	function tampil_laporan(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_laporan.id as id_laporan,tb_laporan.stt as stt_laporan,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_laporan.tb_user_id ;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_satu_laporan($id=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." where id='".$id."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	//user
	function upload(){
		if(!$this->db_koneksi->connect_errno){
			$namafolder="../asset/img/laporan/"; //tempat nyimpen poster
			if (!empty($_FILES["A_foto"]["tmp_name"])){
				$poster=$_FILES['A_foto']['type'];
				$judul=$_POST['A_nama'];
				$deskripsi=$_POST['A_deskripsi'];
				$tgl = date("Y-m-d H:i:s");
				$user = $_SESSION['user_id'];

				if($poster=="image/jpeg" || $poster=="image/jpg" || $poster=="image/gif" || $poster=="image/x-png"){
					$gambar = $namafolder . basename($_FILES['A_foto']['name']);		
					if(move_uploaded_file($_FILES['A_foto']['tmp_name'], $gambar)) {
						$query = "insert into ".$this->tb." (nama_laporan,deskripsi_laporan,screenshoot,tanggal,stt,tb_user_id) VALUES
															('".$judul."','".$deskripsi."','".$gambar."','".$tgl."','0','".$user."')";
						$hasil = $this->db_koneksi->query($query);
						/*echo "Gambar berhasil dikirim ".$gambar;
						echo "<p>Judul Gambar : $judul</p>";		   
						echo "<p><img src=\"$gambar\" width=\"200\"/></p>";	*/
						echo "<script>alert('Berhasil dikirim')</script>";	   
					}else{
		   				echo "<script>alert('Gambar gagal dikirim')</script>";
					}
   				}else{
					echo "<script>alert('Jenis gambar yang anda kirim salah. Harus .jpg .gif .png')</script>";
   				}
			}
		}
	}

	function tampil(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_laporan.id as id_laporan,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_laporan.tb_user_id where tb_laporan.stt=1 order by tb_laporan.tanggal desc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_limit($lim=0){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_laporan.id as id_laporan,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_laporan.tb_user_id where tb_laporan.stt=1 order by tb_laporan.tanggal desc limit ".$lim.";");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_satu($id_us=0){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_laporan.id as id_laporan,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_laporan.tb_user_id where tb_laporan.stt=1 and tb_laporan.tb_user_id='".$id_us."' order by tb_laporan.tanggal desc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_satu_lap($id=0){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_laporan.id as id_laporan,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_laporan.tb_user_id where tb_laporan.stt=1 and tb_laporan.id='".$id."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}
}