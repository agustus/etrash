<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-14 09:35:22
	**/
class Thread{
	private $db_koneksi = NULL;
	private $tb = "tb_thread";
	function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}

	function tampil_thread($id_thread=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.parent_id as parent_thread,tb_thread.id as id_thread, tb_thread.stt as status_thread, tb_kategori.id as id_kategori, tb_kategori.parent_id as parent_kategori, tb_user.id as id_user
from tb_user INNER JOIN (tb_thread INNER JOIN tb_kategori on tb_kategori.id=tb_thread.tb_kategori_id) on tb_thread.tb_user_id=tb_user.id WHERE tb_thread.id='".$id_thread."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function ambil_thread_terakhir(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." order by id desc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function submit_thread($id_thread="",$judul="",$isi="",$id_kategori=""){
		if(!$this->db_koneksi->connect_errno || $isi!=""){
			$tgl = date("Y-m-d H:i:s");
			$user = $_SESSION['user_id'];
			$query = "insert into ".$this->tb." (parent_id, judul, isi, tb_kategori_id, tb_user_id, tgl_post) VALUES 
						('".$id_thread."','".$judul."', '".$isi."','".$id_kategori."','".$user."','".$tgl."')";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo"
				<script>alert('Data Berhasil Disimpan')</script>
				";
				if($id_thread==0){ //meh langsung ka thread nu karek djieun
					$lastthread = $this->ambil_thread_terakhir();
					$id_thread = $lastthread['id'];
				}
				redirect('../forum/thread.php?id='.$id_thread);
			}else{
				echo "<script>alert('Data gagal disimpan ke database')</script>";	
			}
		}
		else{
			echo "<script>alert('Tidak boleh ada yng kosong!')</script>";
		}
	}

	function ubah_thread($id_thread="",$judul="",$isi=""){
		if(!$this->db_koneksi->connect_errno || $isi!=""){
			$tgl = date("Y-m-d H:i:s");

			$query = "update ".$this->tb." SET judul='".$judul."', isi='".$isi."', tgl_modif='".$tgl."' WHERE id='".$id_thread."'";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo "<script>alert('Berhasil Mengubah!');
				history.go(-2)</script>";
			}else{
				//var_dump($query);
				echo "<script>alert('Gagal Mengubah!')</script>";
			}
		}else{
			echo "<script>alert('Tidak boleh ada yng kosong!')</script>";
		}
	}

	function balasan($parent_id){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." where parent_id='".$parent_id."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function balasan_terakhir($parent_id){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.id as thread_id from tb_thread inner join tb_user on tb_user.id=tb_thread.tb_user_id where tb_thread.parent_id='".$parent_id."' order by tb_thread.id desc limit 1;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function cari_thread($key=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.parent_id as parent_thread,tb_thread.id as id_thread, tb_thread.stt as status_thread, tb_kategori.id as id_kategori, tb_user.id as id_user
from tb_user INNER JOIN (tb_thread INNER JOIN tb_kategori on tb_kategori.id=tb_thread.tb_kategori_id) on tb_thread.tb_user_id=tb_user.id WHERE tb_thread.parent_id=0 and tb_thread.judul like '%".$key."%';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	public function ngitung($id_thread){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("update ".$this->tb." set count=count+1 where id='$id_thread';");
		}
	}
}