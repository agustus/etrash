<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-21 23:01:39
	**/
class Poster {

	private $db_koneksi = NULL;
	private $tb = "tb_poster";

	public function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}

	//admin
	function ubah_stat($id="",$stt=""){
		if(!$this->db_koneksi->connect_errno){
			$query = "update ".$this->tb." SET stt='".$stt."' WHERE id='".$id."'";
			$hasil = $this->db_koneksi->query($query);
		}
	}

	function tampil_poster(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_poster.id as id_poster,tb_poster.stt as stt_poster,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_poster.tb_user_id order by tb_poster.tgl_poster desc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_satu_poster($id=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." where id='".$id."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	//user
	function ambilRandom(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." order by rand() limit 1;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function upload(){
		if(!$this->db_koneksi->connect_errno){
			$namafolder="../asset/img/poster/"; //tempat nyimpen poster
			if (!empty($_FILES["A_poster"]["tmp_name"])){
				$poster=$_FILES['A_poster']['type'];
				$judul=$_POST['A_nama'];
				$deskripsi=$_POST['A_deskripsi'];
				$tgl = date("Y-m-d H:i:s");
				$user = $_SESSION['user_id'];

				if($poster=="image/jpeg" || $poster=="image/jpg" || $poster=="image/gif" || $poster=="image/x-png"){
					$gambar = $namafolder . basename($_FILES['A_poster']['name']);		
					if(move_uploaded_file($_FILES['A_poster']['tmp_name'], $gambar)) {
						$orig_image = imagecreatefromjpeg($gambar);
$image_info = getimagesize($gambar); 
$width_orig  = $image_info[0]; // current width as found in image file
$height_orig = $image_info[1]; // current height as found in image file
$width = 500; // new image width
$height = 500; // new image height
$destination_image = imagecreatetruecolor($width, $height);
imagecopyresampled($destination_image, $orig_image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

imagejpeg($destination_image, $gambar, 100);

						$query = "insert into ".$this->tb." (nama_poster,deskripsi_poster,url_poster,tgl_poster,tb_kategori_poster_id,tb_user_id) VALUES
															('".$judul."','".$deskripsi."','".$gambar."','".$tgl."','1','".$user."')";
						$hasil = $this->db_koneksi->query($query);
						/*echo "Gambar berhasil dikirim ".$gambar;
						echo "<p>Judul Gambar : $judul</p>";		   
						echo "<p><img src=\"$gambar\" width=\"200\"/></p>";	*/
						//var_dump($query);
						if($hasil){
							redirect('../web/galeri.php');
						}	 
					}else{
		   				echo "<script>alert('Gambar gagal dikirim')</script>";
					}
   				}else{
					echo "<script>alert('Jenis gambar yang anda kirim salah. Harus .jpg .gif .png')</script>";
   				}
			}
		}
	}

	function tampil(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_poster.id as id_poster,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_poster.tb_user_id where tb_poster.stt=1 order by tb_poster.tgl_poster desc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}
	
	function tampil_limit($lim=0){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_poster.id as id_poster,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_poster.tb_user_id where tb_poster.stt=1 order by tb_poster.tgl_poster desc limit ".$lim.";");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_satu($id_us=0){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_poster.id as id_poster,tb_user.id as id_user from ".$this->tb." inner join tb_user on tb_user.id=tb_poster.tb_user_id where tb_poster.stt=1 and tb_poster.tb_user_id='".$id_us."' order by tb_poster.tgl_poster desc ;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}
}