<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 21:39:15
	**/
class Artikel {
	
	private $db_koneksi = NULL;
	private $tb = "tb_artikel";

	public function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}

	public function simpan($judul="",$isi="",$stt=""){
		if(!$this->db_koneksi->connect_errno || $judul!="" || $isi!=""){
			$tgl = date("Y-m-d H:i:s");
			$user = $_SESSION['user_id'];
			$query = "insert into ".$this->tb." (judul, isi, tgl, tb_user_id, stt) VALUES ('".$judul."', '".$isi."','".$tgl."','".$user."','".$stt."')";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo"
				<script>alert('Data Berhasil Disimpan')</script>
				";
				redirect('../web/artikel.php');
			}else{
				echo "<script>alert('Data gagal disimpan ke database')</script>";	
			}
		}else{
			echo "<script>alert('Tidak boleh ada yng kosong!')</script>";
		}
	}

	function tampil_satu($id) {
        if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * FROM ".$this->tb." where id='".$id."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
    }

    function tampil_satu_aktif($id) {
        if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_artikel.stt as stat_artikel,tb_artikel.id as id_artikel,tb_user.id as id_user FROM tb_artikel inner join tb_user on tb_user.id=tb_artikel.tb_user_id where tb_artikel.id='".$id."' and tb_artikel.stt='1';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
    }

	public function tampil(){
		if(!$this->db_koneksi->connect_errno){
			//$query = $this->db_koneksi->query("SELECT * FROM ".$this->tb." ORDER BY id");
			$query = $this->db_koneksi->query("select *,tb_artikel.stt as stat_artikel,tb_artikel.id as id_artikel FROM tb_artikel inner join tb_user on tb_user.id=tb_artikel.tb_user_id where tb_artikel.stt='1' order by tb_artikel.id desc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	public function ubah($id="",$judul="",$isi="",$stt=""){
		if(!$this->db_koneksi->connect_errno){
			$tgl = date("Y-m-d H:i:s");
			$query = "update ".$this->tb." SET judul='".$judul."', isi='".$isi."', stt='".$stt."', tgl_ubah='".$tgl."' WHERE id='".$id."'";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo "<script>alert('Berhasil Mengubah!')</script>";
			}else{
				//var_dump($query);
				echo "<script>alert('Gagal Mengubah!')</script>";
			}
		}
	}

	public function hapus($id=""){
		if(!$this->db_koneksi->connect_errno){
			$query = "delete from ".$this->tb." WHERE id='$id'";
			$hasil = $this->db_koneksi->query($query);
			if($hasil){
				echo "<script>alert('Berhasil Dihapus!')</script>";
			}
		}
	}

	// Web
	public function tampil_limit($lim){
		if(!$this->db_koneksi->connect_errno){
			//$query = $this->db_koneksi->query("select * FROM ".$this->tb." ORDER BY id");
			$query = $this->db_koneksi->query("select *,tb_artikel.stt as stat_artikel,tb_artikel.id as id_artikel FROM tb_artikel inner join tb_user on tb_user.id=tb_artikel.tb_user_id where tb_artikel.stt='1' order by tb_artikel.id desc limit $lim");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
				return $data;
			}
		}
	}

	public function tampil_awal_akhir($awal="",$akhir=""){
		if(!$this->db_koneksi->connect_errno){
			$mimiti = ($awal - 1) * $akhir;
			$query = $this->db_koneksi->query("select *,tb_artikel.stt as stat_artikel,tb_artikel.id as id_artikel from tb_artikel inner join tb_user on tb_user.id=tb_artikel.tb_user_id where tb_artikel.stt='1' order by tb_artikel.id desc limit ".$mimiti.", ".$akhir.";");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
				return $data;
			}
		}
	}

	public function cari($cari=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_artikel.stt as stat_artikel,tb_artikel.id as id_artikel from tb_artikel inner join tb_user on tb_user.id=tb_artikel.tb_user_id where tb_artikel.stt='1' and tb_artikel.judul like '%".$cari."%' order by tb_artikel.id desc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
				return $data;
			}
		}
	}

	public function ngitung($id_artikel){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("update ".$this->tb." set count=count+1 where id='$id_artikel';");
		}
	}

	public function top_artikel($lim=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." order by count desc limit $lim;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
				return $data;
			}
		}
	}

	public function tampil_artikel_user($id_user=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_user.id as id_user, tb_artikel.id as id_artikel from tb_user inner join tb_artikel on tb_artikel.tb_user_id=tb_user.id where tb_user.id='".$id_user."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
				return $data;
			}
		}
	}
}