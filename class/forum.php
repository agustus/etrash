<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-13 19:42:46
	**/
class Forum{
	private $db_koneksi = NULL;
	private $tb = "tb_thread";
	private $tb_kategori = "tb_kategori";
	private $tb_rating = "tb_rating";

	function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}

	function detail_kategori($id_kategori){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb_kategori." where id='".$id_kategori."' and stt='1' order by id asc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function tampil_kategori(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb_kategori." where parent_id='' and stt='1' order by id asc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_isi_kategori($id_kategori){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.id as thread_id,tb_user.id as user_id from ".$this->tb." inner join tb_user on tb_user.id=tb_thread.tb_user_id where tb_thread.tb_kategori_id='".$id_kategori."' and tb_thread.parent_id='' order by tb_thread.id desc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_sub($id_kategori=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb_kategori." where parent_id='".$id_kategori."' and stt='1' order by id asc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_semua_thread(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." where stt='1' and parent_id='0';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_semua_post(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." where stt='1';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_isi_sub_terakhir($id_kategori){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.id as thread_id,tb_user.id as user_id from ".$this->tb." inner join tb_user on tb_user.id=tb_thread.tb_user_id where tb_thread.tb_kategori_id='".$id_kategori."' and tb_thread.stt='1' and tb_thread.parent_id='0' order by tb_thread.id desc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function tampil_thread($id_thread=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.parent_id as parent_thread,tb_thread.id as id_thread, tb_thread.stt as status_thread, tb_kategori.id as id_kategori, tb_user.id as id_user
from tb_user INNER JOIN (tb_thread INNER JOIN tb_kategori on tb_kategori.id=tb_thread.tb_kategori_id) on tb_thread.tb_user_id=tb_user.id WHERE tb_thread.id='".$id_thread."';");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data[0];
        	}
		}
	}

	function tampil_bls_thread($id_thread=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.parent_id as parent_thread,tb_kategori.parent_id as parent_kategori,tb_thread.id as id_thread, tb_thread.stt as status_thread, tb_kategori.id as id_kategori, tb_user.id as id_user
from tb_user INNER JOIN (tb_thread INNER JOIN tb_kategori on tb_kategori.id=tb_thread.tb_kategori_id) on tb_thread.tb_user_id=tb_user.id WHERE tb_thread.parent_id='".$id_thread."' order by tb_thread.tgl_post asc;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_thread_user($id_user=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.id as id_thread, tb_thread.stt as status_thread, tb_kategori.id as id_kategori, tb_user.id as id_user
from tb_user INNER JOIN (tb_thread INNER JOIN tb_kategori on tb_kategori.id=tb_thread.tb_kategori_id) on tb_thread.tb_user_id=tb_user.id
where tb_user.id='".$id_user."' and tb_thread.parent_id=0 order by tb_thread.id desc limit 5;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function tampil_post_user($id_user=""){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,tb_thread.id as id_thread, tb_thread.stt as status_thread, tb_kategori.id as id_kategori, tb_user.id as id_user
from tb_user INNER JOIN (tb_thread INNER JOIN tb_kategori on tb_kategori.id=tb_thread.tb_kategori_id) on tb_thread.tb_user_id=tb_user.id
where tb_user.id='".$id_user."' order by tb_thread.id;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function top_user_post(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,count(tb_thread.id) as jumlah,tb_thread.id as id_thread, tb_thread.stt as status_thread, tb_kategori.id as id_kategori, tb_user.id as id_user
from tb_user INNER JOIN (tb_thread INNER JOIN tb_kategori on tb_kategori.id=tb_thread.tb_kategori_id) on tb_thread.tb_user_id=tb_user.id
group by tb_user.id order by jumlah desc limit 5;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	function top_user_thread(){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select *,count(tb_thread.id) as jumlah,tb_thread.id as id_thread, tb_thread.stt as status_thread, tb_kategori.id as id_kategori, tb_user.id as id_user
from tb_user INNER JOIN (tb_thread INNER JOIN tb_kategori on tb_kategori.id=tb_thread.tb_kategori_id) on tb_thread.tb_user_id=tb_user.id where tb_thread.parent_id='0'
group by tb_user.id order by jumlah desc limit 5;");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}
	

}