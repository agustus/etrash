<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-11 12:44:38
	**/
class Komentar {
	
	private $db_koneksi = NULL;
	private $tb = "tb_komentar";

	public function __construct($koneksi){
		$this->db_koneksi = $koneksi;
	}

	public function tampil($id_artikel){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("select * from ".$this->tb." where tb_artikel_id='".$id_artikel."' and stt='1' order by tgl asc");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	public function terakhir($lim){
		if(!$this->db_koneksi->connect_errno){
			$query = $this->db_koneksi->query("SELECT *,tb_artikel.id as id_artikel,tb_artikel.tgl as tgl_artikel FROM tb_komentar INNER JOIN tb_artikel on tb_artikel.id=tb_komentar.tb_artikel_id where tb_komentar.stt='1' order by tb_komentar.id desc limit $lim");
			while ($row = $query->fetch_array()){
				$data[] = $row;
			}
			if(!empty($data)){
	            return $data;
        	}
		}
	}

	public function kirim($nama="",$email="",$komentar="",$id_artikel){
		if(!$this->db_koneksi->connect_errno){
			if($nama=="" || $komentar==""){
				echo '
				<section class="note-box">
					<p>Nama/komentar tidak boleh kosong!</p>
				</section>
				';
			}else{
				$tgl = date("Y-m-d H:i:s");
				$query = "INSERT INTO ".$this->tb." (nama, email, komentar, tgl, stt, tb_artikel_id) VALUES ('".$nama."', '".$email."','".$komentar."','".$tgl."','1','".$id_artikel."')";
				$hasil = $this->db_koneksi->query($query);
				if($hasil){
					echo'
						<section class="success-box">
                    		<p>Berhasil mengirim komentar.</p>
               			</section>
               			';
				}else{
					echo '
						<section class="error-box">
							<p>Gagal mengirim komentar!</p>
						</section>
					';
				}
			}
			
		}else{
			echo '
				<section class="error-box">
					<p>Kesalahan database.</p>
				</section>';
		}
	}
}