-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2015 at 11:53 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_etrash`
--
CREATE DATABASE IF NOT EXISTS `db_etrash` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_etrash`;

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE IF NOT EXISTS `tb_artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(450) DEFAULT NULL,
  `isi` text,
  `tgl` timestamp NULL DEFAULT NULL,
  `tgl_ubah` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tb_user_id` int(11) NOT NULL,
  `count` int(11) DEFAULT '1',
  `stt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_artikel_tb_user_idx` (`tb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tb_artikel`
--

INSERT INTO `tb_artikel` (`id`, `judul`, `isi`, `tgl`, `tgl_ubah`, `tb_user_id`, `count`, `stt`) VALUES
(5, 'Daur Ulang Sampah Plastik', '<p>\r\n	<span style="font-family: verdana,geneva;"><a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/7-kode-plastik-daur-ulang">Plastik</a> sudah menjadi kebutuhan sehari-hari di kehidupan manusia saat ini. Mulai dari produk elektronik, makanan, minuman, mainan, rumah tangga, dan lain-lain. Seiring dengan kemajuan jaman dan pola hidup manusia saat ini maka pemakaian plastik yang semakin tinggi akhirnya menimbulkan dampak negatif yaitu sampah.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;">Plastik memiliki waktu lama jika melewati proses <a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/memanfaatkan-berbagai-macam-material-dari-sampah-elektronik">daur ulang</a> secara alami sehingga menjadi permasalahan serius karena memberikan kontribusi serius pada soal sampah. Pemrosesan daur ulang sampah plastik yang sudah dilakukan sekarang masih berbanding jauh dengan jumlah sampah plastik yang dihasilkan. Melihat kondisi diatas maka diperlukan ide dan kreatifitas guna menekan jumlah limbah plastik menjadi barang lain yang bermanfaat.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;">Salah satu yang bisa menjadi ide adalah mengolah sampah plastik menjadi produk keperluan rumah tangga. Ada banyak jenis barang berguna dari daur ulang limbah plastik seperti keranjang sampah, tas, souvenir, dan lain sebagainya. Ketika sampah berubah menjadi bentuk serta fungsi lain yang bisa bermanfaat maka itu merupakan sumber <a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/memanfaatkan-berbagai-macam-material-dari-sampah-elektronik">peluang bisnis</a> alternatif yang menguntungkan.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;">Sampah plastik yang bisa digunakan sebagai bahan untuk membuat kerajinan sangat bermacam-macam. Mulai plastik keras dari <a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/7-kode-plastik-daur-ulang">kemasan</a> <a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/7-kode-plastik-daur-ulang">botol</a> minuman hingga plastik &ldquo;lunak&rdquo; seperti kantong plastik. Bahan penunjang tambahan juga tidak terlalu banyak seperti lem, benang, kawat, dan pewarna (jika diperlukan). Ketika semua sudah tersedia tinggal yang paling menentukan adalah ide untuk mendapatkan kreasi produk yang inovatif serta berguna. Tentu saja yang memiliki nilai jual tinggi dipasaran.</span></p>\r\n<p>\r\n	<img alt="Daur Ulang Sampah Plastik" class="aligncenter size-full wp-image-1075" height="364" src="http://www.inginbisnis.com/wp-content/uploads/2014/04/Daur-Ulang-Sampah-Plastik1.jpg" width="600" /></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;">Kekuatan dari produk daur ulang sampah plastik ini tidak diragukan lagi. Selain plastik terbuat dari bahan sintetis yang sifatnya tahan lama, jika dalam pembuatannya teliti maka tingkat keawetan produk tersebut bisa lama. Setiap produk yang memiliki nilai seni dan kegunaannya luas maka otomatis pasar akan merespon dengan antusias. Keuntungan yang didapatkan dari penjualan produk plastik daur ulang persentasenya tinggi karena tidak memerlukan biaya tinggi untuk mendapatkan bahan-bahannya.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;"><a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/memanfaatkan-berbagai-macam-material-dari-sampah-elektronik">Peluang usaha</a> daur ulang limbah plastik ini masuk kategori industri global, dalam arti bahwa tidak hanya industri skala besar saja yang bisa melakukannya tetapi usaha kecil dan menengah juga dapat mendaur ulang sampah plastik menjadi produk yang berguna. Jika anda peduli dengan lingkungsan dan memiliki ide-ide yang inovatif maka peluang usaha daur ulang sampah plastik ini bisa menjadi pilihan bijaksana yang menguntungkan.</span></p>\r\n', '2015-01-05 07:23:32', '2015-01-22 06:19:55', 2, 23, '1'),
(8, 'Bahaya Sampah Elektronik dan Pengolahannya', '<p>\r\n	Indonesia terkenal sebagai masyarakat dengan budaya yang konsumtif, termasuk dalam pemakaian barang-barang elektronik, entah itu gadget, ponsel, televisi model terbaru, komputer, laptop, dan sebagainya. Setiap hari, pasti ribuan jenis barang elektronik diproduksi dan diimpor ke negeri ini.</p>\r\n<p>\r\n	Tapi pernahkah terbayangkan, bagaimana nasib barang elektronik bekas yang sudah tak dipakai. Sudah benarkah pengelolaan sampah elektronik atau <em>electronic waste</em> atau <em>e-waste</em> di Indonesia? Sebab jika salah pengelolaan, <em>e-waste</em> yang banyak mengandung bahan beracun dan berbahaya (B3) akan memberi dampak negatif bagi manusia.</p>\r\n<p>\r\n	Pengajar Jurusan Teknik Mesin Universitas Janabadra, Yogyakarta, Mochamad Syarasiro mengatakan, sampah elektronik harus dikelola secara benar dengan pemisahan komponen yang benar-benar selektif. Alasannya, ada beberapa zat berbahaya dalam komponen elektronika yang membutuhkan penanganan secara khusus dan tak bisa dicampur dengan sampah lain.</p>\r\n<p>\r\n	&quot;Yang pertama plastik, khususnya jenis PVC, karena di dalamnya mengandung unsur klorin yang kita semua tahu bahwa<br />\r\n	itu zat yang cukup berbahaya bagi kesehatan, dan sangat sulit sekali terurai di dalam tanah, butuh waktu puluhan bahkan ratusan tahun untuk mengurainya,&quot; ujarnya kepada &quot;PR&quot;.</p>\r\n<p>\r\n	Yang kedua, menurut Syamsiro, timbal atau <em>lead</em>. Komponen ini pun sangat berbahaya. Seperti halnya dahulu di dalam bahan bakar bensin mengandung TEL (<em>tetra ethyl lead</em>) untuk menaikkan nilai oktan, di dalamnya terkandung kom-ponen timbal yang sangat berbahaya bagi kesehatan manusia.</p>\r\n<p>\r\n	&quot;TV dan layar komputer tipe CRT yang menggunakan tabung itu mengandung bahan timbal. Sekarang memang sudah beralih ke model LCD, tetapi persoalan berubah ke kandungan merkuri yang juga berbahaya karena tipe LCD ini banyak mengandung komponen merkuri. Tak hanya itu, dalam barang elektronik juga terdapat komponen logam lain yang sangat berbahaya seperti kadmium, beryllium, ataupun komponen lainnya seperti BFR (brominated flame retardant-red.),&quot; tutur lelaki yang saat ini sedang mengambil S-3 di Departemen Enviromental Science and Technology, Tokyo Institute of Technology.</p>\r\n<p>\r\n	Kadmium digunakan untuk pelapisan logam, terutama baja, besi, dan tembaga. Termasuk juga dalam pembuatan baterai dan plastik. Risiko dari kadmium, jika terisap, bersifat iritatif. Dalam jangka waktu lama menimbulkan efek keracunan, gangguan pada sistem organ dalam tubuh manusia dan hewan.</p>\r\n<p>\r\n	Lalu zat berbahaya lainnya yakni arsenik yang digunakan dalam industri elektronik, antara lain dalam pembuatan transistor, semi konduktor, gelas, tekstil, keramik, lem hingga bahan peledak. Arsenik ini berisiko menimbulkan gangguan metabolisme di dalam tubuh manusia dan hewan, bisa mengakibatkan keracunan bahkan kematian.</p>\r\n<p>\r\n	Komponen-komponen tersebut, menu-rut Syamsiro, kalau hanya dibuang ke tempat pembuangan akhir sampah (TPAS), akan mencemari lingkungan sekitarnya dan membahayakan manusia. Oleh karena itu, sampah elektronik ini harus diolah terlebih dahulu sehingga dapat meminimalisasi efek negatif dari zat-zat beracun dan berbahaya tersebut.</p>\r\n<p>\r\n	&quot;Bahkan dengan metode <em>recycling</em> atau daur ulang, kita bisa mendapatkan kembali logam-logam tersebut untuk dibuat komponen baru. Plastiknya bisa dikonversi menjadi bahan bakar cair setara bensin dan solar. Kandungan klorin bisa diolah menjadi HCl untuk berbagai keperluan industri,&quot; ungkapnya.</p>\r\n<p>\r\n	Berdasarkan data Badan Pusat Statistik (BPS) tahun 2012 yang dilansir dari laman Yayasan Lembaga Konsumen Indonesia (YLKI), produksi elektronik dalam negeri untuk dua jenis barang saja, yakni televisi dan komputer, jumlahnya cukup mencengangkan. Indonesia mampu memproduksi televisi sebanyak 12.500.000 kg per tahun dan mengimpor televisi sebanyak 6.687.082 kg per tahun.</p>\r\n<p>\r\n	Sementara untuk komputer, Indonesia mampu memproduksi 12.491.899.469 kg per tahun, dengan jumlah impor 35.344.733 kg per tahun. Sementara itu, menurut data Gabungan Elektronika (Gabel) Indonesia, penjualan barang elektronik pada Februari 2013 terlihat ada pertumbuhan sebesar 20 persen dibandingkan dengan periode yang sama tahun sebelumnya. Penjualan pada bulan Februari 2013 ini sebesar Rp2,4 triliun, meningkat dari Februari tahun 2012 lalu sebesar Rp 2,07 triliun.</p>\r\n<p>\r\n	Syamsiro mengatakan, untuk mengatasi permasalahan e-waste ini harus melibatkan semua pihak dan tidak hanya menjadi tanggung jawab pemerintah, tapi juga menjadi tanggung jawab produsen, penjual, dan masyarakat. (Feby Syarifah)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<em>sumber: Harian Pikiran Rakyat, 11 April 2013</em></p>\r\n', '2015-01-08 08:04:02', '2015-01-19 05:20:20', 2, 11, '1'),
(9, 'Sampah Kukar Bisa Jadi Batu Bara', '<p>\r\n	TENGGARONG - Pemkab Kukar sedang menyiapkan mesin pengolahan sampah menjadi bahan bakar setara batu bara, berlokasi di sekitar TPA Bekotok. Bahan bakar yang dihasilkan bakal memasok sumber energi bagi sejumlah power plant di Kukar. Dua pekan lalu, Bupati Kukar Rita Widyasari didampingi Kepala Perusda Kelistrikan dan Sumber Daya Energi (PKSDE) Kukar M Shafik Avicenna, mengunjungi Shanghai, Tiongkok</p>\r\n<p>\r\n	&ldquo;Kami ke Shanghai melihat langsung pabrik pengolahan sampah menjadi energi. Produk yang dihasilkan berupa pupuk dan batu bara,&rdquo; kata Rita. Dia mengatakan, produk yang dihasilkan hampir sama dengan batu bara asli dengan kalori 5.000-6.000. Hanya bagian unsur yang agak berbeda. Pabrik pengolahan sampah di Shanghai itu akan diimplementasikan di Kukar.</p>\r\n<p>\r\n	Tim dari PT Ametis Indo Sampah siap merakit mesinnya. Rita berharap tahun ini mesin pengolahan sampah siap dioperasikan. &ldquo;Kami sangat membutuhkan energi. Banyak sekali power plant yang perlu. Jadi, sebetulnya ini pengganti energi yang tidak terbarukan. Kalau nanti kekurangan batu bara, kita bisa pakai ini,&rdquo; jelasnya.</p>\r\n<p>\r\n	Ke depan, TPA Bekotok bukan lagi tempat pembuangan sampah tapi hanya pembuangan sementara. Sampah langsung diproses mesin pengolah dan keluar menjadi sumber energi. Pabrik pengolahan sampah ini akan dikelola swasta bekerja sama dengan PKSDE. &ldquo;Sementara ini, mesin pengolahan sampah didanai investor.</p>\r\n<p>\r\n	Kami tidak memakai APBD dulu,&rdquo; ujar Rita. Dia berharap, mesin pengolahan sampah bisa dioperasikan November 2014. &ldquo;Produk pengolahan sampah bisa menjadi briket dan pupuk. Pupuknya pun punya kualitas baik. Sekarang seluruh Shanghai menggunakan produk pupuk andalan ini,&rdquo; tuturnya. Rita menambahkan, briket merupakan buah pemikiran orang Indonesia.</p>\r\n<p>\r\n	Kukar akan menjadi daerah pertama di Indonesia yang menerapkan mesin pengolah sampah menjadi bahan bakar setara batu bara. Sementara itu, Presiden Direktur PT Ametis Indo Sampah Bayu Indrawan mengatakan, mesin pengolahan sampah menggunakan prinsip teknologi hidrotermal, memanfaatkan uap panas jenuh.</p>\r\n<p>\r\n	Sampah campur dimasukkan ke reaktor, lalu diinjeksikan dengan uap panas, sekitar 25-30 bar. &ldquo;Setelah satu jam, sampah keluar menjadi bahan seragam atau homogen yang tidak bau lagi. Bahkan bau seperti aroma kopi dan gampang kering. Karakteristiknya mendekati batu bara,&rdquo; jelas Bayu. Sedangkan bila produk ini dicampur 80 persen batu bara, karakteristik bahan bakarnya jadi hampir sama.</p>\r\n<p>\r\n	&ldquo;Apabila mau mendesain untuk power plant khusus dengan produk ini, kita bisa mendapatkan 100 persen batu bara tapi boiler harus dimodifikasi,&rdquo; imbuhnya. Menurutnya, keunggulan produk ini paling cocok dengan Indonesia. Selama ini dikenal beberapa teknologi pengolahan sampah secara konvensional, yakni incinerator menggunakan prinsip pembakaran.</p>\r\n<p>\r\n	Incinerator memang sangat bagus tapi tidak cocok dengan karakteristik sampah Indonesia. Mayoritas sampah negeri ini merupakan organik yang kadar airnya tinggi. Selain itu, biaya pengolahan sampah lewat incinerator terlalu tinggi. Belum cocok diterapkan di Indonesia. &ldquo;Air yang dibakar berarti efisiensi konversinya rendah sehingga energi yang dihasilkan incinerator kurang maksimum,&rdquo; ucapnya. (hmp02/*/ bby/k11)</p>\r\n', '2015-01-11 11:55:41', '2015-01-25 10:26:08', 2, 19, '1'),
(13, 'Bahaya Sampah Masyarakat', '<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n<p>\r\n	Bahaya Sampah Masyarakat</p>\r\n', '2015-01-19 08:18:25', '2015-01-23 02:39:22', 2, 13, '1'),
(14, 'Adaws', '<p>\r\n	coeg</p>\r\n', '2015-01-25 06:03:48', '2015-01-25 06:08:43', 2, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jwb_vote`
--

CREATE TABLE IF NOT EXISTS `tb_jwb_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jawaban` varchar(45) DEFAULT NULL,
  `vote` int(11) DEFAULT '0',
  `tb_vote_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_jwb_vote_tb_vote1_idx` (`tb_vote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tb_jwb_vote`
--

INSERT INTO `tb_jwb_vote` (`id`, `jawaban`, `vote`, `tb_vote_id`) VALUES
(1, 'Iya', 18, 1),
(2, 'Tidak', 4, 1),
(5, 'Iya', 10, 2),
(6, 'Tidak', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE IF NOT EXISTS `tb_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(45) DEFAULT NULL,
  `deskripsi` text,
  `parent_id` int(11) DEFAULT '0',
  `stt` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `nama_kategori`, `deskripsi`, `parent_id`, `stt`) VALUES
(1, 'Diskusi Sampah', NULL, 0, 1),
(2, 'Jalanan', NULL, 1, 1),
(3, 'Sekitar Masyarakat', NULL, 1, 1),
(4, 'Daur Ulang', NULL, 0, 1),
(5, 'Info Daur Ulang', NULL, 4, 1),
(6, 'Karya Daur Ulang', NULL, 4, 1),
(7, 'Lainnya', NULL, 0, 1),
(8, 'TPA (Tempat Pembuangan Akhir)', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori_poster`
--

CREATE TABLE IF NOT EXISTS `tb_kategori_poster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori_poster` varchar(45) DEFAULT NULL,
  `deskripsi_kategori_poster` text,
  `stt` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_kategori_poster`
--

INSERT INTO `tb_kategori_poster` (`id`, `nama_kategori_poster`, `deskripsi_kategori_poster`, `stt`) VALUES
(1, 'Adaw', 'Gatau', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_komentar`
--

CREATE TABLE IF NOT EXISTS `tb_komentar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(450) DEFAULT NULL,
  `email` varchar(450) DEFAULT NULL,
  `komentar` text,
  `tgl` timestamp NULL DEFAULT NULL,
  `stt` varchar(45) DEFAULT NULL,
  `tb_artikel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_komentar_tb_artikel1_idx` (`tb_artikel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_komentar`
--

INSERT INTO `tb_komentar` (`id`, `nama`, `email`, `komentar`, `tgl`, `stt`, `tb_artikel_id`) VALUES
(1, 'Ferdhika', 'fer@dika.web.id', 'Oke nice info (y)', '2015-01-19 05:20:51', '1', 9);

-- --------------------------------------------------------

--
-- Table structure for table `tb_laporan`
--

CREATE TABLE IF NOT EXISTS `tb_laporan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_laporan` varchar(45) DEFAULT NULL,
  `deskripsi_laporan` text,
  `screenshoot` varchar(500) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `stt` varchar(45) DEFAULT NULL,
  `tb_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_laporan_tb_user1_idx` (`tb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_laporan`
--

INSERT INTO `tb_laporan` (`id`, `nama_laporan`, `deskripsi_laporan`, `screenshoot`, `tanggal`, `stt`, `tb_user_id`) VALUES
(1, 'Ini Keren Pemandangannya', 'Di mana ya', '../asset/img/laporan/aDwNzYw_700b.jpg', '2015-01-25 09:34:09', '1', 2),
(3, 'Adaw', 'm,km.', '../asset/img/laporan/6a604f577dc4b9664bd973c15d45731b.jpg', '2015-01-25 09:06:29', '1', 2),
(4, 'ewgwg', 'Lorem ipsum Lorem ipsum ', '../asset/img/laporan/1.gif', '2015-01-25 09:36:47', '1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_poster`
--

CREATE TABLE IF NOT EXISTS `tb_poster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_poster` varchar(450) DEFAULT NULL,
  `deskripsi_poster` text,
  `url_poster` varchar(450) DEFAULT NULL,
  `tgl_poster` timestamp NULL DEFAULT NULL,
  `stt` int(11) DEFAULT '1',
  `count` int(11) DEFAULT '0',
  `tb_kategori_poster_id` int(11) NOT NULL,
  `tb_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_poster_tb_kategori_poster1_idx` (`tb_kategori_poster_id`),
  KEY `fk_tb_poster_tb_user1_idx` (`tb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tb_poster`
--

INSERT INTO `tb_poster` (`id`, `nama_poster`, `deskripsi_poster`, `url_poster`, `tgl_poster`, `stt`, `count`, `tb_kategori_poster_id`, `tb_user_id`) VALUES
(4, 'Simpanan Pokok', 'Awe', '../asset/img/poster/superhubo_1407990713_92.jpg', '2015-01-25 05:15:08', 1, 0, 1, 4),
(5, 'Apsl', 'dsknfs', '../asset/img/poster/6a604f577dc4b9664bd973c15d45731b.jpg', '2015-01-25 09:50:00', 1, 0, 1, 3),
(6, 'lsdmfds ', ' fmnsf msdf', '../asset/img/poster/1.gif', '2015-01-25 09:50:20', 1, 0, 1, 3),
(7, ',m,n', 'nbbnm', '../asset/img/poster/997085_1496477917284327_2903190376642227090_n.jpg', '2015-01-25 10:51:26', 1, 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_rating`
--

CREATE TABLE IF NOT EXISTS `tb_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` varchar(45) DEFAULT NULL,
  `voter` varchar(45) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `tb_thread_id` int(11) NOT NULL,
  `tb_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_rating_tb_thread1_idx` (`tb_thread_id`),
  KEY `fk_tb_rating_tb_user1_idx` (`tb_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_thread`
--

CREATE TABLE IF NOT EXISTS `tb_thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `judul` varchar(45) DEFAULT NULL,
  `isi` text,
  `tb_kategori_id` int(11) NOT NULL,
  `tb_user_id` int(11) NOT NULL,
  `tgl_post` timestamp NULL DEFAULT NULL,
  `tgl_modif` timestamp NULL DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `stt` int(45) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tb_thread_tb_kategori1_idx` (`tb_kategori_id`),
  KEY `fk_tb_thread_tb_user1_idx` (`tb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `tb_thread`
--

INSERT INTO `tb_thread` (`id`, `parent_id`, `judul`, `isi`, `tb_kategori_id`, `tb_user_id`, `tgl_post`, `tgl_modif`, `count`, `stt`) VALUES
(1, 0, 'Sampah', 'djakjndakwn', 1, 2, '2015-01-13 12:38:32', NULL, 10, 1),
(2, 1, NULL, 'Naon ath?', 1, 3, '2015-01-13 12:39:12', NULL, 0, 1),
(3, 0, 'Testing Thread', 'Tes Di Sub Kategori Sampah 2 [b]Adaw[/b] \r\n[center][size=3][b]SCEditor[/b][/size][/center]\r\n\r\nGive it a try! :)\r\n\r\n[color=#ff00]Red text and [/color][color=#3399ff]blue text![/color]\r\n\r\n[list=1]\r\n[li]A simple list[/li]\r\n[li]With two items[/li]\r\n[/list]\r\n\r\nIf you are using IE9+ or any non-IE browser just type [b]:[/b]) and it should be converted into :) as you type. [quote=ferdhika31 wrote:]Adaw[/quote]\r\n\r\n[img]http://blog.dika.web.id/wp-content/themes/adaw/img/zoo.png[/img]', 2, 2, '2015-01-13 14:58:45', '2015-01-21 15:13:51', 20, 1),
(4, 1, '', 'Aweu  :belo: ', 1, 2, '2015-01-14 12:03:13', '2015-01-21 15:58:22', 0, 1),
(5, 1, '', 'Eleh', 1, 2, '2015-01-14 12:04:35', NULL, 0, 1),
(6, 3, '', 'wah? :p', 2, 2, '2015-01-14 12:30:56', NULL, 0, 1),
(7, 1, '', 'kacrut', 1, 2, '2015-01-14 13:18:25', NULL, 0, 1),
(8, 1, '', 'edan', 1, 2, '2015-01-14 13:20:03', NULL, 0, 1),
(9, 1, '', 'siadeuh', 1, 2, '2015-01-14 13:20:44', NULL, 0, 1),
(10, 3, '', 'Kacrut :v', 2, 2, '2015-01-14 13:33:09', NULL, 0, 1),
(11, 3, '', 'Plak :g', 2, 4, '2015-01-14 13:37:32', NULL, 0, 1),
(12, 0, 'TPU terdekat', 'TPU terdekat', 4, 2, '2015-01-14 13:42:26', NULL, 0, 1),
(13, 12, '', 'Lembang fro', 4, 5, '2015-01-14 13:42:42', NULL, 0, 1),
(14, 12, '', 'wah?', 4, 2, '2015-01-14 13:46:14', NULL, 0, 1),
(15, 0, 'Capruk ah', 'Test posting cuk', 7, 2, '2015-01-14 13:47:37', NULL, 0, 1),
(16, 15, '', 'wkwk bae ath', 7, 2, '2015-01-14 13:47:57', NULL, 0, 1),
(17, 0, 'Masyarakat buang sampah sembarang', 'Masyarakat buang sampah sembarang, tong diturutan barudak..', 1, 2, '2015-01-14 14:04:07', NULL, 1, 1),
(18, 0, 'Tes Di Sub Kategori Sampah 2', 'Tes Di Sub Kategori Sampah 2 [b]Adaw[/b] \r\n[center][size=3][b]SCEditor[/b][/size][/center]\r\n\r\nGive it a try! :)\r\n\r\n[color=#ff00]Red text and [/color][color=#3399ff]blue text![/color]\r\n\r\n[ul]\r\n[li]A simple list[/li]\r\n[li]With two items[/li]\r\n[/ul]\r\n\r\nIf you are using IE9+ or any non-IE browser just type [b]:[/b]) and it should be converted into :) as you type.', 3, 2, '2015-01-14 14:38:03', NULL, 0, 1),
(19, 1, 'Wah', 'coba coba', 1, 3, '2015-01-15 02:51:47', NULL, 0, 1),
(20, 2, '', 'haha :D', 1, 2, '2015-01-18 12:20:25', NULL, 0, 1),
(21, 3, 'Testing Thread', 'wkwk', 2, 2, '2015-01-18 14:59:07', NULL, 0, 1),
(22, 3, '', '	[quote=admin menulis :]Kacrut :v[/quote]\r\n\r\nheeh kitu?', 2, 2, '2015-01-18 14:59:42', NULL, 0, 1),
(23, 17, 'Masyarakat buang sampah sembarang', '[quote=admin menulis :]Masyarakat buang sampah sembarang, tong diturutan barudak..[/quote]\r\n\r\noh kitu', 1, 3, '2015-01-19 09:29:19', NULL, 0, 1),
(24, 0, 'Sampah Bandung, Kemana Perginya?', 'ada yang tau kemana perginya sampah?  :bingung: ', 8, 3, '2015-01-20 10:30:34', '2015-01-24 14:21:46', 6, 1),
(25, 3, '', '[quote=admin menulis :]wah? :p[/quote]\r\n\r\niyah :g', 2, 3, '2015-01-20 11:34:04', NULL, 0, 1),
(27, 3, '', '[list=a]\r\n[li]adaw[/li]\r\n[li]adaw[/li]\r\n[/list]\r\n\r\n :D  :p  :(  :)  :@  T_T  ;)  :sip:  :takut:  :shutup:  :shakehand:  :_pm:  :_peace:  :no:  :yes:  :berduka:  :belo:  :belo:  :bingung:  :cd:  :genit:  :geram:  :hammer:  :heran:  :lol:  :loveindonesia:  :ngantuk:  :nyerah:  :nohope:  :malu: ', 2, 2, '2015-01-21 13:42:10', NULL, 0, 1),
(28, 3, '', '[quote=admin menulis :][list=a]\r\n[li]adaw[/li]\r\n[li]adaw[/li]\r\n[/list]\r\n\r\n :D  :p  :(  :)  :@  T_T  ;)  :sip:  :takut:  :shutup:  :shakehand:  :_pm:  :_peace:  :no:  :yes:  :berduka:  :belo:  :belo:  :bingung:  :cd:  :genit:  :geram:  :hammer:  :heran:  :lol:  :loveindonesia:  :ngantuk:  :nyerah:  :nohope:  :malu: [/quote]\r\n\r\n\r\nhaha test juga  :D ', 2, 3, '2015-01-22 02:59:40', NULL, 0, 1),
(29, 0, 'Daur ulang', 'Daur ulang adalah proses untuk menjadikan suatu bahan bekas menjadi bahan baru dengan tujuan mencegah adanya sampah yang sebenarnya dapat menjadi sesuatu yang berguna, mengurangi penggunaan bahan baku yang baru, mengurangi penggunaan energi, mengurangi polusi, kerusakan lahan, dan emisi gas rumah kaca jika dibandingkan dengan proses pembuatan barang baru. Daur ulang adalah salah satu strategi pengelolaan sampah padat yang terdiri atas kegiatan pemilahan, pengumpulan, pemprosesan, pendistribusian dan pembuatan produk/material bekas pakai, dan komponen utama dalam manajemen sampah modern dan bagian ketiga dalam proses hierarki sampah 4R (Reduce, Reuse, Recycle, and Replace).[br]\r\n\r\nMaterial yang bisa didaur ulang terdiri dari sampah kaca, plastik, kertas, logam, tekstil, dan barang elektronik. Meskipun mirip, proses pembuatan kompos yang umumnya menggunakan sampah biomassa yang bisa didegradasi oleh alam, tidak dikategorikan sebagai proses daur ulang. Daur ulang lebih difokuskan kepada sampah yang tidak bisa didegradasi oleh alam secara alami demi pengurangan kerusakan lahan. Secara garis besar, daur ulang adalah proses pengumpulan sampah, penyortiran, pembersihan, dan pemprosesan material baru untuk proses produksi.[br]\r\n\r\nPada pemahaman yang terbatas, proses daur ulang harus menghasilkan barang yang mirip dengan barang aslinya dengan material yang sama, contohnya kertas bekas harus menjadi kertas dengan kualitas yang sama, atau busa polistirena bekas harus menjadi polistirena dengan kualitas yang sama. Seringkali, hal ini sulit dilakukan karena lebih mahal dibandingkan dengan proses pembuatan dengan bahan yang baru. Jadi, daur ulang adalah proses penggunaan kembali material menjadi produk yang berbeda. Bentuk lain dari daur ulang adalah ekstraksi material berharga dari sampah, seperti emas dari prosesor komputer, timah hitam dari baterai, atau ekstraksi material yang berbahaya bagi lingkungan, seperti merkuri.[br]\r\n\r\nDaur ulang adalah sesuatu yang luar biasa yang bisa didapatkan dari sampah. Proses daur ulang aluminium dapat menghemat 95% energi dan mengurangi polusi udara sebanyak 95% jika dibandingkan dengan ekstraksi aluminium dari tambang hingga prosesnya di pabrik. Penghematan yang cukup besar pada energi juga didapat dengan mendaur ulang kertas, logam, kaca, dan plastik.[br]\r\n\r\nMaterial-material yang dapat didaur ulang dan prosesnya di antaranya adalah:[br][br]\r\n\r\n[b]Bahan bangunan[/b][br]\r\nMaterial bangunan bekas yang telah dikumpulkan dihancurkan dengan mesin penghancur, kadang-kadang bersamaan dengan aspal, batu bata, tanah, dan batu. Hasil yang lebih kasar bisa dipakai menjadi pelapis jalan semacam aspal dan hasil yang lebih halus bisa dipakai untuk membuat bahan bangunan baru semacam bata.[br]\r\n\r\n[b]Baterai[/b][br]\r\nBanyaknya variasi dan ukuran baterai membuat proses daur ulang bahan ini relatif sulit. Mereka harus disortir terlebih dahulu, dan tiap jenis memiliki perhatian khusus dalam pemprosesannya. Misalnya, baterai jenis lama masih mengandung merkuri dan kadmium, harus ditangani secara lebih serius demi mencegah kerusakan lingkungan dan kesehatan manusia.\r\nBaterai mobil umumnya jauh lebih mudah dan lebih murah untuk didaur ulang.[br]\r\n\r\n[b]Barang Elektronik[/b][br]\r\nBarang elektronik yang populer seperti komputer dan telepon genggam umumnya tidak didaur ulang karena belum jelas perhitungan manfaat ekonominya. Material yang dapat didaur ulang dari barang elektronik misalnya adalah logam yang terdapat pada barang elektronik tersebut (emas, besi, baja, silikon, dll) ataupun bagian-bagian yang masih dapat dipakai (microchip, processor, kabel, resistor, plastik, dll). Namun tujuan utama dari proses daur ulang, yaitu kelestarian lingkungan, sudah jelas dapat menjadi tujuan diterapkannya proses daur ulang pada bahan ini meski manfaat ekonominya masih belum jelas.[br]\r\n\r\n[b]Logam[/b][br]\r\nBesi dan baja adalah jenis logam yang paling banyak didaur ulang di dunia. Termasuk salah satu yang termudah karena mereka dapat dipisahkan dari sampah lainnya dengan magnet. Daur ulang meliputi proses logam pada umumnya; peleburan dan pencetakan kembali. Hasil yang didapat tidak mengurangi kualitas logam tersebut.\r\nContoh lainnya adalah aluminium, yang merupakan bahan daur ulang paling efisien di dunia. Namun pada umumnya, semua jenis logam dapat didaur ulang tanpa mengurangi kualitas logam tersebut, menjadikan logam sebagai bahan yang dapat didaur ulang dengan tidak terbatas.[br][br]\r\n\r\n[b]Bahan Lainnya[/b][br]\r\nKaca dapat juga didaur ulang. Kaca yang didapat dari botol dan lain sebagainya dibersihkan dair bahan kontaminan, lalu dilelehkan bersama-sama dengan material kaca baru. Dapat juga dipakai sebagai bahan bangunan dan jalan. Sudah ada Glassphalt, yaitu bahan pelapis jalan dengan menggunakan 30% material kaca daur ulang.[br][br]\r\n\r\nKertas juga dapat didaur ulang dengan mencampurkan kertas bekas yang telah dijadikan pulp dengan material kertas baru. Namun kertas akan selalu mengalami penurunan kualitas jika terus didaur ulang. Hal ini menjadikan kertas harus didaur ulang dengan mencampurkannya dengan material baru, atau mendaur ulangnya menjadi bahan yang berkualitas lebih rendah.[br][br]\r\n\r\nPlastik dapat didaur ulang sama halnya seperti mendaur ulang logam. Hanya saja, terdapat berbagai jenis plastik di dunia ini. Saat ini di berbagai produk plastik terdapat kode mengenai jenis plastik yang membentuk material tersebut sehingga mempermudah untuk mendaur ulang. Suatu kode di kemasan yang berbentuk segitiga 3R dengan kode angka di tengah-tengahnya adalah contohnya. Suatu angka tertentu menunjukkan jenis plastik tertentu, dan kadang-kadang diikuti dengan singkatan, misalnya LDPE untuk Low Density Poly Etilene, PS untuk Polistirena, dan lain-lain, sehingga mempermudah proses daur ulang.[br][br]\r\n\r\n[b]Jenis kode plastik yang umum beredar di antaranya:[/b][br][br]\r\n\r\nPET (Polietilena tereftalat). Umumnya terdapat pada botol minuman atau bahan konsumsi lainnya yang cair.[br]\r\nHDPE (High Density Polyethylene, Polietilena berdensitas tinggi) biasanya terdapat pada botol detergen.[br]\r\nPVC (polivinil klorida) yang biasa terdapat pada pipa, rnitur, dan sebagainya.[br]\r\nLDPE (Low Density Polyethylene, Polietilena berdensitas rendah) biasa terdapat pada pembungkus makanan.[br]\r\nPP (polipropilena) umumnya terdapat pada tutup botol minuman, sedotan, dan beberapa jenis mainan.[br]\r\nPS (polistirena) umum terdapat pada kotak makan, kotak pembungkus daging, cangkir, dan peralatan dapur lainnya.[br]', 5, 4, '2015-01-24 15:43:23', '2015-01-24 16:00:33', 13, 1),
(30, 3, 'Testing Thread', '[quote=admin menulis :]Tes Di Sub Kategori Sampah 2 [b]Adaw[/b] \r\n[center][size=3][b]SCEditor[/b][/size][/center]\r\n\r\nGive it a try! :)\r\n\r\n[color=#ff00]Red text and [/color][color=#3399ff]blue text![/color]\r\n\r\n[list=1]\r\n[li]A simple list[/li]\r\n[li]With two items[/li]\r\n[/list]\r\n\r\nIf you are using IE9+ or any non-IE browser just type [b]:[/b]) and it should be converted into :) as you type. [quote=ferdhika31 wrote:]Adaw[/quote]\r\n\r\n[img]http://blog.dika.web.id/wp-content/themes/adaw/img/zoo.png[/img][/quote]\r\nSemoga kita menang di Dinamik 10 UPI yo ,amiiinnnnn :D ', 2, 4, '2015-01-25 06:11:03', NULL, 0, 1),
(31, 0, 'Tempat Pembuangan Akhir', ' Tujuh alat berat yang biasa digunakan untuk mengolah sampah di Tempat Pembuangan Akhir (TPA) Sarimukti di Kampung Cigedig, Desa Sarimukti, Kecamatan Cipatat, Kabupaten Bandung Barat (KBB), tak dapat digunakan karena rusak. Tujuh alat berat itu pun dibiarkan teronggok di sekitar lokasi TPA.', 1, 4, '2015-01-25 06:15:24', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(450) DEFAULT NULL,
  `nama` varchar(450) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `pin_bbm` varchar(7) DEFAULT NULL,
  `date_register` timestamp NULL DEFAULT NULL,
  `stt_sosial` varchar(45) DEFAULT NULL,
  `lvl` varchar(45) DEFAULT NULL,
  `stt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `email`, `nama`, `twitter`, `pin_bbm`, `date_register`, `stt_sosial`, `lvl`, `stt`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'fer@dika.web.id', 'Ferdhika Yudira', 'ferdhikaaa', '747237B', '2015-01-09 08:25:41', 'Friend Zone', '1', '1'),
(3, 'adaw', '8ba9943744e7d67eceac0275e64235d6', 'ferdhika31@ymail.com', 'Moderator', 'ferdhikayudira', '3225242', '2015-01-09 09:14:09', 'Jomblo', '1', '1'),
(4, 'fauzan', '8ba9943744e7d67eceac0275e64235d6', 'fauzan.sullivan@gmail.com', 'Fauzan', 'fauzan', '', '2015-01-11 10:09:13', 'Pacaran', '2', '1'),
(5, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'rpl4rt08@gmail.com', 'Ferdhika Ganteng', 'rpl4rt', '', '2015-01-13 10:13:10', 'Jomblo', '2', '1'),
(6, 'Rizky_Bayu', 'e10adc3949ba59abbe56e057f20f883e', 'rizkybayu66@gmail.com', 'Rizky Bayu', '@RIIBAY', '---', '2015-01-15 06:59:06', 'Jomblo', '2', '1'),
(7, 'nunu', '2f8c3ab806a42e79c774cb09b41a53c8', 'nunu@yahoo.com', 'nunu', 'nunu', '7732AAA', '2015-01-17 13:46:02', 'Jomblo', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vm`
--

CREATE TABLE IF NOT EXISTS `tb_vm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_di_vm` int(11) DEFAULT NULL,
  `user_vm` int(11) DEFAULT NULL,
  `vm` text,
  `tanggal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_vm`
--

INSERT INTO `tb_vm` (`id`, `user_di_vm`, `user_vm`, `vm`, `tanggal`) VALUES
(1, 2, 5, 'Fotonya tamvan sekali bang :D', '2015-01-17 15:28:04'),
(2, 2, 4, 'Iya ganteng banget tuh', '2015-01-17 15:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vote`
--

CREATE TABLE IF NOT EXISTS `tb_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lokasi` text,
  `lokasi` text,
  `tgl_vote` timestamp NULL DEFAULT NULL,
  `latitude` varchar(250) DEFAULT NULL,
  `longtitude` varchar(45) DEFAULT NULL,
  `url_poto` varchar(450) DEFAULT NULL,
  `stt` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tb_vote`
--

INSERT INTO `tb_vote` (`id`, `nama_lokasi`, `lokasi`, `tgl_vote`, `latitude`, `longtitude`, `url_poto`, `stt`) VALUES
(1, 'TPA Leuwi Gajah', 'Kecamatan Cimahi Selatan, Kelurahan Batujajar Timur Cimahi', '2015-01-24 11:57:33', '-6.8993906', '107.4680915', 'http://kelas7smipa.edublogs.org/files/2012/09/Bencana-Leuwigajah-14-19lvh9q.jpg', 1),
(2, 'TPA Cipatat', 'Kecamatan Cipatat Bandung Barat', '2015-01-24 11:57:33', '-6.8248172', '107.3718027', 'http://farm7.staticflickr.com/6136/6003816227_0545cd7b5f.jpg', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD CONSTRAINT `fk_tb_artikel_tb_user` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_jwb_vote`
--
ALTER TABLE `tb_jwb_vote`
  ADD CONSTRAINT `fk_tb_jwb_vote_tb_vote1` FOREIGN KEY (`tb_vote_id`) REFERENCES `tb_vote` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_komentar`
--
ALTER TABLE `tb_komentar`
  ADD CONSTRAINT `fk_tb_komentar_tb_artikel1` FOREIGN KEY (`tb_artikel_id`) REFERENCES `tb_artikel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_laporan`
--
ALTER TABLE `tb_laporan`
  ADD CONSTRAINT `fk_tb_laporan_tb_user1` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_poster`
--
ALTER TABLE `tb_poster`
  ADD CONSTRAINT `fk_tb_poster_tb_kategori_poster1` FOREIGN KEY (`tb_kategori_poster_id`) REFERENCES `tb_kategori_poster` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_poster_tb_user1` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_rating`
--
ALTER TABLE `tb_rating`
  ADD CONSTRAINT `fk_tb_rating_tb_thread1` FOREIGN KEY (`tb_thread_id`) REFERENCES `tb_thread` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_rating_tb_user1` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_thread`
--
ALTER TABLE `tb_thread`
  ADD CONSTRAINT `fk_tb_thread_tb_kategori1` FOREIGN KEY (`tb_kategori_id`) REFERENCES `tb_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_thread_tb_user1` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
