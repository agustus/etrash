<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 14:14:41
	**/
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Halaman Tidak Ditemukan!</title>
<style type="text/css">
#container {
	text-align: center;
	padding-top: 30px;
}
body {
	background: #f5f5f5;
}
body, h1, p {
	font-family: "Helvetica", serif;
}
h1 {
	font-size: 130px;
	margin-bottom: 0px;
	border: 4px solid #55BE95;
	color: #55BE95;
	display: inline-block;
	width: 210px;
	height: 190px;
	padding: 50px 15px 10px 15px;
	text-align: center;
	text-indent: -3px;

	-moz-border-radius: 50%;
	-webkit-border-radius: 50%;
	border-radius: 20%;
}
p {
	font-size: 25px;
	color: #55BE95;
}
p b {
	font-size: 31px;
	display: block;
}
p a {
	display: block;
	font-size: 12px;
	color: #9cbf11;
	text-decoration: none;
	padding-top: 10px;
	text-shadow: none;
}
p a:hover {
	text-decoration: underline;
}
h1, p {
	text-shadow: 1px 2px 2px #bbb;
}
</style>
</head>
<body>
	<div id="container">
		<h1>404</h1>
		<p>
			<b>Alah!</b>
			Halaman tidak ditemukan!
			<a href="javascript:window.history.back();">&larr; Kembali ke Halaman Sebelumnya.</a>
		</p>
	</div>
</body>
</html>
