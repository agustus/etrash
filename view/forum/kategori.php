<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-13 20:52:56
	**/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
<head>
	<?php include 'meta.php';?>

	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,700italic" rel="stylesheet" type="text/css" />
	<link href="forum/font-awesome.css" rel="stylesheet" />

	<link href="forum/print.css" rel="stylesheet" media="print" title="printonly" />
	<link href="forum/stylesheet.css?assets_version=1" rel="stylesheet" media="screen, projection" />

</head>

<body class="section-viewforum ltr">
<?php include "header.php"; include "menu.php"; ?>
<main>
	<a class="hidden"></a>
	
<div id="second-menu">
<div class="wrap">
	<ul>
		<li>
			<a href="<?php echo base_url('forum');?>" accesskey="h">Beranda</a>
		</li>
		
		<li class="legend">
			<a href="<?php echo site_url();?>"><?php echo $kategori['nama_kategori']; ?></a>
		</li>
		
	</ul>
</div>
</div>


<div id="page-body" class="wrap">

<h2><a href="<?php echo site_url();?>"><?php echo $kategori['nama_kategori']; ?></a></h2>


<div class="topic-actions">

	
		<div class="btn btn-post" title="Buat topik baru">
			<a href="<?php echo base_url('forum/'); ?>buatthread.php?kategori=<?php echo $kategori['id']; ?>"><i class="fa fa-pencil"></i>Buat Thread
			</a>
		</div>
	
		<div class="pagination">
			
			<span class="total"><?php if(!empty($sub_kategori)){echo count($sub_kategori)." Sub Kategori";}else{echo "Tidak ada Sub Kategori";}?></span>
			<span><?php if(!empty($isi_kategori)){echo count($isi_kategori)." Thread/topik";}else{echo "Tidak ada thread/topik.";}?></span>
			<!--<span>Page <strong>1</strong> of <strong>1</strong></span>-->
			
		</div>
	
	</div>
		<div class="forabg">
		<?php
		if(!empty($sub_kategori)){
		?>
			<ul class="forabg-header">
				<li>
					<dl>
						<dt>
							Sub Kategori <?php echo $kategori['nama_kategori']; ?>
						</dt>
						<dd class="topics-posts">Topik/Thread</dd>
						<dd class="lastpost">Penulis</dd>
					</dl>
				</li>
			</ul>
			<ul class="forabg-content">
		<?php
			foreach ($sub_kategori as $sub_kategori) {
		?>
		<li>
			<dl class="icon" style="background-image: url(forum/img/kategori.svg);">
				<dt title="Buka aja">
					<a href="<?php echo base_url('forum/')."kategori.php?id=".$id_kat."&sub=".$sub_kategori['id']."";?>" class="forumtitle"><?php echo $sub_kategori['nama_kategori']; ?></a><br />
					<?php echo $sub_kategori['deskripsi']; ?>
				</dt>
				<?php
					$td = $forum->tampil_isi_sub_terakhir($sub_kategori['id']);
				?>
				<dd class="topics-posts">
							<a href="<?php if(!empty($td['thread_id'])){echo base_url('forum/thread.php?id=').$td['thread_id'];}else{echo site_url();}?>">
								<?php if(!empty($td['judul'])){echo $td['judul'];}else{echo "Belum ada thread.";}?>
							</a>
						</dd>
						<dd class="lastpost"><span>
							oleh <a href="<?php if(!empty($td['user_id'])){echo base_url('web/profil.php?id=').$td['user_id'];}else{echo site_url();}?>" style="color: #AA0000;" class="username-coloured"><?php if(!empty($td['username'])){echo $td['username'];}else{echo "Belom ada.";}?></a>
							<a href="<?php if(!empty($td['thread_id'])){echo base_url('forum/thread.php?id=').$td['thread_id'];}else{echo site_url();}?>">
								<i class="fa  fa-chevron-right"></i>
							</a> <br />
							<?php if(!empty($td['tgl_post'])){echo tgl_indo(substr($td['tgl_post'], 0,10)).", ".substr($td['tgl_post'], 10);}else{echo tgl_indo(substr(date("Y-m-d H:i:s"), 0,10)).", ".substr(date("Y-m-d H:i:s"), 10);}?></span>
						</dd>
				
			</dl>
		</li>
		<?php
			}
		}
		?>
	
	
			</ul>
		</div>
	
		<div class="forumbg">
		<ul class="forumbg-header">
			<li>
				<dl>
					<dt>Thread <?php echo $kategori['nama_kategori']; ?></dt>
					<dd class="posts-views">Balasan / Lihat</dd>
					<dd class="lastpost">Postingan Terakhir</dd>
				</dl>
			</li>
		</ul>
		<ul class="forumbg-content">
	
		<?php
		if(!empty($isi_kategori)){
			foreach ($isi_kategori as $isi_kategori) {
		?>
		<li class="row bg1 sticky">
			<dl class="icon" style="background-image: url(forum/img/topik.svg); background-repeat: no-repeat;">
				<dt title="Buka aja">
					<a href="<?php echo base_url('forum/')."thread.php?id=".$isi_kategori['thread_id']; ?>" class="topictitle"><?php echo $isi_kategori['judul'];?></a>
					<br />
					oleh <a href="<?php if(!empty($isi_kategori['user_id'])){echo base_url('web/profil.php?id=').$isi_kategori['user_id'];}else{echo site_url();}?>" style="color: #AA0000;" class="username-coloured"><?php if(!empty($isi_kategori['username'])){echo $isi_kategori['username'];}else{echo "Belom ada.";}?></a> &raquo; 
					<?php if(!empty($isi_kategori['tgl_post'])){echo tgl_indo(substr($isi_kategori['tgl_post'], 0,10)).", ".substr($isi_kategori['tgl_post'], 10);}else{echo tgl_indo(substr(date("Y-m-d H:i:s"), 0,10)).", ".substr(date("Y-m-d H:i:s"), 10);}?>
				</dt>
				<?php
				$balasan = $thread->balasan($isi_kategori['thread_id']);
				$bls = count($balasan);
				$balasan_terakhir = $thread->balasan_terakhir($isi_kategori['thread_id']);
				?>
				<dd class="posts-views"><?php echo $bls;?> <dfn>Balasan</dfn> / <?php echo $isi_kategori['count'];?> <dfn>x Dilihat</dfn></dd>
				<dd class="lastpost">
				<span>
					<?php
					if(!empty($balasan_terakhir)){
						echo "oleh <a href='".base_url('web/profil.php?id=').$balasan_terakhir['tb_user_id']."' style='color: #AA0000;' class='username-coloured'>".$balasan_terakhir['username']."</a>";
						echo "<!--<a href='".base_url('forum/thread.php?id=').$balasan_terakhir['thread_id']."'>
						<i class='fa fa-chevron-right'></i>
						</a>-->";
					}else{
						echo "oleh <a href='".base_url('web/profil.php?id=').$isi_kategori['user_id']."' style='color: #AA0000;' class='username-coloured'>".$isi_kategori['username']."</a>";
					}
					?>
				</span>
				</dd>
			</dl>
		</li>
		<?php
			}
		}
		?>

	


	
			</ul>
	</div>
	
	<a href="<?php echo base_url('forum');?>" class="left-box left" accesskey="r">Kembali Ke Beranda</a>

	<div class="clear"></div>


</div>

</main>
<?php include "footer.php";?>

</body>
</html>