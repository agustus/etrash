<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-12 12:56:09
	**/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
<head>
	<?php include 'meta.php';?>

	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,700italic" rel="stylesheet" type="text/css" />
	<link href="forum/font-awesome.css" rel="stylesheet" />

	<link href="forum/print.css" rel="stylesheet" media="print" title="printonly" />
	<link href="forum/stylesheet.css?assets_version=1" rel="stylesheet" media="screen, projection" />
</head>

<body class="section-index ltr">
<?php include "header.php"; include "menu.php"; ?>
<main>
<div id="page-body" class="wrap">

	<div class="c-wrap">
		<div class="c8">
			<div class="forabg">
			<?php
			if(!empty($kategori)){
				foreach ($kategori as $kategori) { ?>
			<ul class="forabg-header">
				<li>
					<dl>
						<dt>
							<a href="<?php echo base_url('forum/');?>kategori.php?id=<?php echo $kategori['id']; ?>">Kategori : <?php echo $kategori['nama_kategori']; ?></a>
						</dt>
						<dd class="topics-posts">Topik/Thread</dd>
						<dd class="lastpost">Penulis</dd>
					</dl>
				</li>
			</ul>
			<?php 
			$sub = $forum->tampil_sub($kategori['id']);
			if(!empty($sub)){
				foreach ($sub as $sub_kat) {
			?>
			<ul class="forabg-content">
				<li>
					<dl class="icon" style="background-image: url(forum/img/kategori.svg);">
						<dt title="">
							<a href="<?php echo base_url('forum/')."kategori.php?id=".$kategori['id']."&sub=".$sub_kat['id']."";?>" class="forumtitle"><?php echo $sub_kat['nama_kategori'];?></a><br />
								<?php echo $sub_kat['deskripsi'];?>
						</dt>
						<?php
							$td = $forum->tampil_isi_sub_terakhir($sub_kat['id']);
						?>
						<dd class="topics-posts">
							<a href="<?php if(!empty($td['thread_id'])){echo base_url('forum/thread.php?id=').$td['thread_id'];}else{echo site_url();}?>">
								<?php if(!empty($td['judul'])){echo $td['judul'];}else{echo "Belum ada thread.";}?>
							</a>
						</dd>
						<dd class="lastpost"><span>
							oleh <a href="<?php if(!empty($td['user_id'])){echo base_url('web/profil.php?id=').$td['user_id'];}else{echo site_url();}?>" style="color: #AA0000;" class="username-coloured"><?php if(!empty($td['username'])){echo $td['username'];}else{echo "Belom ada.";}?></a>
							<a href="<?php if(!empty($td['thread_id'])){echo base_url('forum/thread.php?id=').$td['thread_id'];}else{echo site_url();}?>">
								<i class="fa  fa-chevron-right"></i>
							</a> <br />
							<?php if(!empty($td['tgl_post'])){echo tgl_indo(substr($td['tgl_post'], 0,10)).", ".substr($td['tgl_post'], 10);}else{echo tgl_indo(substr(date("Y-m-d H:i:s"), 0,10)).", ".substr(date("Y-m-d H:i:s"), 10);}?></span>
						</dd>
					</dl>
				</li>
			</ul>
			<?php
				}
			}else{
					//echo $kategori['nama_kategori'];
				}
			?>
			<br>
			<?php
				}
			}
			?>
			
			</div>
			


			<div class="secondary-block">
				<div class="secondary-block-header">
					<h3>
					Statistik
					</h3>
				</div>
			<div class="secondary-block-content">
				<p>
					Total post <strong><?php echo count($post);?></strong> &bull; Total thread <strong><?php echo count($thread);?></strong> &bull; Total pengguna <strong><?php echo count($pengguna);?></strong>
				</p>
			</div>
		</div>

		</div>

		<div class="c2">
			
			<?php 
			if(isset($_SESSION['user_id'])){
			?>
			<div class="secondary-block">
				<div class="secondary-block-header">
					<h3>
						Selamat datang di forum <?php $pengaturan->ambilPengaturan('nama');?>
					</h3>
				</div>
				<div class="secondary-block-content">
					Halo <a href="<?php echo base_url('web/profil.php?id='.$dataAkun['id']);?>"><?php echo $dataAkun['nama'];?></a>, kamu bisa sharing tentang mengenai lingkungan sekitar.
				</div>
			</div>
			<?php
			}	
			?>

			<?php
			$adaw = isset($_SESSION['hak_akses']);
			if(!$adaw){
			?>
			<form action="<?php echo base_url('forum/masuk.php');?>" method="post">
			<div class="mono-block">
				<div class="mono-block-header">
					<h3><a href="<?php echo base_url('forum/masuk.php'); ?>">Masuk</a></h3>
				</div>
				<div class="mono-block-content">
					<fieldset class="quick-login">
						<label for="username">Username:</label><input type="text" name="A_uname" id="username" size="10" class="inputbox" title="Username" />
						<label for="password">Password:</label><input type="password" name="A_pass" id="password" size="10" class="inputbox" title="Password" />
						<div class="btn btn-big">
							<input type="submit" name="Joss" value="Masuk"  />
						</div>
					</fieldset>
				</div>
			</div>
			</form>
			<?php
			}
			?>

			<div class="secondary-block">
				<div class="secondary-block-header">
					<h3>
						Top user thread di forum <?php $pengaturan->ambilPengaturan('nama');?>
					</h3>
				</div>
				<div class="secondary-block-content">
					<ul>
						<?php
							if(!empty($topUser_thread)){
								foreach ($topUser_thread as $topUser_thread) {
									echo "<li><a href='".base_url('web/profil.php?id='.$topUser_thread['id_user'])."'>".$topUser_thread['nama']."</a> - ".$topUser_thread['jumlah']." Thread</li>";
								}
							}else{
								echo "Belum ada user buat post!";
							}
						?>
					</ul>
				</div>
			</div>

			<div class="secondary-block">
				<div class="secondary-block-header">
					<h3>
						Top user post di forum <?php $pengaturan->ambilPengaturan('nama');?>
					</h3>
				</div>
				<div class="secondary-block-content">
					<ul>
						<?php
							if(!empty($topUser_post)){
								foreach ($topUser_post as $topUser_post) {
									echo "<li><a href='".base_url('web/profil.php?id='.$topUser_post['id_user'])."'>".$topUser_post['nama']."</a> - ".$topUser_post['jumlah']." Post</li>";
								}
							}else{
								echo "Belum ada user buat post!";
							}
						?>
					</ul>
				</div>
			</div>

		</div>
	</div>
	
</div>

</main>

<?php include "footer.php";?>
</body>
</html>