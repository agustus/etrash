<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-18 16:26:12
	**/
?>
	<base href="<?php echo base_asset();?>" />
	<meta charset="utf-8">
	<title>Forum <?php $pengaturan->ambilPengaturan('nama');?></title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="<?php $pengaturan->ambilPengaturan('deskripsi');?>">
    <meta name="author" content="<?php $pengaturan->ambilPengaturan('author');?>">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />  
	
	<script>
		function open_menu(id) {
			var open_menu_link = document.getElementById(id);
			if(open_menu_link.style.display == 'block')
				open_menu_link.style.display = 'none';
			else
				open_menu_link.style.display = 'block';
		}
	</script>