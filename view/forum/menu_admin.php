<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-18 17:31:21
	**/
?>
<div id="user-menu-a" class="wrap">
	<div id="page-search">
		<form action="cari.php" method="get" id="search">
		<fieldset>
			<input name="keywords" class="keywords" type="text" maxlength="128" title="Cari" placeholder="Cari" />
			<input value="&#xf002;" class="button" type="submit" />
		</fieldset>
		</form>
	</div>
	
	<ul>
				<li>
			<a class="w-icon" href="./adm/index.php?style=2&amp;sid=448b827c37e90e14490a9b93c128d768" data-tooltip="Administration Control Panel"><i class="fa fa-tachometer"></i></a>
		</li>
		
				<li>
			<a class="w-icon" href="./mcp.php?style=2&amp;i=main&amp;mode=front&amp;sid=448b827c37e90e14490a9b93c128d768" data-tooltip="Moderator Control Panel"><i class="fa fa-gavel"></i></a>
		</li>
		
			<li>
			<a class="w-icon" href="./ucp.php?style=2" title="User Control Panel" accesskey="e" data-tooltip="User Control Panel"><i class="fa fa-user"></i></a>
		</li>
				<li>
			<a class="w-icon" href="./ucp.php?style=2&amp;i=pm&amp;folder=inbox"><i class="fa fa-envelope"></i>
						</a>
		</li>
							
														<li>
				<a href="./ucp.php?style=2&amp;mode=logout&amp;sid=448b827c37e90e14490a9b93c128d768" title="Logout" accesskey="x">Logout</a>
			</li>
						</ul>
</div>