<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-18 16:27:18
	**/
?>
<footer id="page-footer-a">
	<div class="copyright">
		<div class="wrap">
			<?php $pengaturan->ambilPengaturan('footer');?>
		</div>

		<div>
			<a id="bottom" accesskey="z"></a>
		</div>
	</div>
</footer>