<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-21 18:09:47
	**/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
<head>

	<?php include 'meta.php';?>

	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,700italic" rel="stylesheet" type="text/css" />
	<link href="forum/font-awesome.css" rel="stylesheet" />

	<link href="forum/print.css" rel="stylesheet" media="print" title="printonly" />
	<link href="forum/stylesheet.css?assets_version=1" rel="stylesheet" media="screen, projection" />

</head>

<body class="section-search ltr">
	<?php include "header.php"; include "menu.php"; ?>
<main>
	<a class="hidden"></a>
	
<div id="page-body" class="wrap">

<h2><?php if(!empty($t_car)){echo "Ketemu ".count($t_car);}else{echo "Ga Ketemu";}?> thread yang sesuai key: <a href="<?php echo site_url();?>"><?php echo $key;?></a></h2>
 <p class="subtitle">Cari query: <strong><?php echo $key;?></strong></p>
	<p><a class="left-box left" href="javascript:history.go(-1)" title="Flashback">Kembali</a></p>


	<div class="topic-actions">

		<div class="pagination">
			<span class="matches"><?php if(!empty($t_car)){echo count($t_car)." Ketemu";}else{echo "Ga Ketemu";}?></span>
			<!--<span>Page <strong>1</strong> of <strong>1</strong></span>-->
		</div>
	</div>

		<?php
		if(!empty($t_car)){
			foreach ($t_car as $cari) {
		?>
		<div class="post bg2">

		<dl class="postprofile">
			<dt>
				<a href="<?php echo base_url('web/profil.php?id='.$cari['id_user']);?>" style="color: #AA0000;" class="username-coloured">
					<img src="<?php echo url_foto($cari['email']); ?>" alt="User gambar" height="80" width="80"><br>
					<?php echo $cari['username']." (".$cari['nama'].")";?>
				</a>
			</dt>
			<dd>
			<?php
				if($cari['lvl']==1){
					echo "Admin";
				}else{
					echo "User";
				}
			?>
			</dd>
			<dd>&nbsp;</dd>	
			<dd><strong>Posts:</strong> 6</dd>
			<dd><strong>Joined:</strong> <?php echo tgl_indo(substr($cari['date_register'], 0,10));?></dd>
			<dd>&nbsp;</dd>
				<dd>Kategori: <a href="<?php if($cari['parent_id']!=0){echo base_url('forum/kategori.php?id='.$cari['parent_id'].'&sub='.$cari['id_kategori']);}else{echo base_url('forum/kategori.php?id='.$cari['id_kategori']);}?>"><?php echo $cari['nama_kategori'];?></a></dd>
				<dd>Topik: <a href="<?php echo base_url('forum/thread.php?id='.$cari['id_thread']);?>"><?php echo $cari['judul'];?></a></dd>
			<dd>Dilihat: <strong><?php echo $cari['count'];?></strong>x</dd>
		</dl>

		<div class="postbody">
			<h3><a href="<?php echo base_url('forum/thread.php?id='.$cari['id_thread']);?>"><?php echo $cari['judul'];?></a></h3>
			<div class="content">
				<?php echo bbcode(substr($cari['isi'], 0, 232));?>  &#46;&#46;&#46;
			</div>
		</div>
	
			<div class="jump2post"><a href="<?php echo base_url('forum/thread.php?id='.$cari['id_thread']);?>">Ke Postingan</a></div>
	
		</div>

		<?php
			}
		}
		?>


	<a class="left-box left" href="javascript:history.go(-1)" title="Flesbek" accesskey="r">Kembali</a>

	<div class="clear"></div>

</div>

</main>
	<?php include "footer.php";?>
</body>
</html>