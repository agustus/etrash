<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-13 21:12:29
	**/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
<head>
	<?php include 'meta.php';?>

	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,700italic" rel="stylesheet" type="text/css" />
	<link href="forum/font-awesome.css" rel="stylesheet" />

	<link href="forum/print.css" rel="stylesheet" media="print" title="printonly" />
	<link href="forum/stylesheet.css?assets_version=1" rel="stylesheet" media="screen, projection" />
</head>

<body class="section-viewtopic ltr">
<?php include "header.php"; include "menu.php"; ?>
<main>
	<a class="hidden"></a>
	
<div id="second-menu">
<div class="wrap">
	<ul>
		<li>
			<a href="<?php echo base_url('forum');?>" accesskey="h">Beranda</a>
		</li>

		<?php 
		if($thread['parent_id']!=0){
			echo '<li class="legend"><a href="'.base_url('forum/kategori.php?id=').$parent_kategori['id'].'">
				'.$parent_kategori['nama_kategori']."
				</a></li>";
			echo '<li class="legend"><a href="'.base_url('forum/kategori.php?id=').$parent_kategori['id'].'&sub='.$thread['id'].'">
			'.$thread['nama_kategori'].'
			</a></li>'; 
		}else{
			echo '<li class="legend"><a href="'.base_url('forum/kategori.php?id=').$thread['id'].'">
			'.$thread['nama_kategori'].'
			</a></li>';
		}
			 
		?>

		<li class="legend">
			<a href="<?php echo site_url();?>"><?php echo $thread['judul'];?></a>
		</li>
	</ul>
</div>
</div>

<div id="page-body" class="wrap">

<h2><a href="<?php echo site_url(); ?>"><?php echo $thread['judul'];?></a></h2>

<div class="topic-actions">

			<div class="btn btn-reply">
			<a href="<?php echo base_url('forum/balas.php?thread='.$id_thread);?>" title="Post a reply"><i class="fa fa-reply"></i>Kirim balasan</a>
		</div>
	
	
			<div class="pagination">
						<span class="total"><?php $adaw = count($bls_thread)+1; echo $adaw." Postingan";?></span>
									<span>Page <strong>1</strong> of <strong>1</strong></span>
								</div>
	
</div>
<div class="clear"></div>


		<div id="p0" class="post bg2 online">
			<dl class="postprofile" id="profile4">
			<dt>
				<a href="<?php echo base_url('web/profil.php?id='.$thread['id_user']);?>" style="color: #AA0000;" class="username-coloured">
					<img src="<?php echo url_foto($thread['email']); ?>" alt="User gambar" height="80" width="80"><br>
					<?php echo $thread['username'];?> (<?php echo $thread['nama'];?>)
				</a>
			</dt>
			<dd>
			<?php
				if($thread['lvl']==1){
					echo "Admin";
				}else{
					echo "User";
				}
			?>
			</dd>
			<dd>&nbsp;</dd>
			<?php
				$tot_poss = $forum->tampil_post_user($thread['id_user']);
			?>
			<dd><strong>Posts:</strong> <?php echo count($tot_poss);?></dd>
			<dd><strong>Joined:</strong> <?php echo tgl_indo(substr($thread['date_register'], 0,10));?></dd>		
				
			</dl>
			
			<div class="postbody">
				<ul class="post-icons">
					<li class="edit-icon" <?php if(isset($_SESSION['user_id'])){$iduser=$_SESSION['user_id'];if($iduser!=$thread['id_user']){echo "style='display:none'";}}else{echo "style='display:none'";}?>>
						<a href="<?php echo base_url('forum/ubah.php?post=').$thread['id_thread'];?>" title="Ubah postingan"><i class="fa fa-pencil"></i><span>Ubah postingan</span></a>
					</li>
					<li class="quote-icon">
						<a href="<?php echo base_url('forum/balas.php?thread='.$id_thread.'&post=').$thread['id_thread'];?>" title="Bales pake quote"><i class="fa fa-quote-right"></i><span>Balas pake quote</span></a>
					</li>
				</ul>
					
			<h3><a href="<?php echo site_url('#p0');?>"><?php echo $thread['judul'];?></a></h3>
			<p class="date"><i class="fa fa-file-o"></i> <?php echo tgl_indo(substr($thread['tgl_post'], 0,10)).", ".substr($thread['tgl_post'], 10);?></p>

			
			<div class="content">
				<?php echo bbcode(strip_tags($thread['isi']));?> 
			</div>

			<div class="date-mod" <?php if(empty($thread['tgl_modif'])){echo "style='display:none'"; }?>>
				<i class="fa fa-file-o"></i> <?php if(!empty($thread['tgl_modif'])){echo "Di perbarui ".tgl_indo(substr($thread['tgl_modif'], 0,10)).", ".substr($thread['tgl_modif'], 10);}?>
			</div>
								
			</div>

		<div class="back2top"><a href="<?php echo site_url('#wrap');?>" class="top" title="Atas">Top</a></div>
		<div class="clear"></div>
	</div>


	<hr class="divider" />

	<?php
	if(!empty($bls_thread)){
		$no = 1;
		foreach ($bls_thread as $bls_thread) {
	?>
	<div id="p<?php echo $no;?>" class="post bg2 online">
		<dl class="postprofile" id="profile6">
			<dt>
				<a href="<?php echo base_url('web/profil.php?id='.$bls_thread['id_user']);?>" style="color: #AA0000;" class="username-coloured">
					<img src="<?php echo url_foto($bls_thread['email']); ?>" alt="User gambar" height="80" width="80"><br>
					<?php echo $bls_thread['username']." (".$bls_thread['nama'].")";?>
				</a>
			</dt>
			<dd>
			<?php
				if($bls_thread['lvl']==1){
					echo "Admin";
				}else{
					echo "User";
				}
			?>
			</dd>
			<dd>&nbsp;</dd>
			<?php
				$tot_pos = $forum->tampil_post_user($bls_thread['id_user']);
			?>	
			<dd><strong>Posts:</strong> <?php echo count($tot_pos);?></dd>
			<dd><strong>Joined:</strong> <?php echo tgl_indo(substr($bls_thread['date_register'], 0,10));?></dd>				
		</dl>
	
		<div class="postbody">
			<ul class="post-icons">
				<li class="edit-icon" <?php if(isset($_SESSION['user_id'])){$iduser=$_SESSION['user_id'];if($iduser!=$bls_thread['id_user']){echo "style='display:none'";}}else{echo "style='display:none'";}?>>
					<a href="<?php echo base_url('forum/ubah.php?post=').$bls_thread['id_thread'];?>" title="Ubah postingan"><i class="fa fa-pencil"></i><span>Ubah postingan</span></a>
				</li>
				<li class="quote-icon">
					<a href="<?php echo base_url('forum/balas.php?thread='.$id_thread.'&post=').$bls_thread['id_thread'];?>" title="Bales pake quote"><i class="fa fa-quote-right"></i><span>Balas pake quote</span></a>
				</li>
			</ul>
					
			<h3><a href="<?php echo site_url('#p'.$no);?>">
			<?php if(!empty($bls_thread['judul'])){echo $bls_thread['judul'];}else{echo "Re : ".$thread['judul'];}?></a></h3>
			<p class="date"><i class="fa fa-file-o"></i> <?php echo tgl_indo(substr($thread['tgl_post'], 0,10)).", ".substr($thread['tgl_post'], 10);?></p>

			
			<div class="content">
				<?php echo bbcode(strip_tags($bls_thread['isi']));?> 
			</div>

			<div class="date-mod" <?php if(empty($bls_thread['tgl_modif'])){echo "style='display:none'"; }?>>
				<i class="fa fa-file-o"></i> <?php if(!empty($bls_thread['tgl_modif'])){echo "Di perbarui ".tgl_indo(substr($bls_thread['tgl_modif'], 0,10)).", ".substr($bls_thread['tgl_modif'], 10);}?>
			</div>
								
		</div>

		<div class="back2top"><a href="<?php echo site_url('#wrap');?>" class="top" title="Atas">Top</a></div>
		<div class="clear"></div>
	</div>

	<hr class="divider" />
	<?php
		$no++;
		}
	}
	?>

	<a href="<?php echo base_url('forum');?>" class="left-box left" accesskey="r">Kembali Ke Beranda</a>

	<div class="clear"></div>


</div>

</main>
<?php include "footer.php";?>
</body>
</html>