<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-18 18:00:34
	**/
?>
<div id="user-menu-a" class="wrap">
	
	<div id="page-search">
		<form action="<?php echo base_url('forum/cari.php');?>" method="get" id="search">
		<fieldset>
			<input name="keywords" class="keywords" type="text" maxlength="128" title="Cari" placeholder="Cari" />
			<input value="&#xf002;" class="button" type="submit" />
		</fieldset>
		</form>
	</div>
	

	<ul>
		<li>
			<a class="w-icon-text" href="<?php echo base_url('web');?>" title="Ke Web" accesskey="x">Web</a>
		</li>
		<?php
		if(isset($_SESSION['hak_akses'])){
        ?>
		<li>
			<a class="w-icon-text" href="<?php echo base_url('forum/index.php?keluar');?>" title="Kaluar" accesskey="x"><i class="fa fa-sign-out"></i><span>Keluar</span></a>
		</li>
		<?php
		}else{
		?>
		<li>
			<a class="w-icon-text" href="<?php echo base_url('forum/masuk.php');?>" title="Login" accesskey="x"><i class="fa fa-sign-in"></i><span>Masuk</span></a>
		</li>	
		<li>
			<a class="w-icon-text" href="<?php echo base_url('forum/daftar.php');?>" title="Login" accesskey="x"><i class="fa fa-lock"></i><span>Daftar</span></a>
		</li>	
		<?php
			}
		?>		
	</ul>
</div>