<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-18 17:30:40
	**/
?>
<header id="page-header-a" class="wrap">
	<a id="top" accesskey="t"></a>
	<a id="wrap"></a>
	<div id="logo">
		<a href="<?php echo base_url('forum');?>" title="Board index"><img src="forum/img/logo_forum.png" alt="" title="" height="65" width="195"></a>
		<p class="skiplink"><a href="<?php echo base_url('forum');?>#start_here">Skip to content</a></p>
	</div>

	<nav id="page-nav">
		<ul>
		</ul>
	</nav>

	<div class="rmenu" onclick="open_menu('nav-menu');"><i class="fa fa-bars"></i></div>

	<ul id="nav-menu">
		<li>
			<a class="w-icon-text" href="<?php echo base_url('web');?>" title="Ke Web" accesskey="x"><span>Web</span></a>
		</li>
		<?php
		if(isset($_SESSION['hak_akses'])){
        ?>
		<li>
			<a class="w-icon-text" href="<?php echo base_url('forum/index.php?keluar');?>" title="Kaluar" accesskey="x"><i class="fa fa-sign-out"></i><span>Keluar</span></a>
		</li>
		<?php
		}else{
		?>
		<li>
			<a class="w-icon-text" href="<?php echo base_url('forum/masuk.php');?>" title="Login" accesskey="x"><i class="fa fa-sign-in"></i><span>Masuk</span></a>
		</li>	
		<li>
			<a class="w-icon-text" href="<?php echo base_url('forum/daftar.php');?>" title="Login" accesskey="x"><i class="fa fa-lock"></i><span>Daftar</span></a>
		</li>	
		<?php
			}
		?>		
	</ul>
</header>