<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-21 19:00:23
	**/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
<head>

	<?php include "meta.php";?>

	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,700italic" rel="stylesheet" type="text/css" />
	<link href="forum/font-awesome.css" rel="stylesheet" />

	<link href="forum/print.css" rel="stylesheet" media="print" title="printonly" />
	<link href="forum/stylesheet.css?assets_version=1" rel="stylesheet" media="screen, projection" />

</head>

<body class="section-posting ltr">
<?php include "header.php"; include "menu.php"; ?>
<main>
	<a class="hidden"></a>
	

<div id="page-body" class="wrap">

<script type="text/javascript">
// <![CDATA[
	onload_functions.push('document.getElementById("username").focus();');
// ]]>
</script>

<form action="<?php echo base_url('forum/masuk.php');?>" method="post" id="login">
<div class="panel">
	<div class="content">
		<h2>Kamu butuh akses untuk membalas topik.</h2>

		<fieldset class="fields1">
		
		<dl>
			<dt><label for="username">Username</label></dt>
			<dd><input type="text" tabindex="1" name="A_uname" id="username" size="25" value="" class="inputbox" /></dd>
		</dl>
		<dl>
			<dt><label for="password">Password</label></dt>
			<dd><input type="password" tabindex="2" id="password" name="A_pass" size="25" class="inputbox" /></dd>
			
		</dl>

		<input type="hidden" name="redirect" value="./posting.php?style=14&amp;mode=reply&amp;f=2&amp;t=4&amp;style=14&amp;sid=c0edb93c74767fe59f911dcb28bdd40f" />

		<dl>
			<dt>&nbsp;</dt>
			<dd>
				<div class="btn btn-big"><input type="submit" name="Joss" value="Masuk" tabindex="6" /></div>
			</dd>
		</dl>
		</fieldset>
	</div>
</div>




</form>

</div>

</main>
<?php include "footer.php";?>
</body>
</html>