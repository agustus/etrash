<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-14 09:31:11
	**/
?>

	<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
<head>
	<?php include 'meta.php';?>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,700italic" rel="stylesheet" type="text/css" />
	<link href="forum/font-awesome.css" rel="stylesheet" />

	<link href="forum/print.css" rel="stylesheet" media="print" title="printonly" />
	<link href="forum/stylesheet.css?assets_version=1" rel="stylesheet" media="screen, projection" />
</head>

<body class="section-posting ltr">
<?php include "header.php"; include "menu.php"; ?>
<main>
	<a class="hidden"></a>
	
<div id="page-body" class="wrap">

	<?php
	if($stt=='bales'){
		echo "<h2><a href='".base_url('forum/thread.php?id=').$thread."'>".$bls['nama_kategori'];
		if(!empty($kategori['parent_id'])){
			if($kategori['parent_id']!=0){
				echo " -> ".$parent_kategori['nama_kategori'];
			}
		}
		echo " -> ".$bls['judul']."</a></h2>";
	}else{
		echo "<h2><a href='".site_url()."'>Kategori : ".$bls['nama_kategori'];
		if(!empty($kategori['parent_id'])){
			if($kategori['parent_id']!=0){
				echo " -> ".$parent_kategori['nama_kategori'];
			}
		}
		echo "</a></h2>";
	}
	?>

		<div id="tabs">
			<ul>
				<li class="activetab">
					<a href="<?php echo site_url();?>">
					<span>
						<h3>
						<?php
						if($stt=='bales'){
							echo "Balas Thread ".$bls['judul'];
						}else{
							echo "Buat Thread Baru";
						}
						?>
						</h3>
					</span>
					</a>
				</li>	
			</ul>
		</div>
<form id="postform" method="post" action="<?php echo site_url();?>" enctype="multipart/form-data">

<div class="panel" id="postingbox">
	<div class="content">
		<script type="text/javascript">
// <![CDATA[
	onload_functions.push('apply_onkeypress_event()');
// ]]>
</script>

<fieldset class="fields1">
	
	<dl style="clear: left;">
		<dt><label for="subject">Judul:</label></dt>
		<dd>
			<input type="text" size="45" maxlength="64" tabindex="2" name="txtJudul" class="inputbox autowidth" <?php echo "value='".$tred['judul']."'";?>>
		</dd>
	</dl>
		
	
<script type="text/javascript">
// <![CDATA[
	var form_name = 'postform';
	var text_name = 'txtIsi';
	var load_draft = false;
	var upload = false;

	// Define the bbCode tags
	var bbcode = new Array();
	var bbtags = new Array('[b]','[/b]','[i]','[/i]','[u]','[/u]','[quote]','[/quote]','[code]','[/code]','[list]','[/list]','[list=]','[/list]','[img]','[/img]','[url]','[/url]','[size=]','[/size]','[br]');
	var imageTag = false;

	// Helpline messages
	var help_line = {
		b: 'Bold text: [b]text[/b]',
		i: 'Italic text: [i]text[/i]',
		u: 'Underline text: [u]text[/u]',
		q: 'Quote text: [quote]text[/quote]',
		c: 'Code display: [code]code[/code]',
		l: 'List: [list][li]text[/li][/list]',
		o: 'Ordered list: e.g. [list=1][li]First point[/li][/list] or [list=a][li]Point a[/li][/list]',
		p: 'Masukkan Gambar: [img]http://url_gambar[/img]',
		w: 'Masukkan URL: [url]http://url[/url] or [url=http://url]URL text[/url]',
		a: 'Inline uploaded attachment: [attachment=]filename.ext[/attachment]',
		s: 'Font colour: [color=red]text[/color] or [color=#FF0000]text[/color]',
		f: 'Font size: [size=85]small text[/size]',
		y: 'List: Add list element',
		f: 'Enter: [br]'
			}

	var panels = new Array('options-panel', 'attach-panel', 'poll-panel');


// ]]>
</script>
		
<script type="text/javascript" src="forum/editor.js"></script>

<div id="format-buttons">
	<div>
	<input type="button" class="button2" accesskey="b" name="addbbcode0" value="B" style="font-weight:bold; width: 30px" onclick="bbstyle(0)" title="Bold text: [b]text[/b]" />
	<input type="button" class="button2" accesskey="i" name="addbbcode2" value="i" style="font-style:italic; width: 30px" onclick="bbstyle(2)" title="Italic text: [i]text[/i]" />
	<input type="button" class="button2" accesskey="u" name="addbbcode4" value="u" style="text-decoration: underline; width: 30px" onclick="bbstyle(4)" title="Underline text: [u]text[/u]" />
	<input type="button" class="button2" accesskey="q" name="addbbcode6" value="Quote" style="width: 50px" onclick="bbstyle(6)" title="Quote text: [quote]text[/quote]" />
	<input type="button" class="button2" accesskey="c" name="addbbcode8" value="Code" style="width: 40px" onclick="bbstyle(8)" title="Code display: [code]code[/code]" />
	<input type="button" class="button2" accesskey="l" name="addbbcode10" value="List" style="width: 40px" onclick="bbstyle(10)" title="List: [list][li]text[/li][/list]" />
	<input type="button" class="button2" accesskey="o" name="addbbcode12" value="List=" style="width: 40px" onclick="bbstyle(12)" title="Ordered list: e.g. [list=1][li]First point[/li][/list] or [list=a][li]Point a[/li][/list]" />
	<input type="button" class="button2" accesskey="y" name="addlistitem" value="[li]" style="width: 40px" onclick="bbstyle(-1)" title="List item: [li]text[/li]" />
	<input type="button" class="button2" accesskey="p" name="addbbcode14" value="Img" style="width: 40px" onclick="bbstyle(14)" title="Insert image: [img]http://image_url[/img]" />
	<input type="button" class="button2" accesskey="w" name="addbbcode16" value="URL" style="text-decoration: underline; width: 40px" onclick="bbstyle(16)" title="Insert URL: [url]http://url[/url] or [url=http://url]URL text[/url]" />
	<select name="addbbcode20" onchange="bbfontstyle('[size=' + this.form.addbbcode20.options[this.form.addbbcode20.selectedIndex].value + ']', '[/size]');this.form.addbbcode20.selectedIndex = 2;" title="Font size: [size=85]small text[/size]">
		<option value="50">Tiny</option>
		<option value="85">Small</option>
		<option value="100" selected="selected">Normal</option>
		<option value="150">Large</option>
		<option value="200">Huge</option>
	</select>	
	
	</div>
</div>

	<div id="smiley-box">
		<strong>Emot</strong><br />
			<a href="#" onclick="insert_text(':D', true); return false;"><img src="<?php echo base_asset('img/emot/nyengir.gif');?>" width="17" height="17" alt=":D" title="Nyengir" /></a>
			<a href="#" onclick="insert_text(':P', true); return false;"><img src="<?php echo base_asset('img/emot/p.gif');?>" width="17" height="17" alt=":p" title="Wlee" /></a>
			<a href="#" onclick="insert_text(':(', true); return false;"><img src="<?php echo base_asset('img/emot/sedih.gif');?>" width="17" height="17" alt=":(" title="Sedih" /></a>
			<a href="#" onclick="insert_text(':)', true); return false;"><img src="<?php echo base_asset('img/emot/senyum.gif');?>" width="17" height="17" alt=":)" title="Senyum" /></a>
			<a href="#" onclick="insert_text(':@', true); return false;"><img src="<?php echo base_asset('img/emot/marah.gif');?>" width="17" height="17" alt=":@" title="Ngambek" /></a>
			<a href="#" onclick="insert_text('T_T', true); return false;"><img src="<?php echo base_asset('img/emot/mewek.gif');?>" width="17" height="17" alt="T_T" title="Mewek" /></a>
			<a href="#" onclick="insert_text(';)', true); return false;"><img src="<?php echo base_asset('img/emot/kedip.gif');?>" width="17" height="17" alt=";)" title="Ngicep" /></a>
			<a href="#" onclick="insert_text(':sip:', true); return false;"><img src="<?php echo base_asset('img/emot/sip.gif');?>" width="17" height="17" alt=":sip" title="Sip" /></a>
			<a href="#" onclick="insert_text(':takut:', true); return false;"><img src="<?php echo base_asset('img/emot/takut.gif');?>" width="17" height="17" alt=":takut" title="Sieun" /></a>
			<a href="#" onclick="insert_text(':shutup:', true); return false;"><img src="<?php echo base_asset('img/emot/shutup.gif');?>" width="17" height="17" alt=":shutup" title="Cicing" /></a>
			<a href="#" onclick="insert_text(':shakehand:', true); return false;"><img src="<?php echo base_asset('img/emot/shakehand.gif');?>" width="17" height="17" alt=":shakehand" title="Salaman" /></a>
			<a href="#" onclick="insert_text(':_pm:', true); return false;"><img src="<?php echo base_asset('img/emot/pm.gif');?>" width="17" height="17" alt=":pm" title="PrivMes" /></a>
			<a href="#" onclick="insert_text(':_peace:', true); return false;"><img src="<?php echo base_asset('img/emot/peace.gif');?>" width="17" height="17" alt=":peace" title="Piss" /></a>
			<a href="#" onclick="insert_text(':no:', true); return false;"><img src="<?php echo base_asset('img/emot/(n).gif');?>" width="17" height="17" alt="(n)" title="No" /></a>
			<a href="#" onclick="insert_text(':yes:', true); return false;"><img src="<?php echo base_asset('img/emot/(y).gif');?>" width="17" height="17" alt="(y)" title="Yes" /></a>
			<a href="#" onclick="insert_text(':berduka:', true); return false;"><img src="<?php echo base_asset('img/emot/beduka.gif');?>" width="17" height="17" alt=":berduka" title="Berduka" /></a>
			<a href="#" onclick="insert_text(':belo:', true); return false;"><img src="<?php echo base_asset('img/emot/belo.gif');?>" width="17" height="17" alt=":belo" title="Woo Belo" /></a>
			<a href="#" onclick="insert_text(':bingung:', true); return false;"><img src="<?php echo base_asset('img/emot/bingung.gif');?>" width="17" height="17" alt=":bingung" title="Bingung" /></a>
			<a href="#" onclick="insert_text(':cd:', true); return false;"><img src="<?php echo base_asset('img/emot/cd.gif');?>" width="17" height="17" alt=":cd" title="Cape Deh" /></a>
			<a href="#" onclick="insert_text(':genit:', true); return false;"><img src="<?php echo base_asset('img/emot/genit.gif');?>" width="17" height="17" alt=":genit" title="Genit" /></a>
			<a href="#" onclick="insert_text(':geram:', true); return false;"><img src="<?php echo base_asset('img/emot/geram.gif');?>" width="17" height="17" alt=":geram" title="Geram" /></a>
			<a href="#" onclick="insert_text(':hammer:', true); return false;"><img src="<?php echo base_asset('img/emot/hammer.gif');?>" width="17" height="17" alt=":hammer" title="Hammer" /></a>
			<a href="#" onclick="insert_text(':heran:', true); return false;"><img src="<?php echo base_asset('img/emot/heran.gif');?>" width="17" height="17" alt=":heran" title="Heran" /></a>
			<a href="#" onclick="insert_text(':lol:', true); return false;"><img src="<?php echo base_asset('img/emot/lol.gif');?>" width="17" height="17" alt=":lol" title="lol" /></a>
			<a href="#" onclick="insert_text(':loveindonesia:', true); return false;"><img src="<?php echo base_asset('img/emot/loveindonesia.gif');?>" width="17" height="17" alt=":loveindonesia" title="Loveindonesia" /></a>
			<a href="#" onclick="insert_text(':ngantuk:', true); return false;"><img src="<?php echo base_asset('img/emot/ngantuk.gif');?>" width="17" height="17" alt=":ngantuk" title="Tunduh" /></a>
			<a href="#" onclick="insert_text(':nyerah:', true); return false;"><img src="<?php echo base_asset('img/emot/nyerah.gif');?>" width="17" height="17" alt=":nyerah" title="Nyerah" /></a>
			<a href="#" onclick="insert_text(':nohope:', true); return false;"><img src="<?php echo base_asset('img/emot/nohope.gif');?>" width="17" height="17" alt=":nohope" title="Nohope" /></a>
			<a href="#" onclick="insert_text(':malu:', true); return false;"><img src="<?php echo base_asset('img/emot/malu.gif');?>" width="17" height="17" alt=":malu" title="Isin" /></a>				
		<hr />		
		BBCode is <em>ON</em><br />
		[img] is <em>ON</em><br />
		[url] is <em>ON</em><br />
		Emoticon are <em>ON</em>		
	</div>

	<div id="message-box">
		<textarea name="txtIsi" id="txtIsi" rows="15" cols="76" tabindex="4" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" onfocus="initInsertions();" class="inputbox">
<?php if(!empty($tred['isi'])){ echo "[quote=".$tred['username']." menulis :]".strip_tags($tred['isi'])."[/quote]";}?>
</textarea>
	</div>
</fieldset>


			</div>
	</div>
	
	
		<fieldset class="submit-buttons">
			
			<div class="btn btn-big">
				<input type="submit" accesskey="k" tabindex="7" name="Post" value="Post">
			</div>

			<div class="btn btn-big">
				<a href="javascript:history.go(-1)">Kembali</a>
			</div>

		</fieldset>



</div>

</main>
<?php include "footer.php";?>
</body>
</html>