<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include 'meta.php'; ?>
<!-- cssnya -->
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/tabel.css" rel="stylesheet" type="text/css" />
</head>

<body bgcolor="#EAECF0">
<div id="header">Vote</div><!--header-->
<div id="menu">
<div id="posisi_gambar">
<div id="gambar">
</div><!--gambar-->
</div><!--posisi gambar-->
<div id="posisi">
<?php include 'menu.php';?>
</div><!--menu-->
</div><!--posisi-->
<div id="isi">
	<div class="tabel" >
    	<?php
    	echo "<table border='1' cellpadding='5'>
		<tr>
			<td>No</td>
			<td>Nama Lokasi</td>
			<td>Tanggal</td>
			<td>Latitude</td>
			<td>Longtitude</td>
			<td>Aksi</td>
		</tr>";
		if(empty($wadahVote)){
			echo "<td colspan='6'>Belum ada vote.</td>";
		}else{
		$i = 1;
		foreach ($wadahVote as $data) {
		echo "<tr><td>" . $i . "</td>
			<td>".$data['nama_lokasi']."</td>
			<td>".$data['tgl_vote']."</td>
			<td>".$data['latitude']."</td>
			<td>".$data['longtitude']."</td>
			<td>";
		if($data['stt']==1){echo "<a href='".site_url('?status=').$data['id']."'>Tidak Aktif</a>";}else{echo "<a href='".site_url('?status=').$data['id']."'>Aktif</a>";}
		echo " | 
				<a href='".site_url('?ubah&id=').$data['id']."'>Ubah</a> 
				| 
				<a href='".site_url('?hapus&id=').$data['id']."'>Hapus</a>
			</td>
		</tr>";
		$i++;
		}
	}
		echo "</table>";
    	?>        
    </div>
<br />

<center><a href="<?php echo site_url('?buat');?>">Buat Vote</a></center>
</div><!-- kotak -->

</div><!--isi-->
<?php include 'footer.php';?><!-- footer -->
</body>
</html>