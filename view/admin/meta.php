<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-25 12:45:09
	**/
?>
	<base href="<?php echo base_asset();?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="description" content="<?php $pengaturan->ambilPengaturan('deskripsi');?>">
    <meta name="author" content="<?php $pengaturan->ambilPengaturan('author');?>">
	<title>Admin</title>

	<link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />