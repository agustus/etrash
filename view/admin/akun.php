<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include 'meta.php';?>
<!-- cssnya -->
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/tabel.css" rel="stylesheet" type="text/css" />
</head>

<body bgcolor="#EAECF0">
<div id="header">Akun</div><!--header-->
<div id="menu">
<div id="posisi_gambar">
<div id="gambar">
</div><!--gambar-->
</div><!--posisi gambar-->
<div id="posisi">
<?php include 'menu.php';?>
</div><!--menu-->
</div><!--posisi-->
<div id="isi">
	<div class="tabel" >
    	<?php
    	echo "<table border='1' cellpadding='5'>
		<tr>
			<td>No</td>
			<td>Username</td>
			<td>Nama</td>
			<td>Email</td>
			<td>Twitter</td>
			<td>Pin BBM</td>
			<td>Tgl Daftar</td>
			<td>Status</td>
			<td>Aksi</td>
		</tr>";
		if(empty($wadahAkun)){
			echo "<td colspan='9'>Belum ada anggota.</td>";
		}else{
		$i = 1;
		foreach ($wadahAkun as $data) {
		echo "<tr><td>" . $i . "</td>
			<td>".$data['username']."</td>
			<td>".$data['nama']."</td>
			<td>".$data['email']."</td>
			<td>".$data['twitter']."</td>
			<td>".$data['pin_bbm']."</td>
			<td>".$data['date_register']."</td>
			<td>";
		if($data['lvl']==1){echo "Admin";}else{echo "User";}
		echo "</td>
			<td>";
		if($data['stt']==1){echo "<a href='".site_url('?status=').$data['id']."'>Tidak Aktif</a>";}else{echo "<a href='".site_url('?status=').$data['id']."'>Aktif</a>";}
		echo "
			</td>
		</tr>";
		$i++;
		}
	}
		echo "</table>";
    	?>   
    </div>
</div><!-- kotak -->
</div><!--isi-->
<?php include 'footer.php';?><!-- footer -->
</body>
</html>