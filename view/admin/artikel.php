<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include 'meta.php';?>
<!-- cssnya -->
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/tabel.css" rel="stylesheet" type="text/css" />
</head>

<body bgcolor="#EAECF0">
<div id="header">Artikel</div><!--header-->
<div id="menu">
<div id="posisi_gambar">
<div id="gambar">
</div><!--gambar-->
</div><!--posisi gambar-->
<div id="posisi">
<?php include 'menu.php';?>
</div><!--menu-->
</div><!--posisi-->
<div id="isi">
	<div class="tabel" >
    <?php
    	echo "<table border='1' cellpadding='5'>
		<tr>
			<td>No</td>
			<td>Judul Artikel</td>
			<td>Autdor</td>
			<td>Tanggal</td>
			<td>Aksi</td>
		</tr>";
		if(empty($wadahArtikel)){
			echo "<td colspan='5'>Tidak ada artikel</td>";
		}else{
		$i = 1;
		foreach ($wadahArtikel as $data) {
		echo "<tr><td>" . $i . "</td>
			<td>".$data['judul']."</td>
			<td>".$data['nama']."</td>
			<td>".$data['tgl']."</td>
			<td>
				<a href='".site_url('?ubah&id=').$data['id_artikel']."'><img src='img/update.png'> Ubah</a> 
				| 
				<a href='".site_url('?hapus&id=').$data['id_artikel']."'><img src='img/delete.png'> Hapus</a>
			</td>
		</tr>";
		$i++;
		}
		}
		echo "</table>";
    ?>            
    </div>
    <br />
<center><a href="<?php echo base_url('admin/artikel.php?tambah'); ?>">Tambah Artikel</a></center>
</div><!-- kotak -->

</div><!--isi-->
<div id="footer">&copy; SMKN 2 Cimahi</div><!-- footer -->
</body>
</html>
