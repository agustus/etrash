<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include "meta.php";?>
	<!-- cssnya -->
	<link href="css/admin.css" rel="stylesheet" type="text/css" />
	<link href="css/reset.css" rel="stylesheet" type="text/css" />
	<link href="css/tabel.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
    
    <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
    <style>
    section{width:672px;margin:20px auto;background:rgba(255,255,255,.7);border-radius:8px}

#gallery ul{
    list-style-type:none;
}
#gallery ul li{
    display:inline-block;
    padding:5px;
}
#gallery ul li img:hover{
    opacity:0.8;
}
@import url('normalize.css');
*{padding:0;margin:0}
.twd-top{
    height:20px;
    background:rgba(0,0,0,.5);
    color:#ddd;
    margin-bottom:20px;
    padding-top:5px;
    font-size:11px;
    font-family:verdana, arial, Times New Roman;
    text-transform:uppercase;
    border-bottom:1px solid rgba(0,0,0,.1);
}
.twd-top a{
    text-decoration:none;
    color:#f4f4f4;
}
.twd-top a:hover{color:#fff}
.twd-top-left{
    display:block;
    float:left;
    padding-left:10px;
}
.twd-top-right{
    display:block;
    float:right;
    padding-right:10px;
}
.twd-clear{
    clear:both;
    padding:0;
    margin:0;
}

.demo-container{
    /*min-height:1000px;*/
}
    </style>
</head>

<body bgcolor="#EAECF0">
<div id="header">Laporan Pengaduan</div><!--header-->
<div id="menu">
<div id="posisi_gambar">
<div id="gambar">
</div><!--gambar-->
</div><!--posisi gambar-->
<div id="posisi">
<?php include "menu.php";?>
</div><!--menu-->
</div><!--posisi-->
<div id="isi">
	<div class="tabel">
    <div id='gallery'>
		<?php
    	echo "<table border='1' cellpadding='5'>
		<tr>
			<td>No</td>
			<td>Nama Laporan</td>
			<td>Deskripsi Laporan</td>
			<td>Foto</td>
			<td>Tanggal</td>
			<td>Aksi</td>
		</tr>";
        if(empty($data)){
            echo "<td colspan='6'>Belum ada laporan.</td>";
        }else{
		$i = 1;
		foreach ($data as $data) {
		echo "<tr><td>" . $i . "</td>
			<td>".$data['nama_laporan']."</td>
			<td title='".$data['deskripsi_laporan']."'>".substr($data['deskripsi_laporan'], 0,10)."..</td>
			<td><li style='list-style:none'><a href='".$data['screenshoot']."'>".$data['screenshoot']."</a></li></td>
			<td>".$data['tanggal']."</td>
			<td>";
		if($data['stt_laporan']==1){
            echo "<button onclick=\"location.href='".site_url('?status=').$data['id_laporan']."'\">Tidak Aktif</button>";
        }else{
            echo "<button onclick=\"location.href='".site_url('?status=').$data['id_laporan']."'\">Aktif</button>";
        }
		echo "
			</td>
		</tr>";
		$i++;
		}
    }
		echo "</table>";
    	?>
        </div>
    </div>
	<br />
</div><!-- kotak -->

</div><!--isi-->
<?php include "footer.php";?><!-- footer -->
</body>
</html>
