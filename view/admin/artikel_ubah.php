<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 10:09:23
	**/
//Ngambil url dari pengaturan
$url = $pengaturan->nyandak_asset('ckeditor/ckeditor.js');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include 'meta.php'; ?>
<!-- cssnya -->
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/tabel.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $url; ?>"></script>
</head>

<body bgcolor="#EAECF0">
<div id="header">Ubah Artikel</div><!--header-->
<div id="menu">
<div id="posisi_gambar">
<div id="gambar">
</div><!--gambar-->
</div><!--posisi gambar-->
<div id="posisi">
<?php include 'menu.php';?>
</div><!--menu-->
</div><!--posisi-->
</div><!-- kotak -->
<div id="isi">
<br />
<div id="posisi_ck">
<hr><script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<form action="<?php echo base_url('admin/artikel.php?ubah');?>" method="post">
<input type="hidden" name="A_id" value="<?php echo $id_artikel;?>">
Judul : <input type="text" name="A_judul" value="<?php echo $dataArtikel['judul'];?>"><br>
Isi : <textarea class="ckeditor" name="A_isi"><?php echo $dataArtikel['isi'];?></textarea><br>
Status : 
<select name="A_stt">
	<?php 
	$stt = $dataArtikel['stt'];
	?>
	<option value="1">Tampilkan</option>
	<option value="0" <?php if($stt==0){ echo "selected";} ?>>Sembunyikan</option>
</select><br>
<input type="submit" value="Ubah" name="A_ubah" />
</form>
</div><!--posisi ck-->
</div><!--isi-->
<?php include 'footer.php';?><!-- footer -->
</body>
</html>