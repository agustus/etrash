<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 21:17:04
	**/
//Ngambil url dari pengaturan
$url = $pengaturan->nyandak_asset('ckeditor/ckeditor.js');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include 'meta.php'; ?>
<!-- cssnya -->
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/tabel.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $url; ?>"></script>
</head>

<body bgcolor="#EAECF0">
<div id="header">Tambah Artikel</div><!--header-->
<div id="menu">
<div id="posisi_gambar">
<div id="gambar">
</div><!--gambar-->
</div><!--posisi gambar-->
<div id="posisi">
<?php include 'menu.php';?>
</div><!--menu-->
</div><!--posisi-->
</div><!-- kotak -->
<div id="isi">
<br />
<div id="posisi_ck">
<hr><script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<form action="<?php echo site_url();?>" method="post">
Judul : <input type="text" name="A_judul" style="width:400px"><br>
Isi : <textarea class="ckeditor" name="A_isi"></textarea><br>
Status : 
<select name="A_stt">
	<option value="1">Tampilkan</option>
	<option value="0">Sembunyikan</option>
</select><br>
<input type="submit" value="Posting" name="A_pos" />
</form>
</div><!--posisi ck-->
</div><!--isi-->
<?php include 'footer.php';?><!-- footer -->
</body>
</html>