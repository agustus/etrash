<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include 'meta.php';?>
<!-- cssnya -->
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
</head>

<body bgcolor="#EAECF0">
<div id="header">Admin Panel</div><!--header-->
<div id="menu">
<div id="posisi_gambar">
<div id="gambar">
</div><!--gambar-->
</div><!--posisi gambar-->
<div id="posisi">
<?php include 'menu.php';?>
</div><!--menu-->
</div><!--posisi-->
<div id="isi">
<div id="kotak">
<div id="text">Selamat Datang <?php echo $_SESSION['username'];?></div>
</div><!-- kotak -->
</div><!--isi-->
<?php include 'footer.php';?><!-- footer -->
</body>
</html>