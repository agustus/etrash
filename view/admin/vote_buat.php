<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include "meta.php";?>
<!-- cssnya -->
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/tabel.css" rel="stylesheet" type="text/css" />
</head>

<body bgcolor="#EAECF0">
<div id="header">Buat Vote TPA</div><!--header-->
<div id="menu">
<div id="posisi_gambar">
<div id="gambar">
</div><!--gambar-->
</div><!--posisi gambar-->
<div id="posisi">
<?php include "menu.php";?>
</div><!--menu-->
</div><!--posisi-->
</div><!-- kotak -->
<div id="isi">
<br />
<div id="posisi_ck">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<form action="<?php echo site_url();?>" method="post">
<div class="tabel" >
<table cellpadding='5'>
	<tr>
		<td style="width:190px"></td>
		<td style="width:20px"></td>
		<td></td>
	</tr>
	<tr>
		<td style="width:190px">Nama Lokasi</td>
		<td style="width:20px"> : </td>
		<td><input type="text" name="A_nama" value="" style="width:400px"></td>
	</tr>
	<tr>
		<td style="width:190px">Lokasi</td>
		<td style="width:20px"> : </td>
		<td><textarea cols="100px" rows="3px" name="A_lok"></textarea></td>
	</tr>
	<tr>
		<td style="width:190px">Latitude</td>
		<td style="width:20px"> : </td>
		<td><input type="text" name="A_lat" value="" style="width:400px"></td>
	</tr>
	<tr>
		<td style="width:190px">Longtitude</td>
		<td style="width:20px"> : </td>
		<td><input type="text" name="A_long" value="" style="width:400px"></td>
	</tr>
	<tr>
		<td style="width:190px">URL Foto</td>
		<td style="width:20px"> : </td>
		<td><input type="text" name="A_foto" value="" style="width:400px"></td>
	</tr>
	<tr>
		<td colspan="3"><input type="submit" value="Tambah" name="A_tambah" /></td>
	</tr>
</table>
</div>
</form>
</div><!--posisi ck-->
</div><!--isi-->
<?php include "footer.php";?><!-- footer -->
</body>
</html>