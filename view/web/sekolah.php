<!DOCTYPE html>
<head>
    <?php include "meta.php"; ?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
</head>

<body>    

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php"; ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Tentang Sekolah</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <!-- section title start -->
            <section class="grid_12 section-title">
                <h5>SMK Negeri 2 Cimahi</h5>
            </section><!-- section title end -->

            <section class="grid_12">
                <center><img src="img/logo.jpg" width="200px"></center><br><br>
                <p>SMK Negeri 2 Cimahi berdiri sejak tahun 2007 di atas tanah seluas 1,6 Hektar yang beralamat di Jl. Kamarung KM. 1,5 No.69 Desa Nyalindung Citeureup Kota Cimahi. Pada tahun Pertama berdiri SMK Negeri 2 Cimahi Memiliki 2 Kompetensi keahlian yaitu Teknik Mekatronika dan Teknik Multimedia, dimana pada saat itu SMK Negeri 2 Cimahi menumpang di SMK Negeri 3 Cimahi sampai tahun 2010. Menginjak tahun ke 2 atau di tahun 2008 SMK Negeri 2 membuka Kompetensi Keahlian Rekayasa Perangkat Lunak. Dan pada tahun 2011 membuka Kompetensi Keahlian Animasi sehingga sampai saat ini SMK Negeri 2 Cimahi memiliki 4 Kompetensi Keahlian. </p>
            </section>

            <section class="grid_12 section-title">
                <h5>Visi Dan Misi</h5>
            </section><!-- section title end -->

            <section class="grid_12">
                <p>Menghasilkan tamatan yang memiliki kompetensi tinggi di bidang teknologi, bertaqwa kepada Tuhan yang Maha Esa dan Mampu berwirausaha sesuai dengan program keahlian dalam menghadapi era globalisasi pada tahun 2015.<br><br>
                    1. Menyiapkan tamatan yang bertaqwa kepada Tuhan Yang Maha Esa<br>
2.Menyiapkan tamatan menjadi tenaga kerja yang memiliki kreatifitas tinggi dan sikap <br>
3.professional yang dapat memberikan pelayanan terhadap kebutuhan industri di tingkat Nasional dan Internasional<br>
4.Menghasilkan Tamatan yang tanggap terhadap perubahan globalisasi.<br>
5.Menyiapkan tamatan yang mandiri dan mampu menciptakan lapangan kerja.<br> </p>
            </section>

            <section class="grid_12">
               <img src="img/smk2.jpg" height="500px" width="1000px">
            </section>

            <!-- team member article start -->
           

            <!-- team member article start -->


        

            <!-- team member article start -->
        

            <!-- note start -->
            <section class="grid_12 note">
                <p class="text">SMK Negeri 2 Cimahi 2015</p>
            </section><!-- note end -->

        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include 'footer.php'; ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
    <!-- end js files -->
</body>
</html>
