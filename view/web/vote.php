<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-15 08:40:49
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php"; ?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
     <link rel="stylesheet" href="css/progressbar.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->

    <!--[if IE 7]>
    <link rel="stylesheet" href="css/ie7.css" media="screen" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->	

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="<?php echo base_asset('js/jquery.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_asset('js/jquery.validate.min.js');?>"></script>
    <script type="text/javascript">
	$(document).ready(function(){
		$("#myform").validate({
			debug: false,
			rules: {
				name: "required"
			},
			messages: {
				name: "Pilih pilihan anda.",
			},
			submitHandler: function(form) {
				$.post("<?php echo base_url('web/vote.php?id='.$ambil_soal['id'].''); ?>", $("#myform").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});

	$(function() {
		$('#loading').ajaxStart(function(){
			$(this).fadeIn();
		}).ajaxStop(function(){
			$(this).fadeOut();
		});
	 
		$('#menu a').click(function() {
			var url = $(this).attr('href');
			$('#results').load(url);
			return false;
		});
	});
	</script> 
</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php"; ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Vote</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">
            <section class="grid_4">
            </section>
            <!-- buat yang udah vote -->
            <section class="grid_4" <?php if(!isset($_COOKIE["user_ids"])){ echo 'style="display:none"';}?>>
                <section class="note-box">
                    <p>Kamu udah pernah melakukan vote..</p>
                </section>
            </section>
 <section class="grid_4" <?php if(isset($_COOKIE["user_ids"])){ echo 'style="display:none"';}?>>
<form name="myform" id="myform" action="" method="POST">  
<input type="hidden" value="<?php echo $ambil_soal['id']; ?>" name="id_soal">
<input type="hidden" value="<?php echo $ambil_soal['soal']; ?>" name="soal">
<?php
	echo "<h5>Apakah ".$ambil_soal['nama_lokasi']." ini bagus?</h5>
    <img src='".$ambil_soal['url_poto']."' height='180'>
    ";
?>
<br>
<div id="loading" style="display:none"><img src="<?php echo base_asset('images/loading.gif');?>" /><br />Mohon tunggu. Vote anda sedang disimpan.....</div>
<?php
	if(!empty($ambil_jwb)){
		foreach ($ambil_jwb as $ambil_jwb) {
			echo "<input type='radio' name='jwb' checked='checked' value='".$ambil_jwb['id']."'/>".$ambil_jwb['jawaban']."<br>";
		}
	}
?>
<input type="submit" name="A_vote" value="Vote" class="btn-medium style-color"/> 
</form>
          </section>
          <section class="grid_4">
          </section>
            <!-- section title start -->
            <section class="section-title">
                <h3>Hasil Voting</h3>
            </section>
            <!-- note start -->
         	<?php
         	$stat = $vote->statistik($ambil_soal['id']);
            $stati = $vote->jumlah_vote($ambil_soal['id']);
            $jum = $stati['tots'];
			foreach ($stat as $stat) {
                $pr = sprintf("%2.1f",(($stat['vote']/$jum)*100));
				
			?>		
					<div id="results">
					<section class="grid_12">
                 		<div class="progress-wrap">
                            <div class="progress">
                              <div class="progress-bar color2" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $pr;?>%">
                               <span class="bar-width"><?php echo $pr;?>%</span>
                              </div>
                            </div>
                            <h5><?php echo $stat['jawaban'];?></h5>
                        </div>
                    </section>
                    </div>
			<?php
					//echo $stat['jawaban']." : ".$stat['vote']." %<br>".$gbr."<hr>";
				}
         	?>
                     <section class="grid_12">
               
            </section><!-- note end -->
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include "footer.php"; ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
    <!-- end js files -->
</body>
</html>
