<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 22:14:57
	**/
?>
	<base href="<?php echo base_asset();?>" />
    <meta charset="utf-8">
    <title><?php $pengaturan->ambilPengaturan('nama'); if($stt_menu=="artikel-det" && $dataArtikel!=""){ echo " - ".$dataArtikel['judul'];;}?></title>
    <meta name="description" content="<?php $pengaturan->ambilPengaturan('deskripsi');?>">
    <meta name="author" content="<?php $pengaturan->ambilPengaturan('author');?>">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="viewport" content="width=device-width">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />  