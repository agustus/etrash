<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-23 10:09:48
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php"; ?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
     <link rel="stylesheet" href="css/coba.css" media="screen" />
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>


</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php"; ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Buat Laporan Pengaduan</h1>
        </section><!-- page-title end -->         
    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- showcase wrapper portfolio single start -->
        <article class="showcase-wrapper single">

            <!-- showcase start -->
            <section class="showcase">

                <!-- section title start -->
                <section class="grid_12 section-title">

                    <!-- title start -->
                    <h5>Buat Laporan Pengaduan</h5><!-- title end -->

                    
                </section><!-- section title end -->

                <!-- showcase item start -->
                <article class="grid_12">
                    
                   <section class="comment-form">
                        <form action="<?php echo site_url(); ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
                            <label>Topik Pengaduan :</label>
                            <input class="name" id="comment-name" name="A_nama" type="text">
                        <label >Pilih Foto Bukti :</label>
  <input class="myFile" name="A_foto" type="file" id="nama_file" size="30"/>
<br><br>
                            <label>Deskripsi Laporan :</label>
                            <textarea class="comment-text" name="A_deskripsi" id="comment-message" rows="8"></textarea>
                        <br>
                            <input type="submit" class="btn-medium style-color" name="btnSimpan" id="btnSimpan" value="Upload" />
                        </form>
                        
                         
                    </section><!-- comment form end -->

           
                </article><!-- showcase item end -->

            </section> <!-- showcase end -->

        </article><!-- showcase wrapper portfolio single end -->
          <!-- content tabs start -->
            <section class="grid_3">

                
    </section><!-- content-wrapper end -->

    <?php include "footer.php"; ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
</body>
</html>