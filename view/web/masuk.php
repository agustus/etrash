<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 12:07:48
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php";?>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" />   

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php";?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Masuk</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <aside class="grid_3 aside" id="poster">
				<ul class="aside-widgets">
                    <!-- categories widget start -->
                    <li class="categories">
                        <h6>Poster Random</h6>
                        <img src="<?php echo $data['url_poster'];?>" height="250" width="200"/>
					</li>
				</ul>
            </aside>
			<aside class="grid_9 aside">
			<center>
			<div class="inputan">
<form action="<?php echo base_url('web/masuk.php');?>" method="post">
Username : <input type="text" name="A_uname" placeholder="Username" required="required"/><br>
Password : <input type="password" name="A_pass" placeholder="Password" required="required"/><br>
<input type="submit" name="Joss" value="Masuk" class="btn"/>
</form>
				Tidak punya akun? silahkan <a href="<?php echo base_url('web/daftar.php');?>">daftar disini</a>.
			</div>
            </center>
			</aside>
			
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include "footer.php";?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/portfolio.js"></script> <!-- portfolio custom options -->
    <script  src="js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto lightbox -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->

</body>
</html>
