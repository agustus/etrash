<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 22:10:51
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php";?>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

</head>

<body>

    <?php include('header.php'); ?>

        <section id="page-title">
            <h1>Artikel</h1>
        </section>

    </section>

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <!-- blog posts container start -->
            <ul class="grid_9 blog">

                <?php
                    if(empty($wadahArtikel)){
                        echo '<center><h6 class="title">Tidak ada artikel</h6></center>';
                    }else{
                        foreach ($wadahArtikel as $data) {
                            echo '
                <li class="blog-post">

                    <ul class="meta">
                        <li class="category tulisan">
                            <a href="#">category</a>
                        </li>

                        <li class="date">
                            <p>'.substr($data['tgl'], 8,2).' <br />'.bulan(intval(substr($data['tgl'], 5,2))).'</p>
                        </li>
                    </ul>

                    <section class="post">
                        <a href="'.base_url('web/artikel.php?baca='.$data['id_artikel']).'">
                            <h6 class="title">'.$data['judul'].'</h6>
                        </a>
                        <p><b>Oleh : <a href="'.base_url('web/profil.php?id='.$data['id']).'">'.$data['nama'].'</a> - '.$data['count'].' kali dibaca</b></p>
                        <p>'.strip_tags(substr($data['isi'], 0,545)).'</p>

                        <a href="'.base_url('web/artikel.php?baca='.$data['id_artikel']).'" class="btn-medium style-color">
                            <span>Baca</span>
                        </a>
                    </section>
                </li>
                            ';
                        } 
                    }    
                ?>

               
                <li class="ruler"></li>

                <?php
                if($stt_menu!="artikel-cari"){?>
                <li class="pagination">
                    <ul>
                        <?php paging_artikelDepan($noHal,$jumSem,$jumArt); ?>
                    </ul>
                </li>
                <?php 
                }
                ?>

            </ul>

            <!-- sidebar start -->
            <aside class="grid_3 aside">

                <!-- sidebar widgets start -->
                <ul class="aside-widgets">
              
                    
                    <li class="posts">
                        <h6>Artikel Teratas</h6>
                        <ul>
                        <?php
                            if(empty($topArtikel)){
                                echo "<li>Tidak ada artikel</li>";
                            }else{
                                foreach ($topArtikel as $topArt) {
                                    echo "
                                        <li>
                                            <a href='".base_url('web/artikel.php?baca='.$topArt['id'])."'>".$topArt['judul']." 
                                            <span class='text-light'>| ".tgl_indo(substr($topArt['tgl'], 0,10))."</span></a>
                                        </li>
                                    ";
                                }
                            }
                        ?>
    
                        </ul>
                    </li>

                    <li class="posts">
                        <h6>Komentar Terakhir</h6>
                        <ul>
                        <?php
                            if(empty($komentarTerakhir)){
                                echo "<li>Tidak ada komentar</li>";
                            }else{
                                foreach ($komentarTerakhir as $komentarTerakhir) {
                                    echo "
                                        <li>
                                            <a href='".base_url('web/artikel.php?baca='.$komentarTerakhir['id'])."'>".$komentarTerakhir['nama']."</a>
                                            <span class='text-light'>".substr($komentarTerakhir['komentar'], 0,35)."</span>
                                        </li>
                                    ";

                                }
                            }
                        ?>
    
                        </ul>
                    </li>
                    
                </ul><!-- sidebar widgets end -->
            </aside><!-- sidebar end -->
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include('footer.php'); ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/portfolio.js"></script> <!-- portfolio custom options -->
    <script  src="js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto lightbox -->
    <script  src="js/jquery.tweetscroll.js"></script> <!-- jQuery tweetscroll plugin -->
    <script  src="http://vjs.zencdn.net/c/video.js"></script> <!-- HTML5 video -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->

</body>
</html>
