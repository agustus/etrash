<!DOCTYPE html>
<head>
    <?php include "meta.php";?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" />
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
    
    <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
    <style>
    section{width:672px;margin:20px auto;background:rgba(255,255,255,.7);border-radius:8px}
#gallery{padding:10px}
#gallery ul{
    list-style-type:none;
}
#gallery ul li{
    display:inline-block;
    padding:5px;
}
#gallery ul li img:hover{
    opacity:0.8;
}
@import url('normalize.css');
*{padding:0;margin:0}
.twd-top{
    height:20px;
    background:rgba(0,0,0,.5);
    color:#ddd;
    margin-bottom:20px;
    padding-top:5px;
    font-size:11px;
    font-family:verdana, arial, Times New Roman;
    text-transform:uppercase;
    border-bottom:1px solid rgba(0,0,0,.1);
}
.twd-top a{
    text-decoration:none;
    color:#f4f4f4;
}
.twd-top a:hover{color:#fff}
.twd-top-left{
    display:block;
    float:left;
    padding-left:10px;
}
.twd-top-right{
    display:block;
    float:right;
    padding-right:10px;
}
.twd-clear{
    clear:both;
    padding:0;
    margin:0;
}

.demo-container{
    /*min-height:1000px;*/
}
    </style> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
</head>

<body>    

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php";?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Galeri Poster</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <!-- section title start -->
            <section class="grid_12 section-title">
                <h5>Galeri Poster</h5>
            </section><!-- section title end -->

           
                    <section>
<div class="demo-container">
            <div id="gallery">

                <ul>
                <?php
                if(!empty($data)){
		foreach ($data as $data) {
			echo "
					<li>
                        <a href='".$data['url_poster']."'' title='".$data['nama_poster']."'>
                            <img src='".$data['url_poster']."' alt='".$data['nama_poster']."' height='150' width='150'/>
                        </a>
                    </li>
				";
			}
		}else{
			echo "Belum ada poster";
		}
                ?>
                   
                </ul>
            </div>
     
        
        </section>
            </section>

           
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->


    <!-- js files -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
    <!-- end js files -->
</body>
</html>