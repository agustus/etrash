<!DOCTYPE html>
<head>
    <?php include 'meta.php'; ?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include 'header.php'; ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Laporan Pengaduan</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <!-- section title start -->
            <section class="grid_12 section-title">

                <!-- title start -->
                <h5>Laporan Pengaduan</h5><!-- title end -->
            </section><!-- section title end -->

        </section><!-- container_12 end -->

        <!-- portfolio items list start -->
       
        <ul id="filter-item" class="container_12">

            <!-- portfolio item start -->
            <li data-type="photography">
                <!-- portfolio figure with animation start-->
                <center><h2><?php echo $data['nama_laporan'];?></h2>             
                        <!-- img start -->
                        <img src="<?php echo $data['screenshoot'];?>" width="460" alt="Future Cars"/></center><br>
                        <section class="grid_12"><center>
                         <table><tr><td>Oleh : <a href="<?php echo base_url('web/profil.php?id='.$data['id_user']);?>"><?php echo $data['nama'];?></a></td></tr>
                       <tr><td>Deskripsi :</td></tr><tr><td><p>
                       <?php echo $data['deskripsi_laporan'];?>
                       </p></td></tr></table></center>
                        </section>
            </li><!-- portfolio item end -->
        </ul>

            <!-- portfolio item start -->
           

            <!-- portfolio item start -->
            <!-- portfolio item start -->
        

            <!-- portfolio item start -->
           

        <!-- pagination page start -->
    </section><!-- content-wrapper end -->

    <?php include 'footer.php'; ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/portfolio.js"></script> <!-- portfolio custom options -->
    <script  src="js/quicksand.js"></script> <!-- quicksand filter -->
    <script  src="js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto lightbox -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
</body>
</html>