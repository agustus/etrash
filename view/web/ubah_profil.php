<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-11 18:05:48
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php"; ?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>


</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php"; ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Profil</h1>
        </section><!-- page-title end -->         
    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- showcase wrapper portfolio single start -->
        <article class="showcase-wrapper single">

            <!-- showcase start -->
            <section class="showcase">

                <!-- section title start -->
                <section class="grid_12 section-title">

                    <!-- title start -->
                    <h5>Ubah Profil</h5><!-- title end -->

                
                </section><!-- section title end -->

               	<img src="<?php echo url_foto($dataAkun['email']); ?>" alt="showcase item" title="Untuk mengubah gambar, silahkan ubah email dan daftarkan di gravatar.com"/>
                <article class="grid_12 showcase-item">
                    
                   <section class="comment-form ubah-profil">
                        <form action="<?php echo base_url('web/profil.php?ubah');?>" method="post">
                            <label>Nama :</label>
                            <input type="text" name="A_nama" value="<?php echo $dataAkun['nama'];?>" />
                            <label>Email :</label>
                           	<input type="text" name="A_email" value="<?php echo $dataAkun['email'];?>" />
                            <label>Twitter :</label>
                            <input type="text" name="A_twt" value="<?php echo $dataAkun['twitter'];?>" />
                            <label>Pin BBM :</label>
                            <input type="text" name="A_bbm" maxlength="7" value="<?php echo $dataAkun['pin_bbm'];?>" />
                            <label>Status :</label>
                            <select name="A_stat">
								<option value="<?php echo $dataAkun['stt_sosial'];?>">-><?php echo $dataAkun['stt_sosial'];?><-</option>
								<option value="Jomblo">Jomblo</option>
								<option value="Baru Putus">Baru Putus</option>
								<option value="Pacaran">Pacaran</option>
								<option value="Baru Pacaran">Baru Pacaran</option>
								<option value="LDR (Loe Doang Relationship)">LDR (Loe Doang Relationship)</option>
								<option value="Friend Zone">Friend Zone</option>
							</select>
                        	<label>Password :</label>
                        	<input type="password" name="A_pass" placeholder="Password"/><small>*Kosongkan jika tidak diubah.</small>
							<label>&nbsp;</label>
							<input type="submit" name="ubah" value="Ubah" class="btn"/>
                        </form>
                       
                    </section><!-- comment form end -->

           
                </article><!-- showcase item end -->

            </section> <!-- showcase end -->

        </article><!-- showcase wrapper portfolio single end -->
          <!-- content tabs start -->


    <?php include "footer.php"; ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
</body>
</html>
