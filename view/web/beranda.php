<!--
 * @Author				: Localhost {Ferdhika Yudira}
 * @Email				: fer@dika.web.id
 * @Web					: http://dika.web.id
 * @Date				: 2015-01-10 20:30:13
 -->
 <!DOCTYPE html>
<head>
	<?php include "meta.php";?>

    <!-- Kumpulan cssnya -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" href="css/anythingslider.css">
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Advent+Pro:400,300,100,200' rel='stylesheet' type='text/css'>

</head>

<body>

    <?php include('header.php'); ?>

    
    <article id="anything-slider">
        <div class="shadow-top"></div>
        <section id="slider-container">
        </section>
        <div class="shadow-bottom"></div>
    </article>

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">      

            <section class="grid_3 services-info">
                <h6 class="title">Artikel Terbaru</h6>
                <?php
                	if(empty($wadahArtikel)){

                	}else{
                		foreach ($wadahArtikel as $data) {
                			echo '
                			<h6><a href="'.base_url('web/artikel.php?baca=').$data['id_artikel'].'">'.$data['judul'].'</a></h6>
                			<p>'.strip_tags(substr($data['isi'], 0, 220)).'...</p>
                			<a href="'.base_url('web/artikel.php?baca=').$data['id_artikel'].'" class="find-out-more">Selengkapnya</a>
                			';
                		}
                	}
                ?>
            </section> <!-- services info end -->

            <!-- services columns start -->
            <ul class="services-wrap">
                <li class="grid_3">
                    <a class="icon icon3">what </a>
                    <h6 class="title">Apa itu E-Trash ?</h6>
                    <p>E-Trash Merupakan sebuah website teknologi yang berfungsi sebagai tempat para user untuk menyampaikan keluhannya maupun mencari informasi yang berhubungan dengan sampah , disini user bisa sharing dan melaporkan orang yang membuang sampah sembarangan..</p>
                </li>

                <li class="grid_3">
                    <a class="icon icon1">fungsi </a>
                    <h6 class="title">Fungsi E-Trash</h6>
                    <p>E-Trash befungsi sebagai sebuah sarana masyarakat, masyarakat bisa berbagi dan mendapatkan informasi tentang kebersihan dan lainnya.</p>
                </li>

                <li class="grid_3">
                    <a class="icon icon2">tujuan </a>
                    <h6 class="title">Tujuan E-Trash</h6>
                    <p>E-Trash Bertujuan untuk membantu program pemerintah tentang kebersihan lingkungan (sampah). Dan bisa berbagi tentang cara untuk mengelola sampah menjadi berguna.</p>
                </li>
            </ul><!-- services columns end -->

        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include "footer.php"; ?>

    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/jquery.easing.1.2.js"></script><!-- Anything Slider optional plugins -->
    <script  src="js/jquery.anythingslider.min.js"></script><!-- Anything Slider -->    
    <script  src="js/jquery.anythingslider.fx.min.js"></script><!-- Anything Slider optional FX extension -->   
    <script  src="js/jquery.carouFredSel-5.6.4-packed.js"></script><!-- CarouFredSel carousel plugin -->
    <script  src="js/portfolio.js"></script> <!-- portfolio custom options -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->

    <!-- end js files -->

</body>
</html>
