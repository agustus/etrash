<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 12:07:48
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php";?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="icon" type="image/x-icon" href="favicon.ico" /> 

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php";?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Daftar</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <aside class="grid_3 aside" id="poster">
                <ul class="aside-widgets">
                    <!-- categories widget start -->
                    <li class="categories">
                        <h6>Poster Random</h6>
                        <img src="<?php echo $data['url_poster'];?>" height="250" width="200"/>
                    </li>
                </ul>
            </aside>
			<aside class="grid_9 aside">
			<center>
				<div id="respon-daftar"></div>
				<!--<section class="success-box">
                    <p>Success messages...</p>
                </section>-->
			<div class="inputan">
<form>
<input type="text" name="A_nama" placeholder="Nama" id="nama" required="required"/><br>
<input type="text" name="A_email" placeholder="Email" id="email" required="required"/><br>
<input type="text" name="A_uname" placeholder="Username" id="username" required="required"/><br>
<input type="password" name="A_pass" placeholder="Password" id="password" required="required"/><br>
<input type="text" name="A_twt" placeholder="twitter ex : ferdhikaaa" id="twitter"/><br>
<input type="text" name="A_bbm" maxlength="8" placeholder="Pin BBM ex : 77A8FF12" id="bbm"/><br>
Status Sosial :
<select name="A_stat" id="stat">
	<option value="Jomblo">Jomblo</option>
	<option value="Baru Putus">Baru Putus</option>
	<option value="Pacaran">Pacaran</option>
	<option value="Baru Pacaran">Baru Pacaran</option>
	<option value="LDR (Loe Doang Relationship)">LDR (Loe Doang Relationship)</option>
	<option value="Friend Zone">Friend Zone</option>
</select><br>
<input type="submit" name="daftar" value="Daftar" class="btn"/>
</form>
				
			</div>
            </center>
			</aside>
			<aside class="grid_3 aside">
			
            </aside>
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include "footer.php";?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/portfolio.js"></script> <!-- portfolio custom options -->
    <script  src="js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto lightbox -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->

    <script>
        /* <![CDATA[ */
        $(function() {
            $('.inputan .btn').on('click', function(event) {
                event.preventDefault();
                var nama = $('#nama').val();
                var email = $('#email').val();
                var username = $('#username').val();                
                var password = $('#password').val();
                var twitter = $('#twitter').val();
                var bbm = $('#bbm').val();
                var stat = $('#stat').val();

                $.ajax({
                    type: 'POST',
                    url: "<?php echo site_url();?>",
                    data: ({nama: nama, email: email, username: username, password: password, twitter:twitter, bbm:bbm, stat:stat})
                }).done(function(data) {
                    $('#respon-daftar').html(data);

                });
            });
        });
        /* ]]> */
    </script>

    
</body>
</html>
