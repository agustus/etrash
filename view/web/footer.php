<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 21:43:04
	**/
?>
	<footer id="footer" class="clearfix">
        <section class="container_12">
            <div class="ruler"></div>

            
            <article class="grid_3">
                <h6>TIM KAMI</h6>
                <section>
                <ul class="colored-list">
                	<li class="list-star"><span>Ferdhika Yudira</span></li>
                	<li class="list-star"><span>Miftah Khoiry</span></li>
                	<li class="list-star"><span>Muhammad Fauzan</span></li>
                	<li class="list-star"><span>Rizky Bayu</span></li>
                </ul>
                </section>
               
                <a href="<?php echo base_url('web/tentang.php?apa=kita');?>" class="find-out-more">Penasaran dengan kita ?</a>
            </article>

            
            <article class="grid_3">
                <h6>Kontak</h6>
                <p>Alamat: Jln Kamarung no.69<br /> Cimahi Utara</p>
                <br />
                <p>Phone: +6283 8217 08285</p>
                <br />
                <a href="mailto:info@smkn2cmi.sch.id">E-mail: info@smkn2cmi.sch.id</a>
            </article>

            <article class="grid_3">
                <h6>Sitemap</h6>
                <section>
                <ul class="colored-list">
                    <li class="list-star"><span><a href="<?php echo base_url('web/tentang.php?apa=sekolah');?>">Tentang Sekolah</a></span></li>
                    <li class="list-star"><span><a href="<?php echo base_url('web/tentang.php?apa=kita');?>">Tentang Kita</a></span></li>
                    <li class="list-star"><span><a href="<?php echo base_url('web/akun.php');?>">Akun</a></span></li>
                </ul>
                </section>
            </article>

            <div class="ruler bottom"></div>               
        </section>

       
        <section class="container_12 clearfix">
            <section class="copyright">
                <p><?php $pengaturan->ambilPengaturan('footer');?></p>
                <a class="to-top-link"><img src="img/to-top.png" class="to-top" alt="back to top"/></a>
            </section>
        </section>
    </footer>