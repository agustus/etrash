<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-20 12:31:33
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php"; ?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" href="css/tabel.css" media="screen" />
    <link rel="stylesheet" href="css/gh-buttons.css">
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" />

    <!--[if IE 7]>
    <link rel="stylesheet" href="css/ie7.css" media="screen" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->	

    <!-- google web fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <script>
	function hapus(id){
    	tanya = confirm("Bener mau di hapus?");
    	if(tanya == 1){
        	window.location.href="<?php echo base_url('web/akun.php?lagi=hapus_artikel&id=');?>"+nim;
    	}
	}
	</script>

</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php"; ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Artikel Saya</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">
            <!-- section title start -->
            <!-- note start -->
                     <section class="grid_12">
            <div class="tabel" >
                <table >
                    <tr>
                        <td>
                            No
                        </td>
                        <td>
                            Judul Artikel
                        </td>
                        <td>
                           Tanggal
                        </td>
                        <td width="180px">
                            Aksi
                        </td>
                    </tr>
                    <?php
					if(!empty($dataArtikel)){
						$no=1;
						foreach ($dataArtikel as $dataArtikel) {
					?>
						<tr>
							<td><?php echo $no;?></td>
							<td><?php echo $dataArtikel['judul'];?></td>
							<td><?php echo $dataArtikel['tgl'];?></td>
							<td>
							<center>
								<a href="<?php echo base_url('web/akun.php?lagi=ubah_artikel&id='.$dataArtikel['id_artikel']);?>"><img src='img/update.png'> Ubah</a>
	                         	<a href="javascript: hapus('<?php echo $dataArtikel['id_artikel'];?>')"><img src='img/delete.png'> Hapus</a>	
							</center>
							</td>
						</tr>
					<?php
						$no++;
						}
					}else{
						echo "<tr>
							<td colspan='4'>Kamu gak punya artikel :(</td>
						</tr>";
					}
					?>
                </table>
            </div>
            <center>
                	<a href="<?php echo base_url('web/akun.php?lagi=buat_artikel');?>" class="btn-big style-color">
                    	<span>Buat Artikel</span>
                	</a>
            </center>

            </section><!-- note end -->
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include "footer.php"; ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/jquery.tweetscroll.js"></script> <!-- jQuery tweetscroll plugin -->
    <script  src="js/socialstream.jquery.js"></script> <!-- Social Networks Feeds -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
    <!-- end js files -->
</body>
</html>
