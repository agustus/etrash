<!DOCTYPE html>
<head>
    <?php include "meta.php";?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
</head>

<body>    

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php";?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Laporan Pengaduan</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <!-- section title start -->
            <section class="grid_12 section-title">
                <h5>Laporan pengaduan</h5>
            </section><!-- section title end -->

            <?php
            if(!empty($data)){
				foreach ($data as $data) {
			?>
            <article class="grid_6 team">
                <img src="<?php echo $data['screenshoot'];?>" height="225px" width="160px" alt="team member" />
                <h5><a href="<?php echo base_url('web/laporan.php?view='.$data['id_laporan']);?>">Judul : <?php echo $data['nama_laporan'];?></a></h5>
                <p>
Deskripsi : <?php echo substr($data['deskripsi_laporan'],0,20);?>..<br>
Oleh : <a href="<?php echo base_url('web/profil.php?id='.$data['id_user']);?>"><?php echo $data['nama'];?></a></p>
            </article>
            <?php
            	}
			}else{
				echo "<article class=\"grid_6 team\">Tidak ada</article>";
			}
			?>

            <!-- note start -->
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include "footer.php";?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
    <!-- end js files -->
</body>
</html>
