<!DOCTYPE html>
<head>
    <?php include "meta.php"; ?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" />  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
</head>

<body>    

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php"; ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Team Kami</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <!-- section title start -->
            <section class="grid_12 section-title">
                <h5>Tentang Kami</h5>
            </section><!-- section title end -->

            <section class="grid_12">
                <p>
                Kita adalah sekumpulan anak anak jomblo <strong>(kecuali si fauzan)</strong>. 
                </p>
            </section>

            <section class="grid_12 section-title">
                <h5>Anggota Kami</h5>
            </section>

            
            <article class="grid_12 team">
                <img src="<?php echo base_asset('img/anggota/ferdhika.jpg');?>" width="193" height="198" />
                <h5>Ferdhika Yudira - Web Programmer</h5>
                <p>Ferdhika Yudira merupakan seorang manusia yang punya cita cita menjadi programmer sejak smp, 
                ia masuk jurusan RPL di smkn 2 cimahi dan berniat melanjutkan pendidikannya ke Universitas di Bandung.
                Sekarang dia masih lajang alias jomblo. 
                Kalo ada cewe cakep daerah setiabudi dan sekitarnya bisa di kenalin lewat fb.</p>

                
                <ul class="social">
				
                    <li class="facebook">
                        <a href="http://facebook.com/ferdhika31" target="_blank">facebook</a>
                    </li>

                    <li class="twitter">
                        <a href="http://twitter.com/ferdhikaaa" target="_blank">twitter</a>
                    </li>
                </ul>
            </article>

           
            <article class="grid_12 team">
                <img src="<?php echo base_asset('img/anggota/miftah.jpg');?>" alt="team member" />
                <h5>Miftah Khoiry - Database Maker</h5>
                <p>Miftah Khoiry Merupakan seorang siswa smkn 2 yang berjurusan di rpl dan seorang jomblo yang coeg sekali</p>

                
                <ul class="social">

                    <li class="facebook">
                        <a href="http://facebook.com/" target="_blank">facebook</a>
                    </li>
					
                    <li class="twitter">
                        <a href="http://twitter.com/">twitter</a>
                    </li>
                </ul>
            </article>

           
            <article class="grid_12 team">
                <img src="<?php echo base_asset('img/anggota/fauzan.jpg');?>" alt="team member" />
                <h5>Muhammad Fauzan - Database Maker</h5>
                <p>Muhammad Fauzan Merupakan orang yang tidak jomblo di antara team kami</p>

                
                <ul class="social">
				
                    <li class="facebook">
                        <a href="http://facebook.com/" target="_blank">facebook</a>
                    </li>

                    <li class="twitter">
                        <a href="http://twitter.com/">twitter</a>
                    </li>
                </ul>
            </article>

            
            <article class="grid_12 team">
                <img src="<?php echo base_asset('img/anggota/rizky.jpg');?>" alt="team member" />
                <h5>Rizky Bayu - Web Designer</h5>
                <p>Rizky Bayu Merupakan seorang siswa smkn 2 cimahi yang berjurusan di rpl dan merupakan seorang jomblo , dia bercita cita ingin sukses dan membahagiakan kedua orang tuanya dan calon pasangan saya kelak.</p>

                
                <ul class="social">

                    <li class="facebook">
                        <a href="http://facebook.com/RizkybayuCheater" target="_blank">facebook</a>
                    </li>

                    <li class="twitter">
                        <a href="http://twitter.com/RIIBAY">twitter</a>
                    </li>
                </ul>
            </article>


        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include 'footer.php'; ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
    <!-- end js files -->
</body>
</html>
