<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-15 09:16:47
	**/
?>
<!DOCTYPE html>
<head>
    <?php include 'meta.php';?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
     <link rel="stylesheet" href="css/progressbar.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> 

    <!--[if IE 7]>
    <link rel="stylesheet" href="css/ie7.css" media="screen" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->	

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

    <style>
      #panel {
        float: left;
        width: 300px;
        height: 550px;
      }
      #map-container {
        margin-left: 300px;
      }
      #map {
        width: 100%;
        height: 550px;
      }
      #markerlist {
        height: 400px;
        margin: 10px 5px 0 10px;
        overflow: auto;
      }
      .title {
        border-bottom: 1px solid #e0ecff;
        overflow: hidden;
        width: 256px;
        cursor: pointer;
        padding: 2px 0;
        display: block;
        color: #000;
        text-decoration: none;
      }
      .title:visited {
        color: #000;
      }
      .title:hover {
        background: #e0ecff;
      }
      #timetaken {
        color: #f00;
      }
      .info {
        width: 200px;
      }
      .info img {
        border: 0;
      }
      .info-body {
        width: 200px;
        height: 200px;
        line-height: 200px;
        margin: 2px 0;
        text-align: center;
        overflow: hidden;
      }
      .info-img {
        height: 220px;
        width: 200px;
      }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="<?php echo base_url('web/map.php');?>"></script>
    <script src="<?php echo base_asset('js/markerclusterer.js');?>"></script>
    <script src="<?php echo base_asset('js/speed_test.js');?>"></script>

    <script>
      google.maps.event.addDomListener(window, 'load', speedTest.init);
    </script>

</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php"; ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Vote</h1>
        </section><!-- page-title end -->

    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            
            <section class="grid_12">
                
                <div id="panel">
      <div style="display:none">
        <input type="checkbox" checked="checked" id="usegmm"/>
        <span></span>
      </div>

      <div style="display:none">
        Lokasi TPA:
        <select id="nummarkers">
          <option value="10">10</option>
          <option value="50">50</option>
          <option value="100" selected="selected">100</option>
          <option value="500">500</option>
          <option value="1000">1000</option>
        </select>

        <span>Time used: <span id="timetaken"></span> ms</span>
      </div>

      <strong>Lokasi TPA</strong>
      <div id="markerlist">

      </div>
    </div>
                <div id="map-container">
                    <div id="map"></div>
                </div>

              <!--<ul class="colored-list" class="display:none">
              	<?php
				if(!empty($tampil_vote)){
					foreach ($tampil_vote as $tampil_vote) {
						echo "
						<li class='list-arrow'>
                    		<span><a href='".base_url('./web/vote.php?id=').$tampil_vote['id']."'>".$tampil_vote['nama_lokasi']."</a></span>
                		</li>
						";
					}
				}else{
					echo "Tidak ada vote.";
				}
				?>
              </ul>-->
            </section>
             
            <!-- column full (grid_12) illustration start -->
            <section class="grid_12 column-illustration last">
           
            </section><!-- column full (grid_12) illustration end -->

            <!-- note start -->
                     <section class="grid_12">
               
            </section><!-- note end -->
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include 'footer.php'; ?>

    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
    <!-- end js files -->
</body>
</html>
