<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-11 18:01:01
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php";?>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>


</head>

<body>

    <!-- header wrapper start -->
    <section id="header-wrapper">

        <?php include "header.php";?>

        <!-- page-title start -->
        <section id="page-title">
            <h1>Profil</h1>
        </section><!-- page-title end -->         
    </section><!-- header wrapper end -->

    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- showcase wrapper portfolio single start -->
        <article class="showcase-wrapper single">

            <!-- showcase start -->
            <section class="showcase">

                <!-- section title start -->
                <section class="grid_12 section-title">

                    <h5>Profil</h5>

                </section>

                
                <article class="grid_12 showcase-item">

                    
                    <img src="<?php echo url_foto($dataAkun['email']); ?>" alt="showcase item"/>
                    
                    <div class="description">
                        <ul>
                            <li>Nama : <?php echo $dataAkun['nama'];?></li>
                            <li>Email : <?php echo '<a href="mailto:'.$dataAkun['email'].'" target="_blank">'.$dataAkun['email'].'</a>';?></li>
                            <li>Twitter : <?php echo '<a href="http://twitter.com/'.$dataAkun['twitter'].'" target="_blank">@'.$dataAkun['twitter'].'</a>';?></li>
                            <li>Pin BBM : <?php echo $dataAkun['pin_bbm'];?></li>
                    		<li>Status Sosial : <?php echo $dataAkun['stt_sosial'];?></li>
                            <li>Terdaftar sejak <?php echo tgl_indo(substr($dataAkun['date_register'], 0,10));?></li>
                        </ul> 
                    </div>

           
                </article>

            </section>
                    <?php
                    if(!isset($_GET['id'])){
                    ?>
 						<a href="<?php echo site_url('?ubah');?>" class="btn-medium style-color">
                            <span>Edit Profil</span>
                        </a> 
                    <?php  
                    }
                    ?>
        </article>
          
            <section class="grid_3">

                
                <section class="section-title">
                    <h5>Aktifitas</h5> 
                </section>

                
                <ul class="tabs">
                    <!--<li class="active">
                        <a href="#tab1">Artikel</a>
                    </li>-->
                    <li class="active">
                        <a href="#tab1">Poster</a>
                    </li>
                    <li>
                        <a href="#tab2">Laporan (Pengaduan)</a>
                    </li>
                    <li class="">
                        <a href="#tab3">Thread</a>
                    </li>
                    <li class="">
                        <a href="#tab4">Visitor Message</a>
                    </li>
                </ul><!-- tabs end -->

                <!-- tabs content -->
                <section class="tab-content-wrap">

                    <!-- tab1 
                    <div id="tab1" class="tab-content">
                        <?php
                        if(!empty($art)){
                            echo "<ul class='colored-list'>";
                            foreach ($art as $art) {
                                echo "<li class='list-arrow'>
                                    <span>
                                        <a href='".base_url('web/artikel.php?baca=').$art['id_artikel']."'>".$art['judul']."</a></strong> 
                                        <strong>".$art['count']."x</strong> Dibaca
                                    </span>
                                </li>";
                            }
                            echo "</ul>";
                        }else{
                            echo "<p>".$dataAkun['nama']." belum pernah buat thread di forum.</p>";
                        }
                        ?>
                    </div>-->


                    <div id="tab1" class="tab-content">
                        <?php
                        if(!empty($dataPoster)){
                            echo "<ul class='colored-list'>";
                            foreach ($dataPoster as $dataPoster) {
                                echo "<li class='list-arrow'>
                                    <span>
                                        <!--<a href='".base_url('web/galeri.php?view=').$dataPoster['id_poster']."'>".$dataPoster['nama_poster']."</a>-->
                                        ".$dataPoster['nama_poster']."
                                    </span>
                                </li>";
                            }
                            echo "</ul>";
                        }else{
                            echo "<p>".$dataPoster['nama']." belum pernah buat poster.</p>";
                        }
                        ?>
                    </div>

                    <div id="tab2" class="tab-content">
                        <?php
                        if(!empty($dataLaporan)){
                            echo "<ul class='colored-list'>";
                            foreach ($dataLaporan as $dataLaporan) {
                                echo "<li class='list-arrow'>
                                    <span>
                                        <a href='".base_url('web/laporan.php?view=').$dataLaporan['id_laporan']."'>".$dataLaporan['nama_laporan']."</a></strong> 
                                    </span>
                                </li>";
                            }
                            echo "</ul>";
                        }else{
                            echo "<p>".$dataLaporan['nama']." belum pernah ngelaporin orang.</p>";
                        }
                        ?>
                    </div>

                    <!-- tab2 content start -->
                    <div id="tab3" class="tab-content">
                        <?php
                        if(!empty($thread)){
                            echo "<ul class='colored-list'>";
                            foreach ($thread as $thread) {
                                echo "<li class='list-arrow'>
                                    <span>
                                        <a href='".base_url('forum/thread.php?id=').$thread['id_thread']."'>".$thread['judul']."</a></strong> 
                                        Kategori : <strong>".$thread['nama_kategori']."</strong>
                                    </span>
                                </li>";
                            }
                            echo "</ul>";
                        }else{
                            echo "<p>".$dataAkun['nama']." belum pernah buat thread di forum.</p>";
                        }
                        ?>
                    </div><!-- tab2 content end -->

                    <!-- tab3 content start -->
                    <div id="tab4" class="tab-content">

                         <!-- post comments start -->
                    <section class="post-comments">
                        <!-- comment form start -->
                    <section class="comment-form" <?php if(!isset($_GET['id'])){ echo "style='display:none'";}?>>
                        <h5>Kirim Pesan</h5>

                        <form action="<?php echo site_url();?>" method="post">
                            <label>Pesan:</label>
                            <input type="hidden" name="A_kode" value="<?php echo $idnya;?>">
                            <textarea class="comment-text" id="comment-message" name="A_pesan" rows="2"></textarea>
                            <input type="submit" class="btn-medium style-color" name="A_kirim" value="Kirim"> 
                        </form>
                        <div id="comment-response"></div>
                    </section><!-- comment form end -->
                        <br><h5><?php if(!empty($list_vm)){echo "Pesan (".count($list_vm).")";}else{echo "Tidak ada pesan.";}?></h5>

                        <!-- post comments list items start -->
                        <ul class="comments-li">
                            
                            <?php 
                            if(!empty($list_vm)){
                                foreach ($list_vm as $list_vm) {
                                    echo '<li>
                                    <section class="comment">
                                    <div class="avatar">
                                        <img src="'.url_foto($list_vm['email']).'" width="87" alt="author" />
                                    </div>

                                    <div class="comment-meta">
                                        <a href="'.base_url('web/profil.php?id='.$list_vm['id_user']).'"><h5>'.$list_vm['nama'].'</h5></a>
                                        '.tgl_indo(substr($list_vm['tanggal'], 0,10)).'
                                    </div>

                                    <div class="comment-body">
                                        <p>'.strip_tags($list_vm['vm']).'</p>
                                    </div>
                                </section>
                                </li>';
                                }
                            }
                            ?>
                                
                        </ul><!-- post comments list items end -->
                    </section><!-- post comments end -->

                    </div><!-- tab3 content end -->
                </section><!-- tabs content end -->
            </section><!-- tabs end -->
    </section><!-- content-wrapper end -->

    <?php include "footer.php";?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->
</body>
</html>
