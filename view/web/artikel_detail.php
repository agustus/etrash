<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 22:54:44
	**/
?>
<!DOCTYPE html>
<head>
    <?php include "meta.php";?>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/960_12_col.css" media="screen" />
    <link rel="stylesheet" href="css/reset.css" media="screen" />
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />
    <link rel="stylesheet" title="activestyle" href="css/default-blue.css" media="screen" /> <!--default blue color style-->  

    <!--[if IE 7]>
<link rel="stylesheet" href="css/ie7.css" media="screen" />
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie8.css" media="screen" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css" media="screen" />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

</head>

<body>

    <?php include('header.php'); ?>

        <!-- page-title start -->
        <section id="page-title">
            <h1><?php echo $dataArtikel['judul']; ?></h1>
        </section><!-- page-title end -->           
    </section><!-- header wrapper end -->


    <!-- content wrapper start -->
    <section id="content-wrapper">

        <!-- container_12 start -->
        <section class="container_12">

            <!-- blog post container start -->
            <article class="grid_9 blog single">

                <!-- blog post start -->
                <article class="blog-post">

                    <!-- meta start -->
                    <ul class="meta">
                        <li class="category tulisan">
                            <a href="#">category</a>
                        </li>

                        <li class="date">
                            <?php echo '<p>'.substr($dataArtikel['tgl'], 8,2).' <br />'.bulan(intval(substr($dataArtikel['tgl'], 5,2))).'</p>';?>
                        </li>
                    </ul><!-- meta end -->

                    <!-- post body start -->
                    <section class="post">
                        <h6 style="border-bottom: 1px solid #ECECEC;">Penulis : <a href="<?php echo base_url('web/profil.php?id='.$dataArtikel['id_user']); ?>"><?php echo $dataArtikel['nama']; ?></a>
                        </h6>
                        <?php echo $dataArtikel['isi']; ?>
                    </section><!-- post body end -->

                    <!-- post comments start -->
                    <section class="post-comments">
                        <?php
                            if(empty($dataKomentar)){
                                echo "<h5>Tidak Ada Komentar</h5>";
                            }else{
                                echo "<h5>Aya ".count($dataKomentar)." komentar</h5>";
                            }
                        ?>

                        <ul class="comments-li">
                            <?php
                            if(!empty($dataKomentar)){
                                foreach ($dataKomentar as $komen) {
                                    echo '
                            <li>
                                <section class="comment">
                                    <div class="avatar">
                                        <img src="'.url_foto($komen['email']).'" width="87" alt="author" />
                                    </div>

                                    <div class="comment-meta">
                                        <h5>'.$komen['nama'].'</h5>
                                        '.$komen['tgl'].'
                                    </div>

                                    <div class="comment-body">
                                        <p>'.strip_tags($komen['komentar']).'</p>
                                    </div>
                                </section>
                            </li>
                                    ';
                                }
                            }
                            ?>
                        </ul><!-- post comments list items end -->
                    </section><!-- post comments end -->

                    <!-- comment form start -->
                    <section class="comment-form">
                        <h5>buru komentar ath euy!</h5>

                        <form>
                            <input type="hidden" id="id_artikel" value="<?php echo $id_art;?>">
                            <label>Nama:</label>
                            <input class="name" id="comment-name" type="text">

                            <label>Email:</label>
                            <input class="email" id="comment-email" type="email">

                            <label>Komentar:</label>
                            <textarea class="comment-text" id="comment-message" rows="8"></textarea>
                            <input class="submit btn-medium style-color" type="submit" value="Kirim">
                        </form>
                        <div id="comment-response"></div>
                    </section><!-- comment form end -->
                </article><!-- blog post end -->
            </article><!-- blog post container end -->

            <!-- sidebar start -->
            <aside class="grid_3 aside">

           
                <ul class="aside-widgets">

        
                    <li class="posts">
                        <h6>Artikel Teratas</h6>
                        <ul>
                        <?php
                            if(empty($topArtikel)){
                                echo "<li>Tidak ada artikel</li>";
                            }else{
                                foreach ($topArtikel as $topArt) {
                                    echo "
                                        <li>
                                            <a href='".base_url('web/artikel.php?baca='.$topArt['id'])."'>".$topArt['judul']." 
                                            <span class='text-light'>| ".tgl_indo(substr($topArt['tgl'], 0,10))."</span></a>
                                        </li>
                                    ";
                                }
                            }
                        ?>
                        </ul>
                    </li>

                    
            </aside><!-- sidebar end -->
        </section><!-- container_12 end -->

    </section><!-- content-wrapper end -->

    <?php include('footer.php'); ?>

    <!-- js files -->
    <script  src="js/jquery-1.7.2.js"></script> <!-- jQuery 1.7.2 -->
    <script  src="js/jquery.tweetscroll.js"></script> <!-- jQuery tweetscroll plugin -->
    <script  src="js/socialstream.jquery.js"></script> <!-- Social Networks Feeds -->
    <script  src="js/include.js"></script> <!-- jQuery custom options -->
    <script  src="js/jquery.placeholder.min.js"></script><!-- jQuery placeholder fix for old browsers -->

    <script>
        /* <![CDATA[ */
        $(function() {
            $('.comment-form .submit').on('click', function(event) {
                event.preventDefault();
                var id_artikel = $('#id_artikel').val();
                var nama = $('#comment-name').val();
                var email = $('#comment-email').val();
                var komentar = $('#comment-message').val();

                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url('web/komentar.php');?>",
                    data: ({id_artikel:id_artikel, nama: nama, email: email, komentar: komentar})
                }).done(function(data) {
                    $('#comment-response').html(data);

                });
            });
        });
        /* ]]> */
    </script>
</body>
</html>
