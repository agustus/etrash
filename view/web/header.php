<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 20:54:39
	**/
?>
    <section id="header-wrapper">

        
        <header id="header" class="clearfix">
        
            <section id="logo">
                <a href="<?php echo base_url('web');?>">
                    <?php $pengaturan->ambilPengaturan('nama');?>
                </a>
            </section>

            <ul class="social">
            
                <li class="facebook">
                    <a href="#" target="_blank">facebook</a>
                </li>

                <li class="twitter">
                    <a href="http://twitter.com/smkn2cimahi" target="_blank">twitter</a>
                </li>
            </ul>
            <section id="nav-container">

                
                <nav id="nav">
                    <ul>
                        <li <?php if($stt_menu=="beranda"){ echo 'class="active"';}?>><a href="<?php echo base_url('web');?>">Beranda</a></li>
                        <li>
                            <a href="<?php echo base_url('forum');?>">Forum</a>
                        </li>
                        <li <?php if($stt_menu=="artikel" || $stt_menu=="artikel-cari"){ echo 'class="active"';}?>>
                            <a href="<?php echo base_url('web/artikel.php');?>">Artikel</a>
                        </li>
                        <li <?php if($stt_menu=="tentang"){ echo 'class="active"';}?>>
                            <a>Tentang</a>
                            <ul>
                                <li><a href="<?php echo base_url('web/tentang.php?apa=sekolah');?>">Sekolah</a></li>
                                <li><a href="<?php echo base_url('web/tentang.php?apa=kita');?>">Kita</a></li>
                            </ul>
                        </li>
                        <li <?php if($stt_menu=="lain"){ echo 'class="active"';}?>>
                            <a>Lain-Lain</a>
                            <ul>
                                <li><a>Poster</a>
                                    <ul>
                                        <li><a href="<?php echo base_url('web/akun.php?lagi=buat_poster');?>">Buat Poster</a></li>
                                        <li><a href="<?php echo base_url('web/galeri.php');?>">Galeri Poster</a></li>
                                    </ul>
                                </li>
								<li><a>Laporan Pengaduan</a>
                                    <ul>
                                        <li><a href="<?php echo base_url('web/laporan.php?buat');?>">Buat Laporan</a></li>
                                        <li><a href="<?php echo base_url('web/laporan.php');?>">List Laporan</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url('web/vote.php');?>">Voting TPA</a></li>
                            </ul>
                        </li>
                        <?php
                        $adaw = isset($_SESSION['hak_akses']);
                        if($adaw){
                            $hak = $_SESSION['hak_akses'];
                            if($hak==1){ //Menu admin
                        ?>
						<li <?php if($stt_menu=="akun" || $stt_menu=="artikel-admin"){ echo 'class="active"';}?>>
                            <a href="<?php echo base_url('web/profil.php');?>">Akun</a>
                            <ul>
                                <li><a href="<?php echo base_url('admin');?>">Admin Area</a></li>
                                <li><a href="<?php echo base_url('web/profil.php');?>">Profil</a></li>
                                <!--<li><a href="<?php echo base_url('web/artikel.php');?>">Artikel</a>
                                    <ul>
                                        <li><a href="<?php echo base_url('web/akun.php?lagi=buat_artikel');?>">Buat Artikel</a></li>
                                        <li><a href="<?php echo base_url('web/akun.php?lagi=lihat_artikel');?>">Lihat Artikel</a></li>
                                    </ul>
                                </li>-->
                                <li><a href="<?php echo base_url('web/akun.php?lagi=keluar');?>">Keluar</a></li>
                            </ul>
                        </li>
                        <?php
                            }else if($hak==2){ //Menu user
                        ?>
                        <li <?php if($stt_menu=="akun"){ echo 'class="active"';}?>>
                            <a href="<?php echo base_url('web/profil.php');?>">Akun</a>
                            <ul>
                                <li><a href="<?php echo base_url('web/profil.php');?>">Profil</a></li>
                                <!--<li><a href="<?php echo base_url('web/artikel.php');?>">Artikel</a>
                                    <ul>
                                        <li><a href="<?php echo base_url('web/akun.php?lagi=buat_artikel');?>">Buat Artikel</a></li>
                                        <li><a href="<?php echo base_url('web/akun.php?lagi=lihat_artikel');?>">Lihat Artikel</a></li>
                                    </ul>
                                </li>-->
                                <li><a href="<?php echo base_url('web/akun.php?lagi=keluar');?>">Keluar</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        }else{
                        ?>
                        <li <?php if($stt_menu=="akun"){ echo 'class="active"';}?>>
                            <a href="<?php echo base_url('web/profil.php');?>">Akun</a>
                            <ul>
                                <li><a href="<?php echo base_url('web/daftar.php');?>">Daftar</a></li>
                                <li><a href="<?php echo base_url('web/masuk.php');?>">Masuk</a></li>
                            </ul>
                        </li>
                        <?php    
                        }
                        ?>
                    </ul> 
                </nav>

                <select id="nav-responsive">
                    <option selected="" value="">Site Navigation...</option>
                    <option value="<?php echo base_url('web');?>">Beranda</option>
                    <option value="<?php echo base_url('forum');?>">Forum</option>
                    <option value="<?php echo base_url('web/artikel.php');?>">Artikel</option>
                    <option value="<?php echo base_url('web/tentang.php');?>">Tentang</option>
                    <option value="<?php echo base_url('web/tentang.php?apa=sekolah');?>">- Sekolah</option>
                    <option value="<?php echo base_url('web/tentang.php?apa=kita');?>">- Kita</option>
                    <option value="<?php echo base_url();?>">Poster</option>
                    <option value="<?php echo base_url('web/akun.php?lagi=buat_poster');?>">Buat Poster</option>
                    <option value="<?php echo base_url('web/galeri.php');?>">Galeri Poster</option>
                    <option value="<?php echo base_url();?>">Laporan Pengaduan</option>
                    <option value="<?php echo base_url('web/laporan.php?buat');?>">-Buat Laporan</option>
                    <option value="<?php echo base_url('web/laporan.php');?>">-Daftar Laporan</option>
                    <option value="<?php echo base_url('web/vote.php');?>">Voting TPA</option>
                    <option value="<?php echo base_url('web/profil.php');?>">Akun</option>
                    <?php
                        $adaw = isset($_SESSION['hak_akses']);
                        if($adaw){
                            $hak = $_SESSION['hak_akses'];
                            if($hak==1){ //Menu admin
                                echo "<option value='".base_url('admin')."'>- Admin Area</option>";
                                echo "<option value='".base_url('web/profil.php')."'>- Profil</option>";
                                echo "<option value='".base_url('web/akun.php?lagi=keluar')."'>- Keluar</option>";
                            }else if($hak==2){ //menu user
                                echo "<option value='".base_url('web/profil.php')."'>- Profil</option>";
                                echo "<option value='".base_url('web/akun.php?lagi=keluar')."'>- Keluar</option>";
                            }else{
                                echo "<option value='".base_url('web/masuk.php')."'>- Masuk</option>";
                                echo "<option value='".base_url('web/daftar.php')."'>- Daftar</option>";
                            }
                        }
                    ?>
                </select> 

                <section id="search">
                    <form action="<?php echo base_url('web/artikel.php');?>" method="get">
                        <input id="search-bkg" name="cari" type="text" placeholder="Search..." />
                        <input id="search-submit" type="submit" />
                    </form>
                </section>

            </section>

        </header>
    </section>