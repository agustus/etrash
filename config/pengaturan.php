<?php
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 14:28:04
	**/
date_default_timezone_set('Asia/Jakarta');
class Pengaturan {
	//Database
	private $host = "localhost";
	private $user = "root";
	private $pass = "";
	private $dbname = "db_etrash";
	private $db_koneksi = null;
	//Pengaturan Website
	private $nama 	 = "E-Trash";
	private $author	 = "Logic";
	private $deskripsi = "Sampah elektronik";
	private $keyword = "";
	private $footer = "Copyright &copy; 2015 SMKN 2 Cimahi"; 
	private $baseurl = "http://localhost/etrash/";
	
	public function ambilKoneksi(){
		$this->db_koneksi = new mysqli($this->host, $this->user, $this->pass, $this->dbname);	
		if ($this->db_koneksi->connect_errno) {
			echo "Gagal konak ke databes: (" . $this->db_koneksi->connect_errno . ") " . $this->db_koneksi->connect_error;
		} else {
			return $this->db_koneksi; 
		}
	}

	public function ambilPengaturan($nami){
		switch ($nami) {
			case 'nama':
				echo $this->nama;
				break;
			case 'deskripsi':
				echo $this->deskripsi;
				break;
			case 'keyword':
				echo $this->keyword;
				break;
			case 'footer':
				echo $this->footer;
				break;
			case 'author':
				echo $this->author;
				break;
			default:
				echo "tidak ada pengaturan";
				break;
		}
	}

	public function url(){
		return $this->baseurl;
	}

	public function nyandak_url($url=null){
		if($url==""){
			return $this->baseurl;
		}else{
			return $this->baseurl.$url;
		}
	}

	public function nyandak_asset($url=null){
		if($url==""){
			return $this->baseurl."asset/";
		}else{
			return $this->baseurl."asset/".$url;
		}
	}
	
}