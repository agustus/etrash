<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 21:55:31
	**/
	//konfigurasi
	require_once '../config/pengaturan.php';

	//class (model)
	require_once '../class/login.php';
	require_once '../class/artikel.php';
	require_once '../class/komentar.php';
	require_once '../class/akun.php';
	require_once '../class/forum.php';
	require_once '../class/thread.php';
	require_once '../class/vote.php';
	require_once '../class/vm.php';
	require_once '../class/poster.php';
	require_once '../class/laporan.php';

	//gudang (library)
	require_once '../gudang/url.php';
	require_once '../gudang/tgl.php';
	require_once '../gudang/notifikasi.php';
	require_once '../gudang/paging.php';
	require_once '../gudang/foto.php';
	require_once '../gudang/bbcode.php';