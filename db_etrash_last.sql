-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2015 at 11:47 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_etrash`
--
CREATE DATABASE IF NOT EXISTS `db_etrash` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_etrash`;

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE IF NOT EXISTS `tb_artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(450) DEFAULT NULL,
  `isi` text,
  `tgl` timestamp NULL DEFAULT NULL,
  `tgl_ubah` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tb_user_id` int(11) NOT NULL,
  `count` int(11) DEFAULT '1',
  `stt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_artikel_tb_user_idx` (`tb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tb_artikel`
--

INSERT INTO `tb_artikel` (`id`, `judul`, `isi`, `tgl`, `tgl_ubah`, `tb_user_id`, `count`, `stt`) VALUES
(15, 'Daur Ulang Sampah Plastik', '<p>\r\n	<span style="font-family: verdana,geneva;">Plastik sudah menjadi kebutuhan sehari-hari di kehidupan manusia saat ini. Mulai dari produk elektronik, makanan, minuman, mainan, rumah tangga, dan lain-lain. Seiring dengan kemajuan jaman dan pola hidup manusia saat ini maka pemakaian plastik yang semakin tinggi akhirnya menimbulkan dampak negatif yaitu sampah.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;">Plastik memiliki waktu lama jika melewati proses daur ulang secara alami sehingga menjadi permasalahan serius karena memberikan kontribusi serius pada soal sampah. Pemrosesan daur ulang sampah plastik yang sudah dilakukan sekarang masih berbanding jauh dengan jumlah sampah plastik yang dihasilkan. Melihat kondisi diatas maka diperlukan ide dan kreatifitas guna menekan jumlah limbah plastik menjadi barang lain yang bermanfaat.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;">Salah satu yang bisa menjadi ide adalah mengolah sampah plastik menjadi produk keperluan rumah tangga. Ada banyak jenis barang berguna dari daur ulang limbah plastik seperti keranjang sampah, tas, souvenir, dan lain sebagainya. Ketika sampah berubah menjadi bentuk serta fungsi lain yang bisa bermanfaat maka itu merupakan sumber peluang bisnis alternatif yang menguntungkan.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;">Sampah plastik yang bisa digunakan sebagai bahan untuk membuat kerajinan sangat bermacam-macam. Mulai plastik keras dari <a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/7-kode-plastik-daur-ulang">kemasan</a> <a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/7-kode-plastik-daur-ulang">botol</a> minuman hingga plastik &ldquo;lunak&rdquo; seperti kantong plastik. Bahan penunjang tambahan juga tidak terlalu banyak seperti lem, benang, kawat, dan pewarna (jika diperlukan). Ketika semua sudah tersedia tinggal yang paling menentukan adalah ide untuk mendapatkan kreasi produk yang inovatif serta berguna. Tentu saja yang memiliki nilai jual tinggi dipasaran.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;">Kekuatan dari produk daur ulang sampah plastik ini tidak diragukan lagi. Selain plastik terbuat dari bahan sintetis yang sifatnya tahan lama, jika dalam pembuatannya teliti maka tingkat keawetan produk tersebut bisa lama. Setiap produk yang memiliki nilai seni dan kegunaannya luas maka otomatis pasar akan merespon dengan antusias. Keuntungan yang didapatkan dari penjualan produk plastik daur ulang persentasenya tinggi karena tidak memerlukan biaya tinggi untuk mendapatkan bahan-bahannya.</span></p>\r\n<p>\r\n	<span style="font-family: verdana,geneva;"><a class="alrptip" href="http://www.inginbisnis.com/peluangbisnis/memanfaatkan-berbagai-macam-material-dari-sampah-elektronik">Peluang usaha</a> daur ulang limbah plastik ini masuk kategori industri global, dalam arti bahwa tidak hanya industri skala besar saja yang bisa melakukannya tetapi usaha kecil dan menengah juga dapat mendaur ulang sampah plastik menjadi produk yang berguna. Jika anda peduli dengan lingkungsan dan memiliki ide-ide yang inovatif maka peluang usaha daur ulang sampah plastik ini bisa menjadi pilihan bijaksana yang menguntungkan.</span></p>\r\n', '2015-01-25 11:25:05', '2015-01-25 11:25:10', 8, 2, '1'),
(16, 'E-Trash Converter Machine ( E C M )', '<p style="text-align: center;">\r\n	<img alt="" height="365" src="/E-Trash/asset/content_upload/images/econverter.png" width="381" /></p>\r\n<p>\r\n	Saya berharap mesin ini di masa depan memang benar benar terrealisasikan<br />\r\n	karena dengan adanya mesin ini saya yakin kota akan menjadi bersih<br />\r\n	karena orang orang akan membuang sampah ke mesin ini dan menukarnya<br />\r\n	dengan uang , dan itupun otomatis akan menarik orang orang untuk<br />\r\n	memungut sampah di jalan / di lingkungannya<br />\r\n	mesin ini mengeluarkan uang sebesar jumlah sampah yang di kumpulkan<br />\r\n	saya berharap mesin ini bisa segera di buat.<br />\r\n	Mungkin hanya sebatas hayalan tapi saya yakin suatu saat di masa depan<br />\r\n	mesin ini akan ada.</p>\r\n', '2015-01-25 11:28:09', '2015-02-04 11:23:51', 8, 10, '1'),
(17, 'Bahaya Sampah Elektronik dan Pengolahannya ', '<p>\r\n	Indonesia terkenal sebagai masyarakat dengan budaya yang konsumtif, termasuk dalam pemakaian barang-barang elektronik, entah itu gadget, ponsel, televisi model terbaru, komputer, laptop, dan sebagainya. Setiap hari, pasti ribuan jenis barang elektronik diproduksi dan diimpor ke negeri ini.</p>\r\n<p>\r\n	Tapi pernahkah terbayangkan, bagaimana nasib barang elektronik bekas yang sudah tak dipakai. Sudah benarkah pengelolaan sampah elektronik atau <em>electronic waste</em> atau <em>e-waste</em> di Indonesia? Sebab jika salah pengelolaan, <em>e-waste</em> yang banyak mengandung bahan beracun dan berbahaya (B3) akan memberi dampak negatif bagi manusia.</p>\r\n', '2015-01-25 12:12:47', NULL, 11, 1, '1'),
(18, 'Perkembangan TPA Pengolah Sampah Menjadi Energi di Indonesia ', '<p>\r\n	(Business Lounge &ndash; Business Today) &ndash; Perkembangan pembangkit listrik tenaga sampah di Indonesia semakin berkembang di Indonesia.&nbsp; Pengoperasian kembali pengolahan sampah menjadi gas metan yang akan menjadi sumber energi listrik di tempat pembuangan akhir salah satunya telah dilakukan oleh TPA Sukawinatan, Palembang.</p>\r\n<p>\r\n	Tidak hanya di Palembang saja, Surabaya juga telah memulai pengembangan tempat pembuangan sampah yang mampu mengolah sampah menjadi energi listrik.&nbsp; Terdapat tiga TPA yang sudah berhasil mengolah sampah menjadi energi listrik yaitu TPA Bratang, TPA Jambangan,dan&nbsp; TPA Tenggilis. Masing-masing TPA tersebut dapat menghasilkan energi listrik sebesar 4.000 kilowatt. Saat ini TPA Bratang yang sudah beroperasi sedangkan TPA Jambangan dan Tenggilis baru akan mulai beroperasi.</p>\r\n<p>\r\n	Masalah sampah memang menjadi tantangan bagi semua negara-negara didunia tidak hanya di Indonesia. Konsep waste to energy seperti yang telah dilakukan dibeberapa TPA di Palembang dan Surabaya dapat menjadi salah satu contoh dalam mengelola masalah sampah terutama sampah-sampah anorganik yang sulit untuk terurai.</p>\r\n<p>\r\n	Diperlukan keseriusan dari pemerintah serta kerjasama aktif pemerintah dengan pihak investor swasta. Prospek pengembangan TPA yang dapat mengolah sampah menjadi energi listrik sangat bagus untuk kedepannya. Sampah bisa menjadi alternatif energi baru dan terbarukan selain dapat mengurangi dampak pencemaran lingkungan yang dihasilkan dengan adanya sampah. Pada saat ini negara-negara maju sudah banyak yang menjalankan konsep ini seperti Amerika Serikat dengan 2.500 MW listrik per tahun dihasilkan dari sampah sebesar 35 juta ton (17% dari total sampah yang dihasilkan) dan Denmark juga berhasil memanfaatkan lebih dari 80% volume sampah untuk waste to energy. Indonesia selanjutnya juga dapat mengembangkan konsep ini pada kota kota lainnya selain Palembang dan Suraba.</p>\r\n<p>\r\n	Saat ini jika melihat dari TPST Bantar Gebang&nbsp; sudah memiliki pembangkit PLTSA berkapasitas 26 MW.&nbsp; Potensi listrik&nbsp; dari TPST Bantar Gebang ini sudah dijadikan&nbsp; sebagai pembangkit listrik oleh PT Godang Tua Jaya sebagai pengelola TPST Bantar Gebang. Jika masing masing rumah rata-rata membutuhkan daya 1.000 watt listrik, maka melalui TPST Bantar Gebang ini mampu memenuhi kebutuhan listrik&nbsp; dari 26.000 rumah.</p>\r\n<p>\r\n	Permasalahan yang saat ini dihadapi oleh TPA Bantar Gerbang terutama Pemprov DKI adalah dalam hal manajemen pengelolaannya. TPA Bantar Gerbang selama ini dikelola oleh swasta sehingga pemerintah tidak dapat melakukan intervensi dalam proses pengelohannya. Sudah terdapat perjanjian hitam diatas putih sehingga Pemprov DKI masih harus menunggu hingga masa kontrak tersebut berakhir untuk melakukan perubahan sistem pengolahan sampah di TPA Bantar Gerbang. Sehingga untuk kedepannya DKI Jakarta memiliki TPA yang ramah lingkungan dan seluruh TPA di Indonesia juga dapat melakukan hal yang sama.</p>\r\n', '2015-01-25 12:13:22', NULL, 11, 1, '1'),
(19, 'Sampah Kukar Bisa Jadi Batu Bara ', '<p>\r\n	TENGGARONG - Pemkab Kukar sedang menyiapkan mesin pengolahan sampah menjadi bahan bakar setara batu bara, berlokasi di sekitar TPA Bekotok. Bahan bakar yang dihasilkan bakal memasok sumber energi bagi sejumlah power plant di Kukar. Dua pekan lalu, Bupati Kukar Rita Widyasari didampingi Kepala Perusda Kelistrikan dan Sumber Daya Energi (PKSDE) Kukar M Shafik Avicenna, mengunjungi Shanghai, Tiongkok</p>\r\n<p>\r\n	&ldquo;Kami ke Shanghai melihat langsung pabrik pengolahan sampah menjadi energi. Produk yang dihasilkan berupa pupuk dan batu bara,&rdquo; kata Rita. Dia mengatakan, produk yang dihasilkan hampir sama dengan batu bara asli dengan kalori 5.000-6.000. Hanya bagian unsur yang agak berbeda. Pabrik pengolahan sampah di Shanghai itu akan diimplementasikan di Kukar.</p>\r\n<p>\r\n	Tim dari PT Ametis Indo Sampah siap merakit mesinnya. Rita berharap tahun ini mesin pengolahan sampah siap dioperasikan. &ldquo;Kami sangat membutuhkan energi. Banyak sekali power plant yang perlu. Jadi, sebetulnya ini pengganti energi yang tidak terbarukan. Kalau nanti kekurangan batu bara, kita bisa pakai ini,&rdquo; jelasnya.</p>\r\n<p>\r\n	Ke depan, TPA Bekotok bukan lagi tempat pembuangan sampah tapi hanya pembuangan sementara. Sampah langsung diproses mesin pengolah dan keluar menjadi sumber energi. Pabrik pengolahan sampah ini akan dikelola swasta bekerja sama dengan PKSDE. &ldquo;Sementara ini, mesin pengolahan sampah didanai investor.</p>\r\n<p>\r\n	Kami tidak memakai APBD dulu,&rdquo; ujar Rita. Dia berharap, mesin pengolahan sampah bisa dioperasikan November 2014. &ldquo;Produk pengolahan sampah bisa menjadi briket dan pupuk. Pupuknya pun punya kualitas baik. Sekarang seluruh Shanghai menggunakan produk pupuk andalan ini,&rdquo; tuturnya. Rita menambahkan, briket merupakan buah pemikiran orang Indonesia.</p>\r\n<p>\r\n	Kukar akan menjadi daerah pertama di Indonesia yang menerapkan mesin pengolah sampah menjadi bahan bakar setara batu bara. Sementara itu, Presiden Direktur PT Ametis Indo Sampah Bayu Indrawan mengatakan, mesin pengolahan sampah menggunakan prinsip teknologi hidrotermal, memanfaatkan uap panas jenuh.</p>\r\n<p>\r\n	Sampah campur dimasukkan ke reaktor, lalu diinjeksikan dengan uap panas, sekitar 25-30 bar. &ldquo;Setelah satu jam, sampah keluar menjadi bahan seragam atau homogen yang tidak bau lagi. Bahkan bau seperti aroma kopi dan gampang kering. Karakteristiknya mendekati batu bara,&rdquo; jelas Bayu. Sedangkan bila produk ini dicampur 80 persen batu bara, karakteristik bahan bakarnya jadi hampir sama.</p>\r\n<p>\r\n	&ldquo;Apabila mau mendesain untuk power plant khusus dengan produk ini, kita bisa mendapatkan 100 persen batu bara tapi boiler harus dimodifikasi,&rdquo; imbuhnya. Menurutnya, keunggulan produk ini paling cocok dengan Indonesia. Selama ini dikenal beberapa teknologi pengolahan sampah secara konvensional, yakni incinerator menggunakan prinsip pembakaran.</p>\r\n<p>\r\n	Incinerator memang sangat bagus tapi tidak cocok dengan karakteristik sampah Indonesia. Mayoritas sampah negeri ini merupakan organik yang kadar airnya tinggi. Selain itu, biaya pengolahan sampah lewat incinerator terlalu tinggi. Belum cocok diterapkan di Indonesia. &ldquo;Air yang dibakar berarti efisiensi konversinya rendah sehingga energi yang dihasilkan incinerator kurang maksimum,&rdquo; ucapnya.</p>\r\n', '2015-01-25 12:13:50', '2015-01-25 12:50:03', 11, 2, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jwb_vote`
--

CREATE TABLE IF NOT EXISTS `tb_jwb_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jawaban` varchar(45) DEFAULT NULL,
  `vote` int(11) DEFAULT '0',
  `tb_vote_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_jwb_vote_tb_vote1_idx` (`tb_vote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tb_jwb_vote`
--

INSERT INTO `tb_jwb_vote` (`id`, `jawaban`, `vote`, `tb_vote_id`) VALUES
(1, 'Iya', 19, 1),
(2, 'Tidak', 4, 1),
(5, 'Iya', 10, 2),
(6, 'Tidak', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE IF NOT EXISTS `tb_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(45) DEFAULT NULL,
  `deskripsi` text,
  `parent_id` int(11) DEFAULT '0',
  `stt` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `nama_kategori`, `deskripsi`, `parent_id`, `stt`) VALUES
(1, 'Diskusi Sampah', NULL, 0, 1),
(2, 'Jalanan', NULL, 1, 1),
(3, 'Sekitar Masyarakat', NULL, 1, 1),
(4, 'Daur Ulang', NULL, 0, 1),
(5, 'Info Daur Ulang', NULL, 4, 1),
(6, 'Karya Daur Ulang', NULL, 4, 1),
(7, 'Lainnya', NULL, 0, 1),
(8, 'TPA (Tempat Pembuangan Akhir)', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori_poster`
--

CREATE TABLE IF NOT EXISTS `tb_kategori_poster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori_poster` varchar(45) DEFAULT NULL,
  `deskripsi_kategori_poster` text,
  `stt` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_kategori_poster`
--

INSERT INTO `tb_kategori_poster` (`id`, `nama_kategori_poster`, `deskripsi_kategori_poster`, `stt`) VALUES
(1, 'Lainnya', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_komentar`
--

CREATE TABLE IF NOT EXISTS `tb_komentar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(450) DEFAULT NULL,
  `email` varchar(450) DEFAULT NULL,
  `komentar` text,
  `tgl` timestamp NULL DEFAULT NULL,
  `stt` varchar(45) DEFAULT NULL,
  `tb_artikel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_komentar_tb_artikel1_idx` (`tb_artikel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_komentar`
--

INSERT INTO `tb_komentar` (`id`, `nama`, `email`, `komentar`, `tgl`, `stt`, `tb_artikel_id`) VALUES
(2, 'Rizky Bayu', 'rizkybayu66@gmail.com', 'Setuju Setuju !!!', '2015-01-25 11:30:05', '1', 16),
(3, 'Miftah', 'Kagaminekiori@gmail.com', 'Saya setuju jika mesin ini benar - benar ada , mungkin banyak orang yang ber profesi sebagai pemulung', '2015-01-25 11:30:54', '1', 16),
(4, 'Fauzan Arrasyid', 'fauzan.sullivan@gmail.com', 'kita sangat berharap kepada pemerintah agar segera membuat mesin seperti ini karena akan sangat bermanfaat', '2015-01-25 12:52:52', '1', 16);

-- --------------------------------------------------------

--
-- Table structure for table `tb_laporan`
--

CREATE TABLE IF NOT EXISTS `tb_laporan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_laporan` varchar(450) DEFAULT NULL,
  `deskripsi_laporan` text,
  `screenshoot` varchar(500) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `stt` varchar(45) DEFAULT NULL,
  `tb_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_laporan_tb_user1_idx` (`tb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tb_laporan`
--

INSERT INTO `tb_laporan` (`id`, `nama_laporan`, `deskripsi_laporan`, `screenshoot`, `tanggal`, `stt`, `tb_user_id`) VALUES
(5, 'Buang Aqua Malem Malem', 'Ini orang saya foto dari deket mau buang sampah sembarangan dari motor pas malem malem, kalo liat orang ini tolong di kasih tau jangan buang sampah sembarangan lagi..\r\nTKP : daerah Cimahi', '../asset/img/laporan/CAM01543.jpg', '2015-01-25 12:04:42', '1', 11),
(6, 'Nih min liat ada yang buang sampah sembarangan', 'enaknya diapain ?', '../asset/img/laporan/CAM01548.jpg', '2015-01-25 12:41:38', '1', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tb_poster`
--

CREATE TABLE IF NOT EXISTS `tb_poster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_poster` varchar(450) DEFAULT NULL,
  `deskripsi_poster` text,
  `url_poster` varchar(450) DEFAULT NULL,
  `tgl_poster` timestamp NULL DEFAULT NULL,
  `stt` int(11) DEFAULT '1',
  `count` int(11) DEFAULT '0',
  `tb_kategori_poster_id` int(11) NOT NULL,
  `tb_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_poster_tb_kategori_poster1_idx` (`tb_kategori_poster_id`),
  KEY `fk_tb_poster_tb_user1_idx` (`tb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tb_poster`
--

INSERT INTO `tb_poster` (`id`, `nama_poster`, `deskripsi_poster`, `url_poster`, `tgl_poster`, `stt`, `count`, `tb_kategori_poster_id`, `tb_user_id`) VALUES
(17, 'Masa kamu ga?', 'nih', '../asset/img/poster/petani.jpg', '2015-01-25 12:20:50', 1, 0, 1, 10),
(18, 'Nah hayo ?', 'aku cuma gue ?', '../asset/img/poster/nah.jpg', '2015-01-25 12:36:46', 1, 0, 1, 10),
(19, 'Hayo ', 'Mending mana ? ', '../asset/img/poster/ax.JPG', '2015-01-25 12:39:04', 1, 0, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tb_rating`
--

CREATE TABLE IF NOT EXISTS `tb_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` varchar(45) DEFAULT NULL,
  `voter` varchar(45) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `tb_thread_id` int(11) NOT NULL,
  `tb_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_rating_tb_thread1_idx` (`tb_thread_id`),
  KEY `fk_tb_rating_tb_user1_idx` (`tb_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_thread`
--

CREATE TABLE IF NOT EXISTS `tb_thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `judul` varchar(45) DEFAULT NULL,
  `isi` text,
  `tb_kategori_id` int(11) NOT NULL,
  `tb_user_id` int(11) NOT NULL,
  `tgl_post` timestamp NULL DEFAULT NULL,
  `tgl_modif` timestamp NULL DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `stt` int(45) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tb_thread_tb_kategori1_idx` (`tb_kategori_id`),
  KEY `fk_tb_thread_tb_user1_idx` (`tb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `tb_thread`
--

INSERT INTO `tb_thread` (`id`, `parent_id`, `judul`, `isi`, `tb_kategori_id`, `tb_user_id`, `tgl_post`, `tgl_modif`, `count`, `stt`) VALUES
(32, 0, 'Nanya Kebersihan', 'Ada yang punya info kebersihan di lapangan sabuga ? :bingung: ', 3, 10, '2015-01-25 11:35:40', NULL, 3, 1),
(33, 0, 'Sampah Halangi Pemandangan', 'Kacaulah kacau pas lagi ke bandung daerah ciroyom , pas naik angkot sebelumnya rapih , tapi kok pas di pasar ci royom pemandangannya berubah jadi 180 derajat berbeda ya ? padahal sayang banget  :( ', 2, 9, '2015-01-25 11:36:16', NULL, 9, 1),
(34, 0, 'Info Daur ulang ', 'Kalo ada yang punya karya dari sampah menjadi berguna bisa di share disini  :_peace: ', 6, 8, '2015-01-25 11:36:22', NULL, 5, 1),
(35, 33, 'Jawab', 'iya kang aku juga sama pas turun dari angkot kaya gitu juga  :( ', 2, 10, '2015-01-25 11:37:38', NULL, 0, 1),
(36, 32, 'Nanya Kebersihan', '[quote=Miftah menulis :]Ada yang punya info kebersihan di lapangan sabuga ? :bingung: [/quote]\r\nCoba tanya ke petugas aja kang siapa tau bisa nge bantu  :D ', 3, 9, '2015-01-25 11:37:41', NULL, 0, 1),
(37, 33, 'Sampah Halangi Pemandangan', '[quote=RizkyBayu menulis :]Kacaulah kacau pas lagi ke bandung daerah ciroyom , pas naik angkot sebelumnya rapih , tapi kok pas di pasar ci royom pemandangannya berubah jadi 180 derajat berbeda ya ? padahal sayang banget  :( [/quote]\r\n\r\n\r\nMungkin orng orangnya belum sepenuhnya punya kesadaran tentang kebersihan  :bingung: ', 2, 8, '2015-01-25 11:38:00', NULL, 0, 1),
(39, 0, 'Jual Tas Daur Ulang Semen 3 Roda', 'Misi gan saya numpang lapak , saya mau jual tas dari hasil daur ulang karung semen 3 roda \r\nsaya , bahanya udah saya rombak jadi bagus + kece  :belo: \r\n[b]PRICE[/b] : IDR 50.000\r\nsok yang mau komen  :D ', 6, 9, '2015-01-25 11:41:07', NULL, 9, 1),
(42, 0, '[ASK] Tempat Pembuang Akhir Daerah Cimahi', 'Misi gan , saya mau nanya kalo tpa daerah cimahi dimana ya ? :heran: ', 8, 9, '2015-01-25 11:42:38', NULL, 14, 1),
(44, 39, 'Pesen ', '[quote]Wilayah mana kang ? masih ada ga barangnya ? :bingung: [/quote]', 6, 10, '2015-01-25 11:44:14', NULL, 0, 1),
(45, 42, '[ASK] Tempat Pembuang Akhir Daerah Cimahi', '[quote=RizkyBayu menulis :]Misi gan , saya mau nanya kalo tpa daerah cimahi dimana ya ? :heran: [/quote]\r\nsaya taunya juga tempat pembuangan umum yang di deket pasar kuda kang  :D ', 8, 10, '2015-01-25 11:45:42', NULL, 0, 1),
(46, 0, 'Sampahmu Mengahalangi Duniaku', 'Hadeuh kacau kacau kacau lah , sampah di mana dimana , kenapa harus ada sampah ?  :( padahal kalo gaada sampah bisa enak :(', 2, 9, '2015-01-25 11:45:56', NULL, 1, 1),
(47, 42, '[ASK] Tempat Pembuang Akhir Daerah Cimahi', '[quote=RizkyBayu menulis :]Misi gan , saya mau nanya kalo tpa daerah cimahi dimana ya ? :heran: [/quote]\r\nkalo mau buan ke siu aja kang', 8, 10, '2015-01-25 11:46:03', '2015-01-25 11:46:39', 0, 1),
(48, 42, '[ASK] Tempat Pembuang Akhir Daerah Cimahi', '[quote=RizkyBayu menulis :]Misi gan , saya mau nanya kalo tpa daerah cimahi dimana ya ? :heran: [/quote]\r\n[br] \r\nDi daerah padasuka ada kayanya gan  :D  :loveindonesia: ', 8, 8, '2015-01-25 11:46:48', NULL, 0, 1),
(49, 0, 'Lagu - Tentang Sampah', '[b]Sampahmu Sampahku [/b]\r\n[br][br]\r\nku lihat sampah disana[br]\r\nku lihat sampah disini[br]\r\nku buang ke sana [br]\r\nkau buang ke sini[br]\r\n[br]\r\noh sakit hatiku [br]\r\nmelihat semua itu [br]\r\nandaikan engkau tau [br]\r\nbertapa susahnya membuang sampah...[br][br]', 7, 9, '2015-01-25 11:47:36', '2015-01-25 11:48:35', 5, 1),
(55, 42, '[ASK] Tempat Pembuang Akhir Daerah Cimahi', '[quote=ferdhika31 menulis :][quote=RizkyBayu menulis :]Misi gan , saya mau nanya kalo tpa daerah cimahi dimana ya ? :heran: [/quote]\r\n[br] \r\nDi daerah padasuka ada kayanya gan  :D  :loveindonesia: [/quote]\r\n\r\nDaerah padasuka di sebelah mananya gan?  :bingung:', 8, 12, '2015-01-25 12:46:00', '2015-01-25 12:46:42', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(450) DEFAULT NULL,
  `nama` varchar(450) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `pin_bbm` varchar(7) DEFAULT NULL,
  `date_register` timestamp NULL DEFAULT NULL,
  `stt_sosial` varchar(45) DEFAULT NULL,
  `lvl` varchar(45) DEFAULT NULL,
  `stt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `email`, `nama`, `twitter`, `pin_bbm`, `date_register`, `stt_sosial`, `lvl`, `stt`) VALUES
(8, 'ferdhika31', '8ba9943744e7d67eceac0275e64235d6', 'fer@dika.web.id', 'Ferdhika Yudira', 'ferdhikaaa', '758E2FC', '2015-01-25 11:08:48', 'Jomblo', '1', '1'),
(9, 'RizkyBayu', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'rizkybayu66@gmail.com', 'Rizky Bayu', 'RIIBAY', '7483746', '2015-01-25 11:29:15', 'Jomblo', '2', '1'),
(10, 'Miftah', '6dcfda78d3e8bc6136fde44ee4bb0362', 'kagaminekiori@gmail.com', 'Miftah Khoiry', 'abc', '768c9f5', '2015-01-25 11:29:33', 'Jomblo', '2', '1'),
(11, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'rpl4rt08@gmail.com', 'Superuser', 'admin', '8327427', '2015-01-25 11:51:19', 'Baru Putus', '1', '1'),
(12, 'fauzan', '8ba9943744e7d67eceac0275e64235d6', 'fauzan.sullivan@gmail.com', 'Fauzan', 'fauzanarrasyid', '7D3A431', '2015-01-25 12:43:20', 'Pacaran', '2', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vm`
--

CREATE TABLE IF NOT EXISTS `tb_vm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_di_vm` int(11) DEFAULT NULL,
  `user_vm` int(11) DEFAULT NULL,
  `vm` text,
  `tanggal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tb_vm`
--

INSERT INTO `tb_vm` (`id`, `user_di_vm`, `user_vm`, `vm`, `tanggal`) VALUES
(5, 9, 8, 'Kang mau beli tas kerajinan yng di forum, mesen 50 pcs ya.. #fast respon eaaa ', '2015-01-25 11:44:08'),
(6, 8, 9, 'Okeh siap gan :D', '2015-01-25 11:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vote`
--

CREATE TABLE IF NOT EXISTS `tb_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lokasi` text,
  `lokasi` text,
  `tgl_vote` timestamp NULL DEFAULT NULL,
  `latitude` varchar(250) DEFAULT NULL,
  `longtitude` varchar(45) DEFAULT NULL,
  `url_poto` varchar(450) DEFAULT NULL,
  `stt` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_vote`
--

INSERT INTO `tb_vote` (`id`, `nama_lokasi`, `lokasi`, `tgl_vote`, `latitude`, `longtitude`, `url_poto`, `stt`) VALUES
(1, 'TPA Leuwi Gajah', 'Kecamatan Cimahi Selatan, Kelurahan Batujajar Timur Cimahi', '2015-01-24 04:57:33', '-6.8993906', '107.4680915', 'http://kelas7smipa.edublogs.org/files/2012/09/Bencana-Leuwigajah-14-19lvh9q.jpg', 1),
(2, 'TPA Cipatat', 'Kecamatan Cipatat Bandung Barat', '2015-01-24 04:57:33', '-6.8248172', '107.3718027', 'http://farm7.staticflickr.com/6136/6003816227_0545cd7b5f.jpg', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD CONSTRAINT `fk_tb_artikel_tb_user` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_jwb_vote`
--
ALTER TABLE `tb_jwb_vote`
  ADD CONSTRAINT `fk_tb_jwb_vote_tb_vote1` FOREIGN KEY (`tb_vote_id`) REFERENCES `tb_vote` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_komentar`
--
ALTER TABLE `tb_komentar`
  ADD CONSTRAINT `fk_tb_komentar_tb_artikel1` FOREIGN KEY (`tb_artikel_id`) REFERENCES `tb_artikel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_laporan`
--
ALTER TABLE `tb_laporan`
  ADD CONSTRAINT `fk_tb_laporan_tb_user1` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_poster`
--
ALTER TABLE `tb_poster`
  ADD CONSTRAINT `fk_tb_poster_tb_kategori_poster1` FOREIGN KEY (`tb_kategori_poster_id`) REFERENCES `tb_kategori_poster` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_poster_tb_user1` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_rating`
--
ALTER TABLE `tb_rating`
  ADD CONSTRAINT `fk_tb_rating_tb_thread1` FOREIGN KEY (`tb_thread_id`) REFERENCES `tb_thread` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_rating_tb_user1` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_thread`
--
ALTER TABLE `tb_thread`
  ADD CONSTRAINT `fk_tb_thread_tb_kategori1` FOREIGN KEY (`tb_kategori_id`) REFERENCES `tb_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_thread_tb_user1` FOREIGN KEY (`tb_user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
