<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 23:34:01
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);

	$stt_menu = "tentang";
	$apa = $_GET['apa'];

	if($apa){
		if($apa=="kita"){
			//echo "ini tentang kita";
			include "../view/web/kita.php";
		}elseif($apa=="sekolah"){
			include "../view/web/sekolah.php";
		}
	}else{
		redirect('index.php');
	}