<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-09 13:49:22
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();
	$login = new Login($koneksi);

	$artikel = new Artikel($koneksi);

	$stt_menu = "beranda"; //buat menu aktif nya
	//buat nampung artikel
	$wadahArtikel = $artikel->tampil_limit(1);
	include "../view/web/beranda.php";
	