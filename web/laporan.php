<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-23 09:50:06
	**/

	require_once "../config/autoload.php";

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);

	$laporan = new Laporan($koneksi);

	$stt_menu = "lain";

	if(isset($_GET['buat'])){
		$login->auth();
		if(isset($_POST['btnSimpan'])){
			$laporan->upload();
		}
		include '../view/web/laporan_buat.php';
	}elseif(isset($_GET['view'])){
		$id_laporan = $_GET['view'];
		$data = $laporan->tampil_satu_lap($id_laporan);
		include '../view/web/laporan_detail.php';
	}else{
		$data = $laporan->tampil();
		include '../view/web/laporan_lihat.php';
	}