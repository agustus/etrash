<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 23:50:04
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();
	$login = new Login($koneksi);
	$poster = new Poster($koneksi);
	$stt_menu = "akun";

	if ($login->isUserLogin() == TRUE){
		//cek hak akses cuy
		$hak = $_SESSION['hak_akses'];
		$login->cek($hak);
	
		if(site_url()==base_url('web/masuk.php')){
			echo "<META http-equiv=\"refresh\" content=\"0;URL=index.php\"> ";
		}else{
			echo "<script>history.go(-2)</script>";	
		}
		
	}else {
		$data = $poster->ambilRandom();
    	include "../view/web/masuk.php";
	}