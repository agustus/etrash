<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-23 10:45:11
	**/
	require_once "../config/autoload.php";

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	//$login->auth();

	$poster = new Poster($koneksi);
	$stt_menu = "lain";

	if(isset($_GET['view'])){
		$id_nya = $_GET['view'];
		if(empty($id_nya)){
			redirect('../web/galeri.php');
		}else{
			echo "detail poster";
		}
	}else{
		$data = $poster->tampil();
		include '../view/web/poster_lihat.php';
	}