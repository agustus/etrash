<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-15 08:31:46
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);

	$vote = New Vote($koneksi);

	$stt_menu="lain";

	if(isset($_GET['id'])){
		$id_vote = $_GET['id'];
		if(!empty($id_vote)){
			$login->auth();
			$ambil_soal = $vote->tampil_soal($id_vote);
			$ambil_jwb = $vote->tampil_jawaban($id_vote);
			//prosesnya
			if(isset($_POST['A_vote'])){
				$id_soal = $_POST['id_soal'];
				$soal = $_POST['soal'];
				$jwb = $_POST['jwb'];
				$vote->tambah($jwb,$id_soal);
				//exit();
			}
			include "../view/web/vote.php";
		}else{
			redirect('../web/vote.php');
		}
	}else{
		$tampil_vote = $vote->tampil_semua();
		include "../view/web/vote_semua.php";
	}