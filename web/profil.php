<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-11 18:00:42
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	$login->auth();

	$stt_menu = "akun";
	$id_us = $_SESSION['user_id'];

	$akun = new Akun($koneksi);
	$forum = new Forum($koneksi);
	$artikel = new Artikel($koneksi);
	$vm = new Vm($koneksi);
	$poster = new Poster($koneksi);
	$laporan = new Laporan($koneksi);

	if(isset($_GET['ubah'])){
		if(isset($_POST['ubah'])){
			$akun->ubah($id_us);
		}
		$dataAkun = $akun->tampil_satu($id_us);
		include "../view/web/ubah_profil.php";
	}else if(isset($_GET['id'])){
		$idnya = $_GET['id'];
		if(!empty($idnya)){
			$thread = $forum->tampil_thread_user($idnya);
			$art = $artikel->tampil_artikel_user($idnya);
			$dataAkun = $akun->tampil_satu($idnya);
			$dataLaporan = $laporan->tampil_satu($idnya);
			$dataPoster = $poster->tampil_satu($idnya);
			$list_vm = $vm->tampil_vm($idnya);

			//kalo di kirim
			if(isset($_POST['A_kirim'])){
				$kode = $_POST['A_kode'];
				$pesan = $_POST['A_pesan'];
				$vm->tambah($kode,$pesan);
			}

			include "../view/web/profil.php";
		}else{
			redirect('../web');
		}
	}else{
		$thread = $forum->tampil_thread_user($id_us);
		$art = $artikel->tampil_artikel_user($id_us);
		$dataAkun = $akun->tampil_satu($id_us);
		$dataLaporan = $laporan->tampil_satu($id_us);
		$dataPoster = $poster->tampil_satu($id_us);
		$list_vm = $vm->tampil_vm($id_us);
		include "../view/web/profil.php";
	}