<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 23:37:55
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	$login->auth();

	$poster = new Poster($koneksi);

	$idus = $_SESSION['user_id'];

	//artikel
	$artikel = new Artikel($koneksi);

	$do = isset($_GET['lagi']);
	if($do){
		$hah = $_GET['lagi'];
		if(empty($hah)){ //direk
			redirect('../web/akun.php');
		}
		if($hah=="buat_artikel"){
			$stt_menu = "artikel-admin"; //buat menu aktif nya
			if(isset($_POST['A_pos'])){
				$judul = $_POST['A_judul'];
				$isi = $_POST['A_isi'];
				$stt = 1;
				$artikel->simpan($judul,$isi,$stt);
			}
			include "../view/web/artikel_buat.php";
		}elseif($hah=="lihat_artikel"){
			$stt_menu = "artikel-admin";
			$dataArtikel = $artikel->tampil_artikel_user($idus);
			include "../view/web/artikel_saya.php";
		}elseif($hah=="ubah_artikel"){
			$stt_menu = "artikel-admin";
			$dataArtikel = $artikel->tampil_artikel_user($idus);
			include "../view/web/artikel_saya.php";
		}elseif($hah=="hapus_artikel"){
			$stt_menu = "artikel-admin";
			$dataArtikel = $artikel->tampil_artikel_user($idus);
			include "../view/web/artikel_saya.php";
		}elseif($hah=="buat_poster"){
			if(isset($_POST['btnSimpan'])){
				$poster->upload();
			}
			$stt_menu = "lain";
			include "../view/web/poster_buat.php";
		}elseif($hah=="profil"){
			include "../view/web/profil.php";
		}elseif($hah=="keluar"){
			$login = new Login($koneksi);
			$login->doLogout();
		}else{
			redirect('../web/akun.php');
		}
	}else{
		//echo "ini halaman akun dashboard";
		redirect('../web/profil.php');
	}