<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-11 13:23:39
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	//artikel
	$komentar = new Komentar($koneksi);

	if(isset($_POST['id_artikel'])){
		$id = $_POST['id_artikel'];
		$nama = $_POST['nama'];
		$email = $_POST['email'];
		$komen = $_POST['komentar'];
		$komentar->kirim($nama,$email,$komen,$id);

	}