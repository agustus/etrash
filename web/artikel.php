<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 22:23:56
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);

	$artikel = new Artikel($koneksi);
	$komentar = new Komentar($koneksi);

	$at = $artikel->tampil();

	$jumSem = count($at);//Jumlah semua artikel
	$jumArt = 4; //jumlah per halaman

	//Top artikel
	$topArtikel = $artikel->top_artikel(7);

	//Komentar Terakhir
	$komentarTerakhir = $komentar->terakhir(7);

	if(isset($_GET['baca'])){
		$id_art = $_GET['baca'];
		$stt_menu = "artikel"; //buat menu aktif nya
		$artikel->ngitung($id_art);
		$dataArtikel = $artikel->tampil_satu_aktif($id_art);
		if(empty($dataArtikel)){
			redirect('./artikel.php');
		}
		$dataKomentar = $komentar->tampil($id_art);
		include "../view/web/artikel_detail.php";
	}elseif(isset($_GET['halaman'])){
		$stt_menu = "artikel"; //buat menu aktif nya
		$noHal= $_GET['halaman'];
		
		$wadahArtikel = $artikel->tampil_awal_akhir($noHal,$jumArt);
		include "../view/web/artikel.php";
	}elseif(isset($_GET['cari'])){
		$cari = $_GET['cari'];
		$stt_menu = "artikel-cari"; //buat menu aktif nya
		
		$wadahArtikel = $artikel->cari($cari);
		include "../view/web/artikel.php";
	}else{
		$stt_menu = "artikel"; //buat menu aktif nya
		$noHal=1;
		//buat nampung artikel
		$wadahArtikel = $artikel->tampil_awal_akhir($noHal,$jumArt);
		include "../view/web/artikel.php";
	}