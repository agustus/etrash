<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-24 19:18:51
	**/

	require_once "../config/autoload.php";

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();

	$login = new Login($koneksi);
	//$login->auth();

	$vote = new Vote($koneksi);

	$dat = $vote->tampil_semua_json();
	echo $dat;