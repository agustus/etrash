<?php 
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-01-10 23:49:58
	**/
	require_once '../config/autoload.php';

	$pengaturan = new Pengaturan();
	$koneksi = $pengaturan->ambilKoneksi();
	$login = new Login($koneksi);
	$poster = new Poster($koneksi);
	$stt_menu = "akun";

	$akun = new Akun($koneksi);

	if ($login->isUserLogin() == TRUE){
		//cek hak akses cuy
		$hak = $_SESSION['hak_akses'];
		
		echo "<META http-equiv=\"refresh\" content=\"0;URL=index.php\"> ";
	}else {
		if(isset($_POST['username'])){
			$akun->daftar();
		}
		$data = $poster->ambilRandom();
    	include "../view/web/daftar.php";
	}